try{
	$.validate({
	    modules : 'location, date, security, file',
	    onModulesLoaded : function() {
	      $('#country').suggestCountry();
	      
	    }
	  });
}
catch(e){
	alert(e);
}
  

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );