<%@page import="gov.bfar.cms.dao.AboutUsDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	AboutUsDao dao = new AboutUsDao();
	int seq = dao.getSuggestedSequence();
%>
<div class="row">
	<div class="wrapper">
		<div class="row">
			<form action="/BFAR-R7/UploadAboutPdf" method="POST"
				enctype="multipart/form-data">
				<div class="row">
					<div class="subtitle">About Us (Main Content) (add PDF)</div>
					<div class="col-md-12">
						<h4>Title</h4>
					</div>
					<div class="col-md-12">
						<input type="text" name="title" required class="form-control"
							onsubmit="uploading();" />
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h4>Order</h4>
					</div>
					<div class="col-md-3">
						<input type="text" name="sequence" id="sequence" class="form-control"
							value="<%=seq%>"
							aria-describedby="name-format" aria-required="true"
							pattern="[0-9]+" required
							oninvalid="setCustomValidity('Please Enter Number Only')"
							onchange="try{setCustomValidity('')}catch(e){}" />
					</div>
				</div>
				<div class="col-md-12">
					<h4>PDF File</h4>
				</div>
				<div class="col-md-12">
					<label>Choose a file : </label>
				</div>
				<div class="col-md-12">
					<input id="uploadPDF" type="file" name="file" class="btn btn-primary"
						onchange="PreviewImage(this.id,'viewer');" />&nbsp;
				</div>
				<div class="col-md-12">
					<iframe id="viewer" frameborder="0" scrolling="no"></iframe>
				</div>
				<div class="col-md-12">
					<br> <input type="submit" class="btn btn-primary"
						name="btnsaveapdf" onclick="return insertPdf('uploadPDF')"
						value="Save Content" />
				</div>
			</form>


		</div>
	</div>
</div>
<div id="result">
	<h3>${requestScope["message"]}</h3>
	<br>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
