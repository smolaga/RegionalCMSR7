<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
HomepageDao dao = new HomepageDao();
Homepage title = dao.findById(request.getAttribute("tid").toString());
Homepage htmlmaincontent = dao.findById(request.getAttribute("htmlmcid").toString());
if(request.getAttribute("changecontenttype")!=null){
    htmlmaincontent.setContentType("html");
}
%>

<style type="text/css">
#tacontent {
	height: 300px;
}
</style>
<div class="row">
	<div class="col-md-12 text-right"><i><b><a href="/BFAR-R7/HomepageContentUpdate?id=<%=htmlmaincontent.getId()%>&status=updateall&columnType=left&cctype=cctpdf">change content type to pdf?</a></b></i></div>
	<div class="col-md-12">&nbsp;</div>
	<form method="POST" action="/BFAR-R7/HomepageLeftContent" onsubmit="uploading();">
		<div class="col-md-12">
			<label>Content Title</label>
		</div>
		<div class="col-md-12">
			<input type="text" name="htmlcontenttitle" class="form-control" value="<%=htmlmaincontent.getHead()%>"/>
			<input type="hidden" name="htmlcontentid" value="<%=htmlmaincontent.getId()%>" />
			<input type="hidden" name="htmlpagelink" value="<%=htmlmaincontent.getPageLink()%>" />
			<input type="hidden" name="htmlcolumntype" value="<%=htmlmaincontent.getColumnType()%>" />
			<input type="hidden" name="htmlcontenttype" value="<%=htmlmaincontent.getContentType()%>" />
			<input type="hidden" name="htmlstatus" value="<%=htmlmaincontent.isStatus()%>" />
			<input type="hidden" name="htmlviewonpage" value="<%=htmlmaincontent.isViewonpage()%>" />
			<input type="hidden" name="htmlviewonlist" value="<%=htmlmaincontent.isViewonlist()%>" />
		</div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<label>Short Content</label>
		</div>
		<div class="col-md-3">
			<textarea id="uphtmlshortcontent" style="height: 300px;" class="mceEditor" name="htmlshortcontent"><%=htmlmaincontent.getShortContent()%></textarea>
		</div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12">
			<label>Content</label>
		</div>
		<%if(htmlmaincontent.getContent() == null){
		    htmlmaincontent.setContent("");}%>
		<div class="col-md-12">
			<textarea id="uphtmlcontent" style="height: 500px;" class="mceEditor" name="htmlcontent"><%=htmlmaincontent.getContent()%></textarea>
		</div>
		<div class="col-md-12">&nbsp;
			<input type="hidden" name="titleid" value="<%=title.getId()%>" />
			<input type="hidden" name="title" value="<%=title.getHead()%>" />
			<input type="hidden" name="sequence" value="<%=title.getSequence()%>" />
			<input type="hidden" name="columnType" value="<%=title.getColumnType()%>" />
		</div>
		<div class="col-md-2" align="left">
			<input type="hidden" name="action" value="updatehtml">
			<input type="submit" value="Submit HTML" name="categoryaction" class="btn btn-primary">
		</div>
	</form>
		<div class="col-md-1">
			<input type="submit" class="btn btn-primary" onclick="goBack()"  value="Cancel" />
		</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>