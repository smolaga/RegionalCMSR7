<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <div class="row">
<div class="wrapper">			
				<div class="row">
			<form method="POST" action="/BFAR-R7/HomepageLeftSubContent" onsubmit="uploading();">
					<div class="col-md-12">
						<label>Content Title</label>
					</div>
					<div class="col-md-12">
						<input type="text" name="htmlsubcontenttitle" class="form-control" />
						<input type="hidden" name="htmlsubcolumntype" class="form-control" value="left"/>
						<input type="hidden" name="htmlsubcontenttype" class="form-control" value="subhtml"/>
						<input type="hidden" name="htmlsubstatus" class="form-control" value="false"/>
					</div>
					<div class="col-md-12">&nbsp;</div>
					<div class="col-md-12">
						<label>Content</label>
					</div>
					<div class="col-md-12">
						<textarea id="tacontent" style="height:500px" class="mceEditor" name="htmlsubcontent"></textarea>
					</div>
					<div class="col-md-12">&nbsp;
					</div>
					<div class="col-md-2" align="left">
						<input type="hidden" name="action" value="insertsubhtml">
						<input type="submit" value="Submit HTML" name="categoryaction" class="btn btn-primary">
					</div>
			</form>
					<div class="col-md-1">
						<input type="submit" class="btn btn-primary" onclick="goBack()"  value="Cancel" />
					</div>
		</div>
				
	</div>
</div>

<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>