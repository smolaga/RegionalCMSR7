<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div class="row" style="background-color: white; box-shadow: 0 3px 3px 1px #000000">
	<div class="col-md-12">&nbsp;</div>
	<div class="col-md-3">
		<div class="panel panel-default">
			<div class="panel-body">
				<ul class="nav nav-pills nav-stacked" id="myTabs">
					<li class="active bg-info"><a href="#banner" data-toggle="pill"><strong>Banner</strong></a></li>
					<li class="bg-info"><a href="#slider" data-toggle="pill"><strong>Slider</strong></a></li>
					<li class="bg-info"><a href="#homepageleft" data-toggle="pill"><strong>Homepage Left</strong></a></li>
					<li class="bg-info"><a href="#homepagecenter" data-toggle="pill"><strong>Homepage Center</strong></a></li>
					<li class="bg-info"><a href="#homepageright" data-toggle="pill"><strong>Homepage Right</strong></a></li>
					<li class="bg-info"><a href="#aboutus" data-toggle="pill"><strong>About Us</strong></a></li>
					<li class="bg-info"><a href="#services" data-toggle="pill"><strong>Services</strong></a></li>
					<li class="bg-info"><a href="#image" data-toggle="pill"><strong>Gallery Image</strong></a></li>
					<li class="bg-info"><a href="#videos" data-toggle="pill"><strong>Gallery Videos</strong></a></li>
					<li class="bg-info"><a href="#profile" data-toggle="pill"><strong>Fisheries Profile</strong></a></li>
					<li class="bg-info"><a href="#downloads" data-toggle="pill"><strong>Downloads</strong></a></li>					
					<li class="bg-info"><a href="#laws" data-toggle="pill"><strong>Laws and Regulation</strong></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="tab-content">
					<div class="bg-info tab-pane active" id="banner">
						<jsp:include page="../banner/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="slider">
						<jsp:include page="../slider/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="homepageleft">
						<jsp:include page="../homepageleft/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="homepagecenter">
						<jsp:include page="../homepagecenter/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="homepageright">
						<jsp:include page="../homepageright/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="aboutus">
						<jsp:include page="../aboutus/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="services">
						<jsp:include page="../services/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="image">
						<jsp:include page="../gallery/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="videos">
						<jsp:include page="../video/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="profile">
						<jsp:include page="../profile/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="downloads">
						<jsp:include page="../download/files.jsp"></jsp:include>
					</div>
					<div class="bg-info tab-pane" id="laws">
						<jsp:include page="../laws/files.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>