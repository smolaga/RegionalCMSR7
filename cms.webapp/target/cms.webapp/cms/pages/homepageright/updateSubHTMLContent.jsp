<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
HomepageDao dao = new HomepageDao();
Homepage htmlsubcontent = dao.findById(request.getAttribute("subhtmlid").toString());
%>
 <div class="panel panel-default">
	<!-- <div class="panel-heading">
		
	</div> -->
	<div class="row" style="background-color:white">
	<div class="panel-body">
		<form method="POST" action="/BFAR-R7/HomepageRightSubContent" onsubmit="uploading();">
			<div class="row">
				<div class="col-md-2">
					<label>Content Title</label>
				</div>
				<div class="col-md-4">
					<input type="text" required name="htmlsubcontenttitle" class="form-control" value="<%=htmlsubcontent.getHead()%>"/>
					<input type="hidden" name="htmlsubcontentid" value="<%=htmlsubcontent.getId()%>" />
					<input type="hidden" name="htmlsubpagelink" value="<%=htmlsubcontent.getSubContentLink()%>" />
					<input type="hidden" name="htmlsubcolumntype" value="<%=htmlsubcontent.getColumnType()%>" />
					<input type="hidden" name="htmlsubcontenttype" value="<%=htmlsubcontent.getContentType()%>" />
					<input type="hidden" name="htmlsubstatus" value="<%=htmlsubcontent.isStatus()%>" />
				</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-12">
					<label>Content</label>
				</div>
				<div class="col-md-12">
					<textarea id="tahtmlcontent" style="height: 500px" class="mceEditor" name="htmlsubcontent"><%=htmlsubcontent.getContent()%></textarea>
				</div>
				<div class="col-md-12">&nbsp;
				</div>
				<div class="col-md-12" align="left">
					<input type="hidden" name="action" value="updatesubhtml">
					<input type="submit" value="Submit HTML" name="categoryaction" class="btn btn-primary">
				</div>
			</div>
		</form>
	</div>
</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>