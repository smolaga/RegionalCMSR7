<%@page import="gov.bfar.cms.dao.VideoDao"%>
<%@page import="gov.bfar.cms.webapp.util.FileUploadUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String videoformat = FileUploadUtil.getConfig("videoformat");
	String videosize = FileUploadUtil.getConfig("videosizelimit");
	VideoDao dao = new VideoDao();
	int seq = dao.getSuggestedSequence();
%>
<script type="text/javascript">
function PreviewImage() {
   videofile=document.getElementById("uploadVideo").files[0];
    videofile_url=URL.createObjectURL(videofile);
    $('#viewer').attr('src',videofile_url);
    $('#viewer').attr('width','100%');
    $('#viewer').attr('height','800px');
}
function uploadingvideo() { 
	var fileInput = $('#uploadVideo');
	var maxSize = '<%=videosize%>'
	if(fileInput.get(0).files.length){
        var fileSize = fileInput.get(0).files[0].size; // in bytes
        if(fileSize>maxSize){
            alert('file size is more then ' + (maxSize/1024) + ' mb');
            return false;
        }else{
        	$('#btnupload').click();
            document.getElementById('loader').style.display='block';
        }
    }else{
        alert('choose file, please');
        return false;
    }
}
$(document).ready(function() {
	$(function() {
	     $("#uploadVideo").change(function (){
	       PreviewImage();
	     });
	  });
});
</script>
<div class="row">
	<div class="wrapper">
		<div class="row">
			<form action="/BFAR-R7/UploadVideo" method="POST" enctype="multipart/form-data" onsubmit="return uploadingvideo();">
					<div class="col-md-12">
						<h4>Title</h4>
					</div>
					<div class="col-md-12">
						<input type="text" name="title" required class="form-control"/>
					</div>
					<div class="col-md-12">
						<h4>Order</h4>
					</div>
					<div class="col-md-3">
						<input type="text" name="sequence" class="form-control" id="sequence" value=<%=seq%>
						aria-describedby="name-format" aria-describedby="name-format" aria-required="true" pattern="[0-9]+" required oninvalid="setCustomValidity('Please Enter Number Only')"
					 onchange="try{setCustomValidity('')}catch(e){}"/>
					</div>
					<div class="col-md-12">
						<h4>Video File</h4>
					</div>
					<div class="col-md-12">
						<label>Choose a file : </label>							
					</div>
					<div class="col-md-12">
						<input id="uploadVideo" type="file" name="file" onchange="PreviewImage(this.id,'viewer');" class="btn btn-primary"/>&nbsp;
						<label style="color:red"></label>
					</div>
					<div class="col-md-12">
						<iframe id="viewer" frameborder="0" scrolling="no" ></iframe>
					</div>
					<div class="col-md-12">
						<br><input type="submit" class="btn btn-primary" name="btnsaveapdf" value="Save Content" onclick="return insertPdf('uploadVideo')"/>
					</div>
			</form>	
		</div>
	</div>
</div>
<div id="result">
	<h3>${requestScope["message"]}</h3>
	<br>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>