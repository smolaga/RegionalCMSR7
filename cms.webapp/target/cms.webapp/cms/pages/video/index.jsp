<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.VideoDao"%>
<%@page import="gov.bfar.cms.domain.Video"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
%>
<div class="row" style="background-color:white;  box-shadow: 0 3px 3px 1px #000000">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12" style="margin-top:20px"><div class="headtitle">Videos</div></div>
				<div class="col-md-12" style="margin-bottom:20px">
					<a href="/BFAR-R7/CMSNavigation?type=video&pageAction=insertContent"><input type="submit" class="btn btn-primary" value="Add Videos" ></a>
				</div>

<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
	<table class="table table-hover table-bordered dataTables">
		<thead>
			<tr>			
				<td>Title</td>
				<td>Date Created</td>
				<td>Order</td>					
				<td>Status</td>
			<% if (user.getUserType().equalsIgnoreCase("Administrator"))
				{
			%>
				<td>View in Page</td>
			<%} %>
				<td>Update</td>
				<td>Delete</td>
			</tr>
		</thead>
		<tbody>
			<%
				VideoDao videoDao = new VideoDao();
				List<Video>video = videoDao.videoFindAllContent();
			%>
			<%
				for (Video videos : video) {
			%>
			<tr>
			
				<td><%=videos.getTitle() %></td>
				<td><%=videos.getDateCreated() %></td>
				<td class="text-center"><%=videos.getSequence() %></td>
				<%if(videos.isStatus()){%>
				    <td class="text-center">Active</td>
						<%}else{%>
								<td class="text-center">Inactive</td>
						<%}%>
			<% if (user.getUserType().equalsIgnoreCase("Administrator"))
				{
			%>
				<%if(videos.isStatus()){%>
					<td class="text-center">	
						<a href="/BFAR-R7/VideoUpdateStatus?contentid=<%=videos.getId()%>&status=hide"><abbr title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a>
					</td> 
					<%} else{%>
					<td class="text-center">	
						<a href="/BFAR-R7/VideoUpdateStatus?contentid=<%=videos.getId()%>&status=show"><abbr title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a>
					</td>
					<%} %>
			<%} %>
				<td class="text-center"><a href="/BFAR-R7/VideoUpdateStatus?contentid=<%=videos.getId()%>&status=updateall"><input
							type="submit" class="btn btn-primary" value="Update"></a></td>
				<td class="text-center"><a href="/BFAR-R7/VideoUpdateStatus?contentid=<%=videos.getId()%>&status=delete">
					<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
			</tr>
			<%
				}
			%>
			</tbody>
			
	</table>
	</div>
	</div>
	</div>	
	</div>
	</div>
</div>