<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.domain.User"%>

<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
%>
<%
HomepageDao dao = new HomepageDao();
Homepage title = dao.findById(request.getAttribute("tid").toString());
System.out.println("TITLE ID: "+title.getId());
String at;
	if(request.getAttribute("inserttabactive") == null){
	    at = null;
	}else{
	    at = request.getAttribute("inserttabactive").toString();
	}
String uphtml, uppdf, upcon;
	if(request.getAttribute("updatemainhtmlpage") == null){
	    uphtml = "";
	}else{
	    uphtml = request.getAttribute("updatemainhtmlpage").toString();
	}
	if(request.getAttribute("updatemainpdfpage") == null){
	    uppdf = "";
	}else{
	    uppdf = request.getAttribute("updatemainpdfpage").toString();
	}
	
	if(uphtml.equalsIgnoreCase("") && uppdf.equalsIgnoreCase("") ){
	    upcon = "\"hide\"";
	}else{
	    upcon = null;
	}

%>

<script type="text/javascript">
(function($) {
	$(document).ready(function() {
$(function() {
     function overlay() {
 		el = document.getElementById("overlay");
 		el.style.visibility = (el.style.visibility == "visible") ? "hidden"
 				: "visible";
 	}
     
     var cat = <%=at%>;
 	if (cat == html  || cat == null) {
 		$('#htmltab').addClass("active");
 		$('#html').addClass("in active");
 		$('#pdftab').removeClass("active");
 	 	$('#pdf').removeClass("in active");
 	} else{
 		$('#htmltab').removeClass("active");
 		$('#html').removeClass("in active");
 		$('#pdftab').addClass("active");
 	 	$('#pdf').addClass("in active");
 	 	
 	}
  });

var uc = <%=upcon%>;
if(uc == null){
	 $('#CenterInsertContent').hide();
	 $('#myTab').hide();
}
	});
})(jQuery);

</script>
<div class="panel panel-default">
	<!-- <div class="panel-heading">
		
	</div> -->
	<div class="row" style="background-color:white; box-shadow: 0 3px 3px 1px #000000">
	<div class="panel-body">
		<div class="row">
			<form method="POST" action="/BFAR-R7/HomepageCenterContent" onsubmit="uploading();">
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-12">
					<label>Main Title</label>
				</div>
				<div class="col-md-12">
					<input type="text" name="title"   class="form-control" value="<%=title.getHead()%>" />
					<input type="hidden" name="titleid"  value="<%=title.getId()%>" />
					<input type="hidden" name="titlestatus" value="<%=title.isStatus()%>" />
				</div>
				<div class="col-md-12">
					<label>Order</label>
				</div>
				<div class="col-md-3">
					<input type="hidden" name="oldsequence" value="<%=title.getSequence()%>" /> 
					<input type="text" name="sequence" class="form-control" value="<%=title.getSequence()%>" 
					aria-describedby="name-format" aria-required="true" pattern="[0-9]+" required oninvalid="setCustomValidity('Please Enter Number Only')"/>
				</div>
				<input type="hidden" name="columnType" class="form-control" value="<%=title.getColumnType()%>" />
				<div class="col-md-1">
					<input type="hidden" name="action" value="updatetitle">
					<input type="submit" class="btn btn-primary" value="Update" />
				</div>
			</form>
				<div class="col-md-1">
					<input type="submit" class="btn btn-primary" onclick="goBack()"  value="Cancel" />
				</div>
		</div>
		<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
				<table class="table table-hover table-bordered dataTables">
					<thead>
						<tr>
							<td>TITLE</td>
							<td>DATE CREATED</td>
							<td>CONTENT TYPE</td>
						<% if (user.getUserType().equalsIgnoreCase("Posts User"))
							{
						%>
							<td>ACTIVE IN HOMEPAGE</td>
							<td>ACTIVE IN LIST</td>
						<%} %>
						<% if (user.getUserType().equalsIgnoreCase("Administrator"))
							{
						%>
							<td>VIEW ON HOMEPAGE</td>
							<td>VIEW ON LIST</td>
						<%} %>
							<td>UPDATE</td>
							<td>COPY LINK</td>
							<td>DELETE</td>
						</tr>
					</thead>
					<tbody>
					<%
					String headId = title.getId();
					HomepageDao mcenterlist = new HomepageDao();
					List<Homepage> mll = mcenterlist.findAllContent("center", "html", "pdf", headId);
					for(Homepage lm : mll){
					%>
						<tr>
							<td><%=lm.getHead()%></td>
							<td><%=lm.getDateCreated()%></td>
							<%if(lm.getContentType().equalsIgnoreCase("pdf")){%>
						<td class="text-center">
						<img src="/BFAR-R7/cms/media/img/updatePDF.png">
						</td>
						<%}else if(lm.getContentType().equalsIgnoreCase("html")){ %>
						<td class="text-center">
						<img src="/BFAR-R7/cms/media/img/updateHTML.png">
						</td>						
						<%} %>
						<% if (user.getUserType().equalsIgnoreCase("Posts User"))
							{
						%>
							<td><%=lm.isViewonpage()%></td>
							<td><%=lm.isViewonlist()%></td>
						<%} %>
						<% if (user.getUserType().equalsIgnoreCase("Administrator"))
							{
						%>
							<%if (lm.isViewonpage()) {%>
							<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=lm.getId()%>&status=hide&activeat=vop&columnType=center">
							<abbr title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a></td>
							<%} else {%>
							<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=lm.getId()%>&status=show&activeat=vop&columnType=center">
							<abbr title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a></td>
							<%}%>
							<%if (lm.isViewonlist()) {%>
							<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=lm.getId()%>&status=hide&activeat=vol&columnType=center">
							<abbr title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a></td>
							<%} else {%>
							<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=lm.getId()%>&status=show&activeat=vol&columnType=center">
							<abbr title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a></td>
							<%}%>
						<%} %>
							<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=lm.getId()%>&status=updateall&columnType=center">
								<input type="submit" class="btn btn-primary" value="Update"></a></td>
							<td class="text-center"><button class="btn btn-primary btnCopy" data-toggle="popover" data-content="Copied!" data-clipboard-text="<%=lm.getPageLink()%>">Copy</button></td>
							<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=lm.getId()%>&status=delete&columnType=center">
							<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>	
						</tr>
					<%} %>
					</tbody>
				</table>
				</div>				
			</div>
			<div class="col-md-12">&nbsp;</div>
		</div>
		<div class="row" >
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<ul id="myTab" class="nav nav-tabs">
							<li id="htmltab" class="btn btn-primary"><a href="#html" data-toggle="tab">HTML</a></li>
							<li id="pdftab" class="btn btn-primary"><a href="#pdf" data-toggle="tab">PDF</a></li>
						</ul>
					</div>
					<div class="panel-body">
					<div id="CenterInsertContent">
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane fade in active" id="html">
								<form method="POST" action="/BFAR-R7/HomepageCenterContent" onsubmit="uploading();">
									<div class="row">
										<div class="col-md-2">
											<label>Content Title</label>
										</div>
										<div class="col-md-4">
											<input type="text"   name="htmlcontenttitle" class="form-control" />
										</div>
										<div class="col-md-12">&nbsp;</div>
										<div class="col-md-12">
											<label>Short Content</label>
										</div>
										<div class="col-md-6">
											<textarea id="htmlshortcontent"   style="height: 300px;" class="mceEditor" name="htmlshortcontent"></textarea>
										</div>
										<div class="col-md-12">&nbsp;</div>
										<div class="col-md-12">
											<label>Content</label>
										</div>
										<div class="col-md-12">
											<textarea id="htmlcontent" style="height: 300px;" class="mceEditor" name="htmlcontent"></textarea>
										</div>
										<div class="col-md-12">&nbsp;
											<input type="hidden" name="titleid" value="<%=title.getId()%>" />
											<input type="hidden" name="title" value="<%=title.getHead()%>" />
											<input type="hidden" name="sequence" value="<%=title.getSequence()%>" />
											<input type="hidden" name="columnType" value="<%=title.getColumnType()%>" />
										</div>
										<div class="col-md-12" align="left">
											<input type="hidden" name="action" value="inserthtml">
											<input type="submit" value="Submit HTML" name="categoryaction" class="btn btn-primary">
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane fade" id="pdf">
								<form method="POST" action="/BFAR-R7/HomepageCenterContent?action=insertpdf&sequence=<%=title.getSequence()%>" enctype="multipart/form-data" onsubmit="uploading();">
									<div class="row">
										<div class="col-md-2">
											<label>Content Title</label>
										</div>
										<div class="col-md-4">
											<input type="text"    name="pdfcontenttitle" class="form-control" />
										</div>
										<div class="col-md-12">&nbsp;</div>
										<div class="col-md-12">
											<label>Short Content</label>
										</div>
										<div class="col-md-3">
											<textarea id="pdfshortcontent"   style="height: 300px;" class="mceEditor" name="pdfshortcontent"></textarea>
										</div>
										<div class="col-md-12">&nbsp;</div>
										<div class="col-md-12">
											<label>Content</label>
										</div>
										<div class="col-md-2">
											<label>Choose a file : </label>
										</div>
										<div class="col-md-10">
											<input id="uploadPDF" type="file" name="pdffile" onchange="PreviewImage(this.id,'viewer');" class="btn btn-primary"/>
										</div>
										<div class="col-md-12">
											<iframe id="viewer" frameborder="0" scrolling="no"></iframe>
										</div>
										<div class="col-md-12">&nbsp;
											<input type="hidden" name="titleid" value="<%=title.getId()%>" />
											<input type="hidden" name="title" value="<%=title.getHead()%>" />
											<input type="hidden" name="sequence" value="<%=title.getSequence()%>" />
											<input type="hidden" name="columnType" value="<%=title.getColumnType()%>" />
										</div>
										<div class="col-md-12" align="left">
											<input type="hidden" name="action" value="insertpdf">
											<input type="submit" value="Submit PDF" name="categoryaction" class="btn btn-primary" onclick="return insertPdf('uploadPDF')">
										</div>
									</div>
								</form>
							</div>
						</div>
						</div>
						<div id="CenterUpdateContent">
							<% if(uphtml != ""){%>
							<div id="updateHTML" class="">
							    <jsp:include page="<%=uphtml%>"></jsp:include>
							</div>
							<% } %>
							<% if(uppdf != ""){%>
							<div id="updatePDF" class="">
							    <jsp:include page="<%=uppdf%>"></jsp:include>
							</div>
							<% } %>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>