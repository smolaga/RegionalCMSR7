<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%
	HomepageDao dao = new HomepageDao();
	Homepage pdfsubcontent = dao.findById(request.getAttribute("subpdfid").toString());
%>
 <div class="panel panel-default">
	<!-- <div class="panel-heading">
		
	</div> -->
	<div class="row" style="background-color:white; box-shadow: 0 3px 3px 1px #000000">
	<div class="panel-body">
		<form method="POST" action="/BFAR-R7/HomepageCenterSubContent?action=updatesubpdf" enctype="multipart/form-data" onsubmit="uploading();">
			<div class="row">
				<div class="col-md-2">
					<label>Content Title</label>
				</div>
				<div class="col-md-4">
					<input type="text" name="pdfsubcontenttitle" class="form-control" value="<%=pdfsubcontent.getHead()%>"/>
					<input type="hidden" name="pdfsubcontentid" class="form-control" value="<%=pdfsubcontent.getId()%>"/>
					<input type="hidden" name="pdfsubcontentlink" class="form-control" value="<%=pdfsubcontent.getSubContentLink()%>"/>
					<input type="hidden" name="pdfsubcolumntype" class="form-control" value="<%=pdfsubcontent.getColumnType()%>"/>
					<input type="hidden" name="pdfsubcontenttype" class="form-control" value="<%=pdfsubcontent.getContentType()%>"/>
					<input type="hidden" name="pdfsubfilepath" class="form-control" value="<%=pdfsubcontent.getPdfFilePath()%>"/>
					<input type="hidden" name="pdfsubstatus" class="form-control" value="<%=pdfsubcontent.isStatus()%>"/>
					<input type="hidden" name="pdffileupdatestatus" id="pdffileupdatestatus" class="form-control" value="false"/>
				</div>
				<div class="col-md-12">
					<label>Choose a file : </label>
				</div>
				<div class="col-md-12">
					<input id="uploadPDFUpdate" type="file" name="pdffile" onchange="PreviewImage(this.id,'viewerup')" class="btn btn-primary" />
				</div>
				<div class="col-md-12" id="dvpdf">
					<iframe id="viewerUpdate" title="PDF in an i-Frame" src="<%=pdfsubcontent.getPdfFilePath() %>" frameborder="0" scrolling="no" height="1100" width="100%" ></iframe>
				</div>
				 <div class="col-md-12" id="dvpdfupdate">
					<iframe id="viewerup" frameborder="0" scrolling="no"></iframe>
				</div> 
				<div class="col-md-12">&nbsp;
				</div>
				<div class="col-md-12" align="left">
					<input type="hidden" name="action" value="updatesubpdf">
					<input type="submit" value="Submit PDF" name="categoryaction" class="btn btn-primary">
				</div>
			</div>
		</form>
	</div>
	</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>