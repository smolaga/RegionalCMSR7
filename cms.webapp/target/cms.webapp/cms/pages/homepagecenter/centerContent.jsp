<%-- <%
String at;
if(request.getAttribute("tabactive") == null){
    at = null;
}else{
    at = request.getAttribute("tabactive").toString();
}
%> --%>
<script type="text/javascript">
$(document).ready(function() {
var cat = ${tabactivecenter};
if (cat == main  || cat == '') {
	$('#maintab').addClass("active");
	$('#main').addClass("in active");
	$('#subtab').removeClass("active");
 	$('#sub').removeClass("in active");
} else{
	$('#maintab').removeClass("active");
	$('#main').removeClass("in active");
	$('#subtab').addClass("active");
 	$('#sub').addClass("in active");
 	
}
});	
</script>
<% session.removeAttribute("tabactivecenter"); %>
<div class="row" style="background-color:white;box-shadow: 0 3px 3px 1px #000000">
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<ul id="myTab" class="nav nav-tabs">
					<li id="maintab" class="btn btn-primary"><a href="#main" data-toggle="tab">Main Content</a></li>
					<li id="subtab" class="btn btn-primary"><a href="#sub" data-toggle="tab">Sub Content</a></li>
				</ul>
			</div>
			<div class="panel-body">
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade in active" id="main">
						<jsp:include page="centerTitleList.jsp"></jsp:include>
					</div>
					<div class="tab-pane fade" id="sub">
						<jsp:include page="centerSubContentList.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
