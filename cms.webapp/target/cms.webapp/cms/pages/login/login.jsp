<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>RFO-CMS LOGIN</title>

        <!-- CSS -->
        <style type="text/css">
        @font-face {
		  font-family: 'Roboto';
		  font-style: normal;
		  font-weight: 100;
		  src: local('Roboto Thin'), local('Roboto-Thin'), url(http://fonts.gstatic.com/s/roboto/v15/2tsd397wLxj96qwHyNIkxPesZW2xOQ-xsNqO47m55DA.woff2) format('woff2');
		}
		@font-face {
		  font-family: 'Roboto';
		  font-style: normal;
		  font-weight: 300;
		  src: local('Roboto Light'), local('Roboto-Light'), url(http://fonts.gstatic.com/s/roboto/v15/Hgo13k-tfSpn0qi1SFdUfVtXRa8TVwTICgirnJhmVJw.woff2) format('woff2');
		}
		@font-face {
		  font-family: 'Roboto';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Roboto'), local('Roboto-Regular'), url(http://fonts.gstatic.com/s/roboto/v15/CWB0XYA8bzo0kSThX0UTuA.woff2) format('woff2');
		}
		@font-face {
		  font-family: 'Roboto';
		  font-style: normal;
		  font-weight: 500;
		  src: local('Roboto Medium'), local('Roboto-Medium'), url(http://fonts.gstatic.com/s/roboto/v15/RxZJdnzeo3R5zSexge8UUVtXRa8TVwTICgirnJhmVJw.woff2) format('woff2');
		}
        </style>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cms/media/css/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cms/media/css/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/cms/media/css/assets/css/form-elements.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/cms/media/css/assets/css/style.css">
<%
	session.invalidate();
	String invalid = "";
	if (request.getAttribute("invalidpw") == null) {
		invalid = " ";
	} else {
		invalid = request.getAttribute("invalidpw").toString();
	}
%>
        

    </head>

    <body>

        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
               
                     <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 text mobile-only ">
                         
                            <div class="description">
                            	
                            </div>
                        </div>
                     <div class="mainpic">
                    	<img src="${pageContext.request.contextPath}/cms/media/img/headername.png" alt="" class="img-responsive" style="margin-left:60px; margin-top:-50px;">
                    	</div>
                    	
                    </div> 
                  
                    <div class="row">
                    	
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        
                        	 <div class="form-top" style="box-shadow: 0 5px 5px 3px #000000">
                        	 <br>             	 
                        		<img src="${pageContext.request.contextPath}/cms/media/img/cmslog.png" alt="" class="img-responsive">
                           <br>
			                  
			                     <form method="POST" action="/BFAR-R7/authenticate"  role="form">
			                     <div class="form-group">						
						<input type="text" class="form-control" placeholder="Username" name="username" >
						</div>
						<div class="form-group">	
						<input type="password" class="form-control" placeholder="Password" name="userpass" >
						</div>
			                    	
			                        <label><%=invalid %></label>
						<input type="submit" value="Login" class="btn btn-success">
			                        <br>
			                        <br>
			                        <br>
			                       
			                    </form>
			                    
			                  
		                  </div> 
                        </div>
                    </div>
                
                    <br>
                    <br>
                  <div class="copy-right">
				<p><strong>Copyright @ 2016 All rights reserved - BUREAU OF FISHERIES AND AQUATIC RESOURCES</strong></p>
			</div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="${pageContext.request.contextPath}/cms/media/css/assets/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/cms/media/css/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/cms/media/css/assets/js/jquery.backstretch.min.js"></script>
        <script src="${pageContext.request.contextPath}/cms/media/css/assets/js/scripts.js"></script>
        
        
    </body>

</html>