<%@page import="gov.bfar.cms.domain.User"%>
<%@page import="gov.bfar.cms.dao.UserDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/text_editor.js"></script>	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
	<% String id = (String) request.getAttribute("getidcontent");
	UserDao userDao = new UserDao();
	User user = userDao.findById(id);
	%>
<body>

<script type="text/javascript">
	$(document).ready(function(){
		$('#type').val('<%=user.getUserType() %>')
	});
	
	$(document).ready(function(){
		$('#question').val('<%=user.getSecretQuestion() %>')
	});
	
</script>

<div class="row">
	<div class="wrapper">
		<form method="POST" action="/BFAR-R7/UpdateUserAccount">
		<input type="hidden" value="<%=user.getId()%>" name="id">
		<input type="hidden" name="createdby" value="<%=user.getCreatedBy() %>"/>
			<div class="row">
				<div class="col-md-12">
					<h4>First Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="firstName" class="form-control" value="<%=user.getFirstname()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Middle Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="middleName" class="form-control"  value="<%=user.getMiddlename()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Last Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="lastName" class="form-control" value="<%=user.getLastname()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Designation:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="designation" class="form-control" value="<%=user.getDesignation()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Select Type of User:</h4>
				</div>
				<div class="col-md-8">
					<select name="userType" id="type" class="form-control">
					  <option value="0">--Select--</option>
					  <option value="Administrator">Administrator</option>
					  <option value="Posts User">Posts User</option>
					 </select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>User Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="userName" class="form-control" value="<%=user.getUsername()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Password:</h4>
				</div>
				<div class="col-md-8">
					<input type="password" name="userPassword" class="form-control" value="<%=user.getUserPassword()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4 class="hidden">Select Secret Question:</h4>
				</div>
				<div class="col-md-8">
					<select class="hidden" name="secretQuestion" id="question" class="form-control">
					  <option value="0">--Select--</option>
					  <option value="What is your favorite pet">What is your favorite pet?</option>
					  <option value="What is your youngest brother's birthday">What is your youngest brother's birthday?</option>
					  <option value="When you were young, what did you want to be when you grew up"> When you were young, what did you want to be when you grew up?</option>
					  <option value="What was your childhood nickname">What was your childhood nickname?</option>
					  <option value="What is your oldest cousin's first and last name">What is your oldest cousin's first and last name?</option>
					 </select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4 class=""hidden>Answer</h4>
				</div>
				<div class="col-md-8">
					<input class="hidden" type="text" name="answer" class="form-control" value="<%=user.getAnswer()%>"/>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsaveleft" value="Update Content" />
				</div>
			</div>
		</form>
	</div>
	</div>
	
</body>
</html>

<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>