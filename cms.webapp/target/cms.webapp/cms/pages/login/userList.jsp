<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.UserDao"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
	<div class="row">
	<div class="wrapper">

<div class="col-md-12"><div class="headtitle">User Lists</div></div>

<div class="row">
<div class="col-md-12">
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<td>Name</td>
				<td>User Type</td>
				<td>Update</td>
				<td>Remove</td>
			</tr>
		</thead>
		<tbody>

		 <%
			UserDao userDao = new UserDao();
				List<User> user = userDao.findAll();
			%>
			<%
			
			for(User user2 : user){
				
			%>
			<tr>
				<td><%=user2.getFirstname() + " " + user2.getMiddlename() + " "+ user2.getLastname() %></td>
				<td><%=user2.getDesignation()%></td>
				<td style="text-align: center"><a href="/BFAR-R7/AccountUpdateStatus?contentid=<%=user2.getId()%>&status=updateall"><input type="submit" class="btn btn-primary" value="update"></a></td>
				<td style="text-align: center"><a href="/BFAR-R7/DeleteUserAccount?id=<%=user2.getId()%>"><input type="button" class="btn btn-warning" value="delete"></a></td>
			</tr>
			<%
				}
			%> 
			</tbody>
			
	</table>
	</div>
</div>
</div>	
</div>
</body>
</html>