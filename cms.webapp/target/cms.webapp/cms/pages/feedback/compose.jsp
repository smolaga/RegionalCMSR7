<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<form method="POST" action="/BFAR-R7/SendEmailToMany" onsubmit="uploading();">
	<div class="row" style="background-color:white;box-shadow: 0 3px 3px 1px #000000">
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-2"><strong>Recipient Address : </strong></div>
		<div class="col-md-8"><textarea rows="1" class="form-control" name="recipient"></textarea></div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-2"><strong>Subject : </strong></div>
		<div class="col-md-8"><textarea rows="1" class="form-control" name="subject"></textarea></div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-2"><strong>Content : </strong></div>
		<div class="col-md-8"><textarea class="form-control" rows="5" name="message"></textarea></div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12"><input class="btn btn-primary" type="submit" value="SEND"></div>
		<div class="col-md-12">&nbsp;</div>
	</div>
</form>