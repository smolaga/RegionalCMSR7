<%@page import="gov.bfar.cms.domain.Footer"%>
<%@page import="gov.bfar.cms.dao.FooterDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript">
	$(document).ready(function(){
		var upstat = '${upfmin}'
		var title = '${ftitle}'
		var content= ''
		var id= '${fid}'
		if(upstat == "false"){
			$('#btnf').val("Save");
		}
		else{
			$('#ftitle').val(title);	
			/* $('#ftacontent').val(content); */
			$('#fid').val(id);
			$('#btnf').val("Update");
		}
	});
</script>

<form method="POST" action="/BFAR-R7/InsertFooter?type=cmsfooter" onsubmit="uploading();">
<div class="row" style="margin-left: 10px; margin-right: 10px">
	<div class="col-md-12">
		<div class="row">
			<div class="subtitle">CMS Footer</div>
				<div class="col-md-12">
					<h4>Title</h4>
				</div>
				<div class="col-md-12">
				<input type="hidden" id="fid" name="fid"/>
					<input type="text" name="ftitle" id="ftitle" required class="form-control"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Content</h4>
					<br>
					<textarea id="ftacontent" class="mceEditor" style="height: 300px" name="fcontent">${fcontent}</textarea>
				</div>
				<div class="col-md-2"><br></div>
			</div>
			<div class="row">
				<div class="col-md-12">
						<br><input type="submit" id="btnf" class="btn btn-primary" name="action" /><br><br>
						<label style="color:red">${insertfootermsg}</label>
						 <% session.removeAttribute("insertfootermsg"); %>
				</div>
			</div>
	</div>
</div>
</form>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <% session.removeAttribute("upfmin"); %>
 <% session.removeAttribute("ftitle"); %>
 <% session.removeAttribute("fcontent"); %>
 <% session.removeAttribute("fid"); %>