<script type="text/javascript">
$(document).ready(function() {
var at = '${tabactivesettings}'
if (at == 'emailinquiry'  || at == '') {
	$('#email').addClass("in active");
	$('#cmsbanner').removeClass("in active");
	$('#cmsfooter').removeClass("in active");
}else if(at == 'cmsbanner'){
	$('#email').removeClass("in active");
	$('#cmsbanner').addClass("in active");
	$('#cmsfooter').removeClass("in active");
}else if(at == 'cmsfooter'){
	$('#email').removeClass("in active");
	$('#cmsbanner').removeClass("in active");
	$('#cmsfooter').addClass("in active");
}
});	
</script>
<% session.removeAttribute("tabactivesettings"); %>
<div class="row" style="background-color:white;box-shadow: 0 3px 3px 1px #000000">
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<ul class="nav nav-tabs">
	                 <li class="btn btn-primary"><a href="#email" data-toggle="tab">Email Inquiry</a></li>
	                 <li class="btn btn-primary"><a href="#cmsbanner" data-toggle="tab">CMS Banner</a></li>
	                 <li class="btn btn-primary"><a href="#cmsfooter" data-toggle="tab">CMS Footer</a></li>
	             </ul>
			</div>
			<div class="panel-body">
				<div class="tab-content">
                     <div class="tab-pane fade in active" id="email">
                         <jsp:include page="EmailInquiry.jsp"></jsp:include>
                     </div>
                     <div class="tab-pane fade" id="cmsbanner">
                         <jsp:include page="CMSBanner.jsp"></jsp:include>
                     </div>
                     <div class="tab-pane fade" id="cmsfooter">
                        <jsp:include page="CMSFooter.jsp"></jsp:include>
                     </div>
                 </div>
			</div>
		</div>
	</div>
</div>
</div>