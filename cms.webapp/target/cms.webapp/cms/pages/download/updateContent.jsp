<%@page import="gov.bfar.cms.domain.Download"%>
<%@page import="gov.bfar.cms.dao.DownloadDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<% String id = (String) request.getAttribute("getidcontent");
DownloadDao downloadDao = new DownloadDao();
Download download = downloadDao.findById(id);
%>
<body>
<div class="row">
<div class="wrapper">
		<form method="POST" action="/BFAR-R7/UpdateDownloadContent" onsubmit="uploading();">
			<div class="row">
				<div class="col-md-12">
				<h4>Title</h4>
				</div>
				<div class="col-md-12">
					<input type="text" name="title" required class="form-control" value="<%=download.getTitle()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Order</h4>
				</div>
				<div class="col-md-3">
					<input type="text" name="sequence" value="<%=download.getSequence() %>" class="form-control"
						 aria-describedby="name-format" aria-required="true" pattern="[0-9]+" required oninvalid="setCustomValidity('Please Enter Number Only')"
						 onchange="try{setCustomValidity('')}catch(e){}"/>
				</div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col-md-12">
					<h4>Content</h4>
					<br>
					<textarea id="tacontent" class="mceEditor" style="height: 500px" name="content" ><%=download.getContent() %></textarea>
				</div>
			</div>
			<input type="hidden" name="id" value="<%=download.getId() %>"/>
			<input type="hidden" name="oldsequence" value="<%=download.getSequence() %>"/>
			<input type="hidden" name="status" value="<%=download.isStatus() %>"/>
			<input type="hidden" name="pagelink" value="<%=download.getPageLink() %>"/>
			<input type="hidden" name="createdby" value="<%=download.getCreatedBy() %>"/>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsavehleft" value="Save Content" />
				</div>
			</div>
		</form>
	</div>
	</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>