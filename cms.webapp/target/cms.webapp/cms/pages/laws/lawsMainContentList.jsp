<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.LawsDao"%>
<%@page import="gov.bfar.cms.domain.Laws"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
%>


<div class="row">
<div class="col-md-12" style="margin-bottom: 2%"><div class="headtitle">Laws and Regulation (Main Content)</div>
<a href="/BFAR-R7/CMSNavigation?type=laws&pageAction=insertContent"><input type="submit" class="btn btn-primary" value="Add HTML" ></a>
</div>
</div>

<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
<p style="color:blue"><i>Note: Only One HTML Content should be display.</i></p>
	<table class="table table-hover table-bordered dataTables">
		<thead>
			<tr>
				<td>Title</td>
				<td>Date Created</td>
				<td>Content Type</td>
				<td>Status</td>
			<% if (user.getUserType().equalsIgnoreCase("Administrator"))
					{
				%>
				<td>View in Page</td>
			<%} %>
				<td>Update</td>
				<td>Copy Link</td>
				<td>Delete</td>
			</tr>
		</thead>
		<tbody>
			<%
			LawsDao lawsDao = new LawsDao();
				List<Laws> laws = lawsDao.lawsContentList("html", "pdf");
			%>
			<%
			
			for(Laws laws2 : laws){
				
			%>
			<tr>
		
				<td><%=laws2.getTitle()%></td>
				<td><%=laws2.getDateCreated() %></td>
				<%if(laws2.getContentType().equalsIgnoreCase("pdf")){%>
						<td class="text-center">
						<img src="/BFAR-R7/cms/media/img/updatePDF.png">
						</td>
						<%}else if(laws2.getContentType().equalsIgnoreCase("html")){ %>
						<td class="text-center">
						<img src="/BFAR-R7/cms/media/img/updateHTML.png">
						</td>
						
						<%} %>
				<%if(laws2.isStatus()){%>
				    <td class="text-center">Active</td>
						<%}else{%>
								<td class="text-center">Inactive</td>
						<%}%>
			<% if (user.getUserType().equalsIgnoreCase("Administrator"))
					{
				%>
				<%if(laws2.isStatus()){%>
					<td class="text-center">	
						<a href="/BFAR-R7/LawsUpdateStatus?contentid=<%=laws2.getId()%>&status=hide"><abbr title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a>
					</td> 
					<%} else{%>
					<td class="text-center">	
						<a href="/BFAR-R7/LawsUpdateStatus?contentid=<%=laws2.getId()%>&status=show"><abbr title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a>
					</td>
					<%} %>
				<%} %>
				<td class="text-center"><a href="/BFAR-R7/LawsUpdateStatus?contentid=<%=laws2.getId()%>&status=updateall&contenttype=<%=laws2.getContentType() %>"><input type="submit" class="btn btn-primary" value="Update"></a></td>
				<td class="text-center"><button class="btn btn-primary btnCopy" data-toggle="popover" data-content="Copied!" data-clipboard-text="<%=laws2.getPageLink()%>">Copy</button></td>
				<td class="text-center"><a href="/BFAR-R7/LawsUpdateStatus?contentid=<%=laws2.getId()%>&status=delete">
					<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
			</tr>
			<%
				}
			%>
			</tbody>
			
	</table>
	</div>
	</div>
	</div>