<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<script type="text/javascript">
function PreviewImage() {
    pdffile=document.getElementById("uploadPDF").files[0];
    pdffile_url=URL.createObjectURL(pdffile);
    $('#viewer').attr('src',pdffile_url);
    $('#viewer').attr('width','100%');
    $('#viewer').attr('height','800px');
}
$(document).ready(function() {
	$(function() {
	     $("#uploadPDF").change(function (){
	       PreviewImage();
	     });
	  });
});
</script>

	<div class="row">
<div class="wrapper">
				<div class="row">
				<div class="subtitle">Laws and Regulation (Sub Content) (add PDF)</div>
					<form action="/BFAR-R7/UploadSubLawsPdf" method="POST"
						enctype="multipart/form-data" onsubmit="uploading();">
						<div class="col-md-12">
							<h4>Title</h4>
						</div>
						<div class="col-md-12">
							<input type="text" required name="title" class="form-control"/>
						</div>
						<div class="col-md-12">
							<h4>PDF File</h4>
						</div>
						<div class="col-md-12">
							<label>Choose a file : </label>							
						</div>
						<div class="col-md-12">
							<input id="uploadPDF" type="file" name="file" onchange="PreviewImage(this.id,'viewer');" class="btn btn-primary"/>&nbsp;
						</div>
						<div class="col-md-12">
							<iframe id="viewer" frameborder="0" scrolling="no" ></iframe>
						</div>
						<div class="col-md-12">
							<br> <input type="submit" class="btn btn-primary" name="btnsaveapdf" onclick="return insertPdf('uploadPDF')" value="Save Content" />
						</div>
					</form>
					
									
				</div>
			</div>
		</div>
	<div id="result">
		<h3>${requestScope["message"]}</h3>
		<br>
	</div>