package gov.bfar.cms.webapp.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CMSFiles {

    private String fileName;
    
    
    
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public CMSFiles() {
        super();
        // TODO Auto-generated constructor stub
    }

    public CMSFiles(String fileName) {
        super();
        this.fileName = fileName;
    }

    public List<CMSFiles> file(String dirPath) {
        List<CMSFiles> cmsFiles = new ArrayList<>();
        File dir = new File(dirPath);
        String[] files = dir.list();
        if (files.length == 0) {
            System.out.println("The directory is empty");
        } else {
            for (String aFile : files) {
                cmsFiles.add(new CMSFiles(aFile));
            }
        }
        return cmsFiles; 
    }
}
