package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.DownloadDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Download;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertDownloadSubContent
 */
@WebServlet("/InsertDownloadSubContent")
public class InsertDownloadSubContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertDownloadSubContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    // TODO Auto-generated method stub
        insertsub(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    insertsub(request, response);
	}
	protected void insertsub(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        Download download = new Download();
        DownloadDao downloadDao = new DownloadDao();
        download.setId(uuid);
        download.setTitle(request.getParameter("title"));
        download.setContent(request.getParameter("content"));
        download.setContentType("subhtml");
        download.setSubContentLink("/WebsiteNavigation?id="+download.getId()+"&pageAction=downloadContentFrame");
        download.setStatus(true);
        download.setDateCreated(date.get());
        download.setCreatedBy(userid);
        download.setModifiedDate(date.get());
        download.setModifiedBy(userid);
        downloadDao.save(download);
        
        session.setAttribute("tabactivedownload", "sub");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=download");
      
    }


}