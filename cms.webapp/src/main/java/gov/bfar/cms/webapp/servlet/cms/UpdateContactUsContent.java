package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ContactUsDao;
import gov.bfar.cms.domain.ContactUs;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateContactUsContent
 */
@WebServlet("/UpdateContactUsContent")
public class UpdateContactUsContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateContactUsContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        update(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    update(request, response);
	}
	
	protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM-dd-yyyy hh:mm:ss aa");
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
	    ContactUsDao contactUsDao = new ContactUsDao();
        ContactUs contactUs = new ContactUs();
        contactUs.setId(request.getParameter("id"));
        contactUs.setTitle(request.getParameter("title"));
        contactUs.setContent(request.getParameter("content"));
        contactUs.setStatus(Boolean.parseBoolean(request.getParameter("status")));
        contactUs.setModifiedBy(userid);
        contactUs.setPageLink(request.getParameter("pagelink"));
        contactUs.setModifiedDate(date.get());
        contactUsDao.update(contactUs);
        
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=contactus");
      
    }
}

