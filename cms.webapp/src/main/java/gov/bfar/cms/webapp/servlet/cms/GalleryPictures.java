package gov.bfar.cms.webapp.servlet.cms;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import gov.bfar.cms.dao.PicturesDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Pictures;
import gov.bfar.cms.util.DateUtil;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

/**
 * Servlet implementation class GalleryPictures
 */
@WebServlet("/GalleryPictures")
public class GalleryPictures extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private String UPLOAD_DIRECTORY = "";
    private String DOWNLOAD_DIRECTORY = "";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GalleryPictures() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        gallerypic(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        gallerypic(request, response);
    }

    protected void gallerypic(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String pageaction = request.getParameter("action");
        String userid = (String)session.getAttribute("userid");
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        PicturesDao picturesDao = new PicturesDao();
        Pictures pictures = new Pictures();

        String gallerytitleid = null;
        String gallerytitle = null;
        String gallerypagelink = null;
        int gallerysequence = 0;
        String gallerytitledateevent = null;

        String gallerytype = null;
        String gallerycontenttype = null;
        boolean gallerystatus = false;

        if (pageaction.equalsIgnoreCase("addtitle")) {
            UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryimageup");
            DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryimagedl");
            Optional<String> gtitleimg = DateUtil.getFormattedDate("MM-dd-yyyy");
            String gtitleimguuid = BaseDomain.newUUID();
            String filePath = null;
            String contentType = null;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {

                            fname = gtitleimguuid + "(" + gtitleimg.get() + ")" + ".jpeg";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;
                            String ext = FilenameUtils.getExtension(fname);
                            contentType = ext;
                        } else {
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch (fName) {
                                case "gallerytitle":
                                    gallerytitle = value;
                                    break;
                                case "gallerysequence":
                                    gallerysequence = Integer.parseInt(value);
                                    break;
                                case "gallerytype":
                                    gallerytype = value;
                                    break;
                                case "gallerycontenttype":
                                    gallerycontenttype = value;
                                    break;
                                case "gallerytitledateevent":
                                    gallerytitledateevent = value;
                                    break;
                            }
                        }
                    }

                    request.setAttribute("message", "File Uploaded Successfully");
                    request.setAttribute("name", fname);
                    request.setAttribute("size", fsize);
                    request.setAttribute("type", ftype);
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to " + ex);
                }

            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }

            pictures.setId(uuid);
            pictures.setTitle(gallerytitle);
            pictures.setSequence(gallerysequence);
            pictures.setType(gallerytype);
            pictures.setContentType(gallerycontenttype);
            pictures.setImageFilePath(filePath);
            pictures.setDateEvent(gallerytitledateevent);
            pictures.setPageLink("/WebsiteNavigation?id=" + pictures.getId() + "&pageAction=gallerycontentFrame");
            pictures.deactive();
            pictures.setCreatedBy(userid);
            pictures.setDateCreated(date.get());
            pictures.setModifiedDate(date.get());
            pictures.setModifiedBy(userid);
            picturesDao.save(pictures);
            picturesDao.spAutoOrdering(pictures.getSequence(), pictures.getId(), pictures.getType(), pictures.getContentType(), 0);
            pictures.toString();
            request.setAttribute("tid", pictures.getId());
            response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=gallery");
            /*
             * request.setAttribute("navigation",
             * "../cms/pages/gallery/galleryPictureList.jsp");
             */

        } else if (pageaction.equalsIgnoreCase("updatetitle")) {
            UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryimageup");
            DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryimagedl");
            Optional<String> gtitleimg = DateUtil.getFormattedDate("MM-dd-yyyy");
            String gtitleimguuid = BaseDomain.newUUID();
            String imgtitlechange = null;
            String filePath = null;
            String contentType = null;
            String titleoldsequence = null;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {
                            fname = gtitleimguuid + "(" + gtitleimg.get() + ")" + ".jpeg";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;
                            String ext = FilenameUtils.getExtension(fname);
                            contentType = ext;
                        } else {
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch (fName) {
                                case "gallerytitleid":
                                    gallerytitleid = value;
                                    break;
                                case "gallerytitle":
                                    gallerytitle = value;
                                    break;
                                case "gallerysequence":
                                    gallerysequence = Integer.parseInt(value);
                                    break;
                                case "titleoldsequence":
                                    titleoldsequence = value;
                                    break;
                                case "gallerypagelink":
                                    gallerypagelink = value;
                                    break;
                                case "gallerytype":
                                    gallerytype = value;
                                    break;
                                case "gallerytitledateevent":
                                    gallerytitledateevent = value;
                                    break;
                                case "gallerycontenttype":
                                    gallerycontenttype = value;
                                    break;
                                case "gallerystatus":
                                    gallerystatus = Boolean.parseBoolean(value);
                                    break;
                                case "imgtitlechange":
                                    imgtitlechange = value;
                                    break;
                            }
                        }
                    }

                    request.setAttribute("message", "File Uploaded Successfully");
                    request.setAttribute("name", fname);
                    request.setAttribute("size", fsize);
                    request.setAttribute("type", ftype);
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to " + ex);
                }

            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
            Pictures t = picturesDao.findById(gallerytitleid);
            pictures.setId(gallerytitleid);
            pictures.setTitle(gallerytitle);
            pictures.setSequence(gallerysequence);
            pictures.setType(gallerytype);
            pictures.setContentType(gallerycontenttype);
            pictures.setPageLink(gallerypagelink);
            pictures.setStatus(gallerystatus);
            if (imgtitlechange.equalsIgnoreCase("true")) {
                pictures.setImageFilePath(filePath);
            } else {
                pictures.setImageFilePath(t.getImageFilePath());
            }
            pictures.setModifiedBy(userid);
            pictures.setModifiedDate(date.get());
            picturesDao.update(pictures);
            picturesDao.spAutoOrdering(pictures.getSequence(),
                                       pictures.getId(),
                                       pictures.getType(),
                                       pictures.getContentType(),
                                       Integer.parseInt(titleoldsequence));
            request.setAttribute("tid", pictures.getId());
            response.sendRedirect("/BFAR-R7/UpdatePictureStatus?contentid=" + gallerytitleid + "&status=updateall&type=gallery");

        } else if (pageaction.equalsIgnoreCase("insertalbumimage")) {
            UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryimageup");
            DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryimagedl");
            Optional<String> imgdate = DateUtil.getFormattedDate("MM-dd-yyyy");
            String filePath = null;
            List<Pictures> filePictures = new ArrayList<>();
            Pictures titlePic = new Pictures();
            List<Pictures> titlePicture = new ArrayList<>();
            Pictures datePic = new Pictures();
            List<Pictures> datePicture = new ArrayList<>();
            int seq = 0;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                    int fileCount = 0;

                    for (FileItem item : multiparts) {
                        String imguuid = BaseDomain.newUUID();
                        String imgFileTitle = null;
                        String imgFileDate = null;
                        if (!item.isFormField()) {
                            fname = imguuid + "(" + imgdate.get() + ")" + ".jpeg";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;

                            System.out.println("FILE NAME : " + fname);
                            System.out.println("FILE PATH : " + UPLOAD_DIRECTORY + fname);
                            if (!fname.equals("undefined") || !fname.equals(null)) {
                                fileCount++;
                                System.out.println(fileCount + " yes");
                            }
                            pictures.setTitleId(request.getParameter("tid"));
                            pictures.setType("gallery");
                            pictures.setContentType("albumimage");
                            pictures.setImageDescription("");
                            pictures.setSequence(seq);
                            pictures.setImageFilePath(filePath);
                            pictures.setId(imguuid);
                            pictures.activate();
                            pictures.setDateCreated(date.get());
                            pictures.setCreatedBy(userid);
                            pictures.setModifiedDate(date.get());
                            pictures.setModifiedBy(userid);
                            filePictures.add(pictures);
                            pictures = new Pictures();
                        } else {
                            String fName = item.getFieldName();
                            String value = item.getString();

                            if (fName.contains("albumTitleNo")) {
                                System.out.println(fName + " : " + value);
                                titlePic.setTitle(value);
                                titlePicture.add(titlePic);
                                titlePic = new Pictures();
                            }
                            if (fName.contains("albumEventDate")) {
                                System.out.println(fName + " : " + value);
                                datePic.setDateEvent(value);
                                datePicture.add(datePic);
                                datePic = new Pictures();
                            }
                            /*
                             * switch(fName) { case "gallerytitleid":
                             * gallerytitleid = value; break; case "imagetitle":
                             * imagetitle = value; break; case "imagedateevent":
                             * imagedateevent = value; break; case "imagetype":
                             * imagetype = value; break; case
                             * "imagecontenttype": imagecontenttype = value;
                             * break; case "imagesequence": imagesequence =
                             * Integer.parseInt(value); break; case
                             * "galleryoldsequence": galleryoldsequence = value;
                             * break; case "action": action = value; break; case
                             * "imgchange": imgchange = value; break; case
                             * "imageid": imageid = value; break; }
                             */
                        }
                    }

                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to " + ex);
                }
                int picIndex = 0;
                for (Pictures pic : filePictures) {
                    pic.setTitle(titlePicture.get(picIndex).getTitle());
                    pic.setDateEvent(datePicture.get(picIndex).getDateEvent());
                    pic.setSequence(picturesDao.getSuggestedSequenceGallery("gallery", "albumimage", request.getParameter("tid")));
                    picturesDao.save(pic);
                    picIndex++;
                }
            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }

            request.setAttribute("tid", request.getParameter("tid"));
            response.sendRedirect("/BFAR-R7/UpdatePictureStatus?contentid=" + request.getParameter("tid") + "&status=updateall&type=gallery");
        } else if (pageaction.equalsIgnoreCase("updatealbumimage")) {
            UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryimageup");
            DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryimagedl");
            Optional<String> imgdate = DateUtil.getFormattedDate("MM-dd-yyyy");
            String imagetitle = null;
            int imagesequence = 0;
            String imageid = null;
            String imagedateevent = null;
            String imgdescription = null;
            String imagecontenttype = null;
            String imagetype = null;
            String galleryoldsequence = null;
            String action = null;
            String imgchange = null;
            String filePath = null;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {
                            fname = uuid + "(" + imgdate.get() + ")" + ".jpeg";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;
                        } else {
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch (fName) {
                                case "gallerytitleid":
                                    gallerytitleid = value;
                                    break;
                                case "imagetitle":
                                    imagetitle = value;
                                    break;
                                case "imagedateevent":
                                    imagedateevent = value;
                                    break;
                                case "imgdescription":
                                    imgdescription = value;
                                    break;
                                case "imagetype":
                                    imagetype = value;
                                    break;
                                case "imagecontenttype":
                                    imagecontenttype = value;
                                    break;
                                case "imagesequence":
                                    imagesequence = Integer.parseInt(value);
                                    break;
                                case "galleryoldsequence":
                                    galleryoldsequence = value;
                                    break;
                                case "action":
                                    action = value;
                                    break;
                                case "imgchange":
                                    imgchange = value;
                                    break;
                                case "imageid":
                                    imageid = value;
                                    break;
                            }
                        }
                    }
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to " + ex);
                }

            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
            Pictures picup = new Pictures();
            pictures.setTitleId(gallerytitleid);
            pictures.setTitle(imagetitle);
            pictures.setImageDescription(imgdescription);
            pictures.setDateEvent(imagedateevent);
            pictures.setType(imagetype);
            pictures.setContentType(imagecontenttype);
            pictures.setSequence(imagesequence);
            picup = picturesDao.findById(imageid);
            if (imgchange.equalsIgnoreCase("true")) {
                pictures.setImageFilePath(filePath);
            } else {
                pictures.setImageFilePath(picup.getImageFilePath());
            }
            pictures.setSequence(imagesequence);
            pictures.setId(picup.getId());
            pictures.setStatus(picup.isStatus());
            pictures.setModifiedBy(userid);
            pictures.setModifiedDate(date.get());
            picturesDao.update(pictures);
            picturesDao.spAutoOrderingGallery(pictures.getSequence(),
                                              pictures.getId(),
                                              pictures.getType(),
                                              pictures.getContentType(),
                                              pictures.getTitleId(),
                                              Integer.parseInt(galleryoldsequence));

            request.setAttribute("tid", gallerytitleid);
            response.sendRedirect("/BFAR-R7/UpdatePictureStatus?contentid=" + gallerytitleid + "&status=updateall&type=gallery");
        }

    }

}
