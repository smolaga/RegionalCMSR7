package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.UserDao;
import gov.bfar.cms.domain.User;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class AboutUsUpdateStatus
 */
@WebServlet("/AccountUpdateStatus")
public class AccountUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		updatestatus(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		updatestatus(request, response);
	}
	
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionredirect = request.getParameter("status");
		String id = request.getParameter("contentid");
		HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		request.setAttribute("getidcontent", id);
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		UserDao userDao = new UserDao();
		User user = userDao.findById(id);		
		User user2 = new User();
		user2.setId(user.getId());
		user2.setFirstName(user.getFirstName());
		user2.setMiddleName(user.getMiddleName());
		user2.setLastName(user.getLastName());
		user2.setDesignation(user.getDesignation());
		user2.setUserName(user.getUserName());
		user2.setUserPassword(user.getUserPassword());
		user2.setUserType(user.getUserType());
		user2.setSecretQuestion(user.getSecretQuestion());
		user2.setAnswer(user.getAnswer());
		user2.setModifiedDate(date.get());
		user2.setModifiedBy(userid);
		user2.setUserAccessContent(user.getUserAccessContent());
		user2.setCanUpdate(user.isCanUpdate());
		
		if (actionredirect.equalsIgnoreCase("updateall")) {
			request.setAttribute("navigation", "../cms/pages/login/updateuserInfo.jsp");
		}
		else if (actionredirect.equalsIgnoreCase("delete")) {
			request.setAttribute("navigation", "../cms/pages/login/updateuserInfo.jsp");
		}
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
		dispatcher.forward(request, response);
	}
		
}

