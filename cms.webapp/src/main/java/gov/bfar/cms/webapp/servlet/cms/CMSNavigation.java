package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.EmailInquiryDao;
import gov.bfar.cms.domain.EmailInquiry;
import gov.bfar.cms.dao.FooterDao;
import gov.bfar.cms.domain.Footer;
import gov.bfar.cms.dao.PicturesDao;
import gov.bfar.cms.domain.Pictures;

/**
 * Servlet implementation class CMSNavigation
 */
@WebServlet("/CMSNavigation")
public class CMSNavigation extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final String CMS_INDEX = "/cms/cmsIndex.jsp";  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CMSNavigation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		navigation(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		navigation(request, response);
	}

	protected void navigation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    HttpSession session = request.getSession();
	    String action = request.getParameter("pageAction");
		
		if (action.equalsIgnoreCase("homepageleftcontent")) {
			request.setAttribute("navigation", "../cms/pages/homepageleft/leftContent.jsp");
			request.setAttribute("type", "left");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("homepagerightcontent")) {
			request.setAttribute("navigation", "../cms/pages/homepageright/rightContent.jsp");
			request.setAttribute("type", "right");		
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("homepagecentercontent")) {
			request.setAttribute("navigation", "../cms/pages/homepagecenter/centerContent.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("insertcontent")) {
			String type = request.getParameter("type"); 
			
			if (type.equalsIgnoreCase("center")) {
				request.setAttribute("columntype", type);				
				request.setAttribute("navigation", "../cms/pages/homepagecenter/insertMainTitle.jsp");
			}
			else if (type.equalsIgnoreCase("left")) {
			    request.setAttribute("columntype", type);               
                request.setAttribute("navigation", "../cms/pages/homepageleft/insertMainTitle.jsp");
			}
			else if (type.equalsIgnoreCase("right")) {
			    request.setAttribute("columntype", type);               
                request.setAttribute("navigation", "../cms/pages/homepageright/insertMainTitle.jsp");
			}
			else if (type.equalsIgnoreCase("aboutus")) {
				request.setAttribute("navigation", "../cms/pages/aboutus/insertContent.jsp");
			}
			else if (type.equalsIgnoreCase("service")) {
				request.setAttribute("navigation", "../cms/pages/services/insertContent.jsp");
			}
			else if (type.equalsIgnoreCase("contactus")) {
				request.setAttribute("navigation", "../cms/pages/contactus/insertContent.jsp");
			}
			else if (type.equalsIgnoreCase("profile")) {
				request.setAttribute("navigation", "../cms/pages/profile/insertContent.jsp");
			}
			else if (type.equalsIgnoreCase("transparency")) {
                request.setAttribute("navigation", "../cms/pages/transparency/insertContent.jsp");
            }
			else if (type.equalsIgnoreCase("footer")) {
				request.setAttribute("navigation", "../cms/pages/footer/insertContent.jsp");
				request.setAttribute("type", "footer");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
				dispatcher.forward(request, response);
          
            }
			
			else if (type.equalsIgnoreCase("video")) {
                request.setAttribute("navigation", "../cms/pages/video/insertContent.jsp");
            }
			else if (type.equalsIgnoreCase("facilities")) {
                request.setAttribute("navigation", "../cms/pages/facilities/insertContent.jsp");
            }
			else if (type.equalsIgnoreCase("program")) {
                request.setAttribute("navigation", "../cms/pages/program/insertContent.jsp");
            }
			else if (type.equalsIgnoreCase("download")) {
                request.setAttribute("navigation", "../cms/pages/download/insertContent.jsp");
            }
			else if (type.equalsIgnoreCase("publication")) {
                request.setAttribute("navigation", "../cms/pages/publication/insertContent.jsp");
            }
			else if (type.equalsIgnoreCase("laws")) {
                request.setAttribute("navigation", "../cms/pages/laws/insertContent.jsp");
            }
			else if (type.equalsIgnoreCase("otherlinks")) {
                request.setAttribute("navigation", "../cms/pages/otherLinks/insertContent.jsp");
            }
			else if (type.equalsIgnoreCase("gallery")) {
			    request.setAttribute("picturetype", type);
                request.setAttribute("navigation", "../cms/pages/gallery/insertGalleryTitle.jsp");
            }
			

			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		
		else if (action.equalsIgnoreCase("insertpdfcontent")) {
            String type = request.getParameter("type");         
            
             if (type.equalsIgnoreCase("aboutus")) {
                request.setAttribute("navigation", "../cms/pages/aboutus/insertpdfContent.jsp");
            }
             else if (type.equalsIgnoreCase("center")) {
                    request.setAttribute("coltype", "center");
                    request.setAttribute("navigation", "../cms/pages/homepagecenter/insertPDFContent.jsp");
                }            
            
             else if (type.equalsIgnoreCase("profile")) {
                    request.setAttribute("navigation", "../cms/pages/profile/insertpdfContent.jsp");
                }
             else if (type.equalsIgnoreCase("facilities")) {
                 request.setAttribute("navigation", "../cms/pages/facilities/insertpdfContent.jsp");
             }
             else if (type.equalsIgnoreCase("download")) {
                 request.setAttribute("navigation", "../cms/pages/download/insertpdfContent.jsp");
             }
            
             
             RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
             dispatcher.forward(request, response);
        }
		
		else if (action.equalsIgnoreCase("insertsubContent")) {
			String type = request.getParameter("type");
			
			 if (type.equalsIgnoreCase("left")) {
			     request.setAttribute("columntype", type);			     
			     request.setAttribute("navigation", "../cms/pages/homepageleft/insertSubHTMLContent.jsp");
			 }
    		 else if (type.equalsIgnoreCase("right")){
    		     request.setAttribute("columntype", type);
    		      
    		     request.setAttribute("navigation", "../cms/pages/homepageright/insertSubHTMLContent.jsp");
    		 }
    		 else if (type.equalsIgnoreCase("center")){
    		     request.setAttribute("columntype", type);
    		      
    		     request.setAttribute("navigation", "../cms/pages/homepagecenter/insertSubHTMLContent.jsp");
    		 }
    		 else if (type.equalsIgnoreCase("profile")) {
                request.setAttribute("navigation", "../cms/pages/profile/insertsubContent.jsp");
    		 }
    		 else if (type.equalsIgnoreCase("service")) {
                request.setAttribute("navigation", "../cms/pages/services/insertsubContent.jsp");
    		 }
    		 else if (type.equalsIgnoreCase("aboutUs")) {
                request.setAttribute("navigation", "../cms/pages/aboutus/insertsubContent.jsp");
    		 }
    		 else if (type.equalsIgnoreCase("facilities")) {
                 request.setAttribute("navigation", "../cms/pages/facilities/insertsubContent.jsp");
              }			
			else if (type.equalsIgnoreCase("transparency")) {
                request.setAttribute("navigation", "../cms/pages/transparency/insertsubContent.jsp");
            }
			else if (type.equalsIgnoreCase("program")) {
                request.setAttribute("navigation", "../cms/pages/program/insertsubContent.jsp");
            }
			else if (type.equalsIgnoreCase("download")) {
                request.setAttribute("navigation", "../cms/pages/download/insertsubContent.jsp");
            }
			else if (type.equalsIgnoreCase("publication")) {
                request.setAttribute("navigation", "../cms/pages/publication/insertsubContent.jsp");
            }
			else if (type.equalsIgnoreCase("laws")) {
                request.setAttribute("navigation", "../cms/pages/laws/insertsubContent.jsp");
            }
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);

		}
		
		else if (action.equalsIgnoreCase("insertsubpdfContent")) {
            String type = request.getParameter("type");
            
             if (type.equalsIgnoreCase("left")) {
                request.setAttribute("columntype", type);                 
                request.setAttribute("navigation", "../cms/pages/homepageleft/insertSubPDFContent.jsp");
            }
             else if(type.equalsIgnoreCase("right")){
                 request.setAttribute("columntype", type);
                  
                 request.setAttribute("navigation", "../cms/pages/homepageright/insertSubPDFContent.jsp");
             }
             else if(type.equalsIgnoreCase("center")){
                 request.setAttribute("columntype", type);                  
                 request.setAttribute("navigation", "../cms/pages/homepagecenter/insertSubPDFContent.jsp");
             }
            else if (type.equalsIgnoreCase("profile")) {
                request.setAttribute("navigation", "../cms/pages/profile/insertsubpdfContent.jsp");
            }
            else if (type.equalsIgnoreCase("service")) {
                request.setAttribute("navigation", "../cms/pages/services/insertsubpdfContent.jsp");
            }
            else if (type.equalsIgnoreCase("aboutUs")) {
                request.setAttribute("navigation", "../cms/pages/aboutus/insertsubpdfContent.jsp");
            }
            else if (type.equalsIgnoreCase("transparency")) {
                request.setAttribute("navigation", "../cms/pages/transparency/insertsubpdfContent.jsp");
            }
            else if (type.equalsIgnoreCase("facilities")) {
                request.setAttribute("navigation", "../cms/pages/facilities/insertsubpdfContent.jsp");
            }
            else if (type.equalsIgnoreCase("program")) {
                request.setAttribute("navigation", "../cms/pages/program/insertsubpdfContent.jsp");
            }
            else if (type.equalsIgnoreCase("download")) {
                request.setAttribute("navigation", "../cms/pages/download/insertsubpdfContent.jsp");
            }
            else if (type.equalsIgnoreCase("publication")) {
                request.setAttribute("navigation", "../cms/pages/publication/insertsubpdfContent.jsp");
            }
            else if (type.equalsIgnoreCase("laws")) {
                request.setAttribute("navigation", "../cms/pages/laws/insertsubpdfContent.jsp");
            }
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		
		
		else if (action.equalsIgnoreCase("slider")) {
			request.setAttribute("navigation", "../cms/pages/slider/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("banner")) {
			request.setAttribute("navigation", "../cms/pages/banner/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("gallery")) {
			request.setAttribute("navigation", "../cms/pages/gallery/galleryTitleList.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("video")) {
			request.setAttribute("navigation", "../cms/pages/video/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("about")) {
			request.setAttribute("navigation", "../cms/pages/aboutus/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("service")) {
			request.setAttribute("navigation", "../cms/pages/services/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("profile")) {
			request.setAttribute("navigation", "../cms/pages/profile/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("transparency")) {
            request.setAttribute("navigation", "../cms/pages/transparency/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("download")) {
            request.setAttribute("navigation", "../cms/pages/download/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("otherlinks")) {
            request.setAttribute("navigation", "../cms/pages/otherLinks/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("contactus")) {
			request.setAttribute("navigation", "../cms/pages/contactus/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("footer")) {
            request.setAttribute("navigation", "../cms/pages/footer/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("facilities")) {
            request.setAttribute("navigation", "../cms/pages/facilities/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("program")) {
            request.setAttribute("navigation", "../cms/pages/program/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("publication")) {
            request.setAttribute("navigation", "../cms/pages/publication/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("laws")) {
            request.setAttribute("navigation", "../cms/pages/laws/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("home")) {
			request.setAttribute("navigation", "../cms/homepage.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("settings")) {
		    EmailInquiryDao emailInquiryDao = new EmailInquiryDao();
	        EmailInquiry emailInquiry = emailInquiryDao.OneEmailSend();
	        try {
	            request.setAttribute("email", emailInquiry.getEmailaddress());
	            request.setAttribute("password", emailInquiry.getPassword());
	            request.setAttribute("id", emailInquiry.getId());
	            request.setAttribute("upemin", "true");
	        } catch (Exception e) {
	        	 request.setAttribute("email", null);
	        	 request.setAttribute("password", null);
		         request.setAttribute("id", null);
	            request.setAttribute("upemin", "false");
	        }
	        FooterDao footerDao = new FooterDao();
	        Footer footer = footerDao.OneFooterDisplay();
	        try {
	            session.setAttribute("ftitle", footer.getTitle());
	            session.setAttribute("fcontent", footer.getContent());
	            session.setAttribute("fid", footer.getId());
	            session.setAttribute("upfmin", "true");
	        } catch (Exception e) {
	        	session.setAttribute("ftitle", null);
	            session.setAttribute("fcontent", null);
	            session.setAttribute("fid", null);
	        	session.setAttribute("upfmin", "false");
	        }
	        
	        PicturesDao picturesDao = new PicturesDao();
	        Pictures pictures = picturesDao.OneBannerDisplay();
	        try { 
	            session.setAttribute("bid", pictures.getId());
	            session.setAttribute("upbmin", "true");
	        } catch (Exception e) {
	        	session.setAttribute("bid", null);
	        	session.setAttribute("upbmin", "false");
	        }
			request.setAttribute("navigation", "../cms/pages/settings/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("viewemail")) {
		    request.setAttribute("id", request.getParameter("id"));
		    request.setAttribute("navigation", "../cms/pages/feedback/viewInquiry.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("adduser")) {
			request.setAttribute("navigation", "../cms/pages/login/createAccount.jsp");	
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("viewuser")) {
			request.setAttribute("navigation", "../cms/pages/login/userList.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("files")) {
            request.setAttribute("navigation", "../cms/pages/files/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("feedback")) {
            request.setAttribute("navigation", "../cms/pages/feedback/index.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("compose")) {
            request.setAttribute("navigation", "../cms/pages/feedback/compose.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("logout")) {
		    try {
                session = request.getSession(false);
                if (session != null){
                	session.setMaxInactiveInterval(1);
                    session.invalidate();	
                }
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/pages/login/login.jsp");
                dispatcher.forward(request, response);
            } catch (IllegalStateException e) {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/pages/login/login.jsp");
                dispatcher.forward(request, response);
            }
		}
	}
}
