package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ServiceDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Service;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertService
 */
@WebServlet("/InsertService")
public class InsertService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		insertService(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		insertService(request, response);
	}
	protected void insertService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        Service service = new Service();
        ServiceDao serviceDao = new ServiceDao();
        service.setId(uuid);
        service.setTitle(request.getParameter("title"));
        service.setContent(request.getParameter("content"));
        service.setContentType("html");
        service.setPageLink("/WebsiteNavigation?id="+service.getId()+"&pageAction=serviceContentFrame");
        service.setStatus(false);
        service.setDateCreated(date.get());
        service.setCreatedBy(userid);
        service.setModifiedDate(date.get());
        service.setModifiedBy(userid);
        serviceDao.save(service);
        
        session.setAttribute("tabactiveservices", "main");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=service");
     
    }
}