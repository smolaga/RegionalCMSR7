package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.DownloadDao;
import gov.bfar.cms.domain.Download;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateDownloadContent
 */
@WebServlet("/UpdateDownloadContent")
public class UpdateDownloadContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateDownloadContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        updatecontent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatecontent(request, response);
	}
	protected void updatecontent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
	    DownloadDao downloadDao = new DownloadDao();
	    Download download = new Download();
	    download.setId(request.getParameter("id"));
	    download.setTitle(request.getParameter("title"));
	    download.setContent(request.getParameter("content"));
	    download.setContentType("html");
	    download.setSequence(Integer.parseInt(request.getParameter("sequence")));
	    download.setStatus(Boolean.parseBoolean(request.getParameter("status")));
	    download.setModifiedBy(userid);
	    download.setPageLink(request.getParameter("pagelink"));
	    download.setModifiedDate(date.get());
	    downloadDao.update(download);
	    downloadDao.spAutoOrdering(download.getSequence(), download.getId(),Integer.parseInt(request.getParameter("oldsequence")));
	    session.setAttribute("tabactivedownload", "main");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=download");      
    }
}
