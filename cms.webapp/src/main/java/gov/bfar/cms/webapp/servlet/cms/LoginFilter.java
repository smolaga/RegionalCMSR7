package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebListener;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Application Lifecycle Listener implementation class LoginFilter
 *
 */
@WebListener
@WebFilter("/*")
public class LoginFilter  implements Filter {

	private static final String NAVIGATION_PATH = "/BFAR-R7/CMSNavigation?pageAction=%s";
	private static final String DASHBOARD_PATH = String.format(NAVIGATION_PATH, "home");
			
	/**
	 * Default constructor.
	 */
	public LoginFilter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		
		String path = req.getRequestURI();
		
		if(path.indexOf("media") > 1) {
			chain.doFilter(request, response);

		} else {
			// active session
			if(session != null) {
				if(path.contains("authenticate") || path.contains("login.jsp")) {
					res.sendRedirect(DASHBOARD_PATH);
				} else {
					chain.doFilter(request, response);
				}
			} else {
				// no active session
				if(path.contains("login.jsp") || path.contains("authenticate")) {
					chain.doFilter(request, response);
				} else {
	                res.sendRedirect("/BFAR-R7/cms/pages/login/login.jsp");
				}
				
			}
	       
		}

		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
