package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.LawsDao;
import gov.bfar.cms.domain.Laws;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateLawsContent
 */
@WebServlet("/UpdateLawsContent")
public class UpdateLawsContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateLawsContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        updatecontent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatecontent(request, response);
	}
	protected void updatecontent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Laws laws = new Laws();
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
	    LawsDao lawsDao = new LawsDao();
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        laws.setId(request.getParameter("id"));
        laws.setTitle(request.getParameter("title"));
        laws.setContent(request.getParameter("content"));
        laws.setContentType("html");
        laws.setStatus(Boolean.parseBoolean(request.getParameter("status")));
        laws.setModifiedBy(userid);
        laws.setPageLink(request.getParameter("pagelink"));
        laws.setModifiedDate(date.get());
        lawsDao.update(laws);        
        session.setAttribute("tabactivelaws", "main");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=laws");
   
    }

}