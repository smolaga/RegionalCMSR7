package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ProfileDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Profile;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertProfile
 */
@WebServlet("/InsertProfile")
public class InsertProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		insertProfile(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		insertProfile(request, response);
	}
	protected void insertProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        Profile profile = new Profile();
        ProfileDao profileDao = new ProfileDao();
        profile.setId(uuid);
        profile.setTitle(request.getParameter("title"));
        profile.setContent(request.getParameter("content"));
        profile.setContentType("html");
        profile.setPageLink("/WebsiteNavigation?id="+profile.getId()+"&pageAction=profileContentFrame");
        profile.setSequence(Integer.parseInt(request.getParameter("sequence")));
        profile.setStatus(false);
        profile.setDateCreated(date.get());
        profile.setCreatedBy(userid);
        profile.setModifiedDate(date.get());
        profile.setModifiedBy(userid);
        profileDao.save(profile);
        profileDao.spAutoOrdering(profile.getSequence(), profile.getId(),0);
        session.setAttribute("tabactiveprofile", "main");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=profile");       
        
    }

}


