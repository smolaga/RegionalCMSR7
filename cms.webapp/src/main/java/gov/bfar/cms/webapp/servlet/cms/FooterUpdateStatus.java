package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.FooterDao;
import gov.bfar.cms.domain.Footer;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class FooterUpdateStatus
 */
@WebServlet("/FooterUpdateStatus")
public class FooterUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FooterUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatestatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatestatus(request, response);
	}
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String actionredirect = request.getParameter("status");
        String id = request.getParameter("contentid");
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        request.setAttribute("getidcontent", id);
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        FooterDao footerDao = new FooterDao();
        Footer footer = footerDao.findById(id);        
        Footer footers = new Footer();
        footers.setId(footer.getId());
        footers.setTitle(footer.getTitle());
        footers.setContent(footer.getContent());
        footers.setContentType(footer.getContentType());
        footers.setPageLink(footer.getPageLink());
        footers.setModifiedBy(userid);
        footers.setModifiedDate(date.get());
        
        if (actionredirect.equalsIgnoreCase("show")) {
            footers.setStatus(true);
            footerDao.update(footers);
            footerDao.setOneActiveStatus("footer", footer.getId());
            request.setAttribute("navigation", "../cms/pages/footer/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("hide")) {
            footers.setStatus(false);
            footerDao.update(footers);
            request.setAttribute("navigation", "../cms/pages/footer/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("updateall")) {
            request.setAttribute("navigation", "../cms/pages/footer/updateContent.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("delete")) {			
        	footerDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/footer/index.jsp");
		}
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
        dispatcher.forward(request, response);
    }

}
