package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import gov.bfar.cms.dao.ContactUsDao;
import gov.bfar.cms.domain.ContactUs;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertContactUs
 */
@WebServlet("/InsertContactUs")
public class InsertContactUs extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertContactUs() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        insertContent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    insertContent(request, response);
	}
	protected void insertContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        ContactUs contactUs = new ContactUs();
        ContactUsDao contactUsDao = new ContactUsDao();
        contactUs.setId(uuid);
        contactUs.setTitle(request.getParameter("title"));
        contactUs.setContent(request.getParameter("content"));
        contactUs.setPageLink("/WebsiteNavigation");
        contactUs.setStatus(false);
        contactUs.setDateCreated(date.get());
        contactUs.setCreatedBy(userid);
        contactUs.setModifiedDate(date.get());
        contactUs.setModifiedBy(userid);
        contactUsDao.save(contactUs);            
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=contactus");
     
        
        
        
    }

}
