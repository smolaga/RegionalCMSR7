package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ServiceDao;

import gov.bfar.cms.domain.Service;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class ServiceUpdateStatus
 */
@WebServlet("/ServiceUpdateStatus")
public class ServiceUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServiceUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updateStatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updateStatus(request, response);
	}
	protected void updateStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionredirect = request.getParameter("status");
		String id = request.getParameter("contentid");
		HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		request.setAttribute("getidcontent", id);
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		ServiceDao serviceDao = new ServiceDao();
		Service service = serviceDao.findById(id);	
		Service services = new Service();
		services.setId(service.getId());
		services.setTitle(service.getTitle());
		services.setContent(service.getContent());
		services.setContentType(service.getContentType());
		services.setPdfFilePath(service.getPdfFilePath());
		services.setSequence(service.getSequence());
		services.setPageLink(service.getPageLink());
		services.setModifiedBy(userid);
		services.setModifiedDate(date.get());
		
		if (actionredirect.equalsIgnoreCase("show")) {
			services.setStatus(true);
			serviceDao.update(services);
			serviceDao.setOneActiveStatus(service.getId());
			request.setAttribute("navigation", "../cms/pages/services/index.jsp");
		}
		else if (actionredirect.equalsIgnoreCase("hide")) {
			services.setStatus(false);
			serviceDao.update(services);
			request.setAttribute("navigation", "../cms/pages/services/index.jsp");
		}
		else if (actionredirect.equalsIgnoreCase("updateall")) {
			String ctype = request.getParameter("contenttype");
			if (ctype.equalsIgnoreCase("html")) {
				request.setAttribute("navigation", "../cms/pages/services/updateContent.jsp");
			}
			else if (ctype.equalsIgnoreCase("subhtml")) {
                request.setAttribute("navigation", "../cms/pages/services/updateSubHTMLContent.jsp");
            }
            else if (ctype.equalsIgnoreCase("subpdf")) {
                request.setAttribute("navigation", "../cms/pages/services/updateSubPDFContent.jsp");
            }
		}
		else if (actionredirect.equalsIgnoreCase("delete")) {			
			serviceDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/services/index.jsp");
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
		dispatcher.forward(request, response);
	}

}
