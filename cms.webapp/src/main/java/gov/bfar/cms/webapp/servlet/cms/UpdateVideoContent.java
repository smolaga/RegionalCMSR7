package gov.bfar.cms.webapp.servlet.cms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import gov.bfar.cms.dao.VideoDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Video;
import gov.bfar.cms.util.DateUtil;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

/**
 * Servlet implementation class UpdateVideoContent
 */
@WebServlet("/UpdateVideoContent")
public class UpdateVideoContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String UPLOAD_DIRECTORY ="";
	private String DOWNLOAD_DIRECTORY ="";

    /**
     * @throws FileNotFoundException 
     * @see HttpServlet#HttpServlet()
     */
    public UpdateVideoContent() {
    	super();
    	UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryvideoup");
    	DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("galleryvideodl");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatevideo(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatevideo(request, response);
	}
	protected void updatevideo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> videodate = DateUtil.getFormattedDate("MM-dd-yyyy");
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        String videouuid = BaseDomain.newUUID();
        String uuid = null;
        String reqTitle = null;
        String filePath = null;       
        String sequence = null;
        String oldsequence = null;
        boolean status = false;
        String videofilestatus = null;
        String fname = null;
        String fsize = null;
        String ftype = null;
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {                     
                        String name = new File(item.getName()).getName();
                        String ext = FilenameUtils.getExtension(name);
                        fname = videouuid + "(" + videodate.get() + ")" + "." + ext;
                        fsize = new Long(item.getSize()).toString();                    
                        item.write(new File(UPLOAD_DIRECTORY + fname));
                        String rootfilepath = DOWNLOAD_DIRECTORY;
                        filePath = rootfilepath.toString() + fname;
                      
                    }else{
                        String fName = item.getFieldName();
                        String value = item.getString();
                        switch(fName)
                        {
                        case "title":
                            reqTitle = value;
                            break;
                        case "id":
                            uuid = value;
                            break;
                        case "sequence":
                            sequence = value;
                            break;
                        case "oldsequence":
                            oldsequence = value;
                            break;
                        case "videofilestatus":
                            videofilestatus = value;
                            break;
                        case "status":
                            status = Boolean.parseBoolean(value);
                            break;
                        }
                     
                    }
                }
       
                request.setAttribute("name", fname);
                request.setAttribute("size", fsize);
                request.setAttribute("type", ftype);
            } catch (Exception ex) {
                request.setAttribute("message", "File Upload Failed due to "
                        + ex);
            }
 
        } else {
            request.setAttribute("message", "Sorry this Servlet only handles file upload request");
        }             
        int filesize = ((Integer.parseInt(fsize)/1024)/1024);      
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        Video video = new Video();
        VideoDao videoDao = new VideoDao();
        video.setId(uuid);
        if (videofilestatus.equalsIgnoreCase("true")) {
            video.setVideoFilePath(filePath);
        }else{
            Video videofilepath = videoDao.findById(uuid);
            video.setVideoFilePath(videofilepath.getVideoFilePath());
        }
        video.setTitle(reqTitle);
        video.setSequence(Integer.parseInt(sequence));
        video.setPageLink("/WebsiteNavigation?id="+video.getId()+"&pageAction=videogallery");
        video.setStatus(status);
        video.setModifiedDate(date.get());
        video.setModifiedBy(userid);
        videoDao.update(video);
        videoDao.spAutoOrdering(video.getSequence(), video.getId(),Integer.parseInt(oldsequence));
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=video");  
    }

}
