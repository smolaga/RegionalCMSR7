package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.AboutUsDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.AboutUs;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertAboutUsSubContent
 */
@WebServlet("/InsertAboutUsSubContent")
public class InsertAboutUsSubContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertAboutUsSubContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
	    subcontent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    subcontent(request, response);
	}
	protected void subcontent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        String uuid = BaseDomain.newUUID();
        AboutUs aboutUs = new AboutUs();
        AboutUsDao aboutUsDao = new AboutUsDao();
        aboutUs.setId(uuid);
        aboutUs.setTitle(request.getParameter("title"));
        aboutUs.setContent(request.getParameter("content"));
        aboutUs.setContentType("subhtml");
        aboutUs.setSubContentLink("/WebsiteNavigation?id="+aboutUs.getId()+"&pageAction=aboutContentFrame");
        aboutUs.setStatus(true);
        aboutUs.setDateCreated(date.get());
        aboutUs.setCreatedBy(userid);
        aboutUs.setModifiedDate(date.get());
        aboutUs.setModifiedBy(userid);
        aboutUsDao.save(aboutUs);
        session.setAttribute("tabactiveaboutus", "sub");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=about");
     
    }


}
