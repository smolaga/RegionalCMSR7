package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.VideoDao;
import gov.bfar.cms.domain.Video;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateVideo
 */
@WebServlet("/VideoUpdateStatus")
public class VideoUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
	      
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VideoUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	      update(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    update(request, response);
	}
	protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
	    String actionredirect = request.getParameter("status");
        String id = request.getParameter("contentid");       
        request.setAttribute("getidcontent", id);
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        VideoDao videoDao = new VideoDao();
        Video video = videoDao.findById(id);     
        Video videos = new Video();
        videos.setId(video.getId());
        videos.setTitle(video.getTitle());
        videos.setVideoFilePath(video.getVideoFilePath());
        videos.setSequence(video.getSequence());
        videos.setPageLink(video.getPageLink());
        videos.setModifiedBy(userid);
        videos.setModifiedDate(date.get());
        
        if (actionredirect.equalsIgnoreCase("show")) {
            videos.setStatus(true);
            videoDao.update(videos);
            request.setAttribute("navigation", "../cms/pages/video/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("hide")) {
            videos.setStatus(false);
            videoDao.update(videos);
            request.setAttribute("navigation", "../cms/pages/video/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("updateall")) {
            request.setAttribute("navigation", "../cms/pages/video/updateVideo.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("delete")) {			
        	videoDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/video/index.jsp");
		}
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
        dispatcher.forward(request, response);
    }

}
