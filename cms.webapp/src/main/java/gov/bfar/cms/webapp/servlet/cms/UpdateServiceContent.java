package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ServiceDao;
import gov.bfar.cms.domain.Service;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateServiceContent
 */
@WebServlet("/UpdateServiceContent")
public class UpdateServiceContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServiceContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updateservicecontent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updateservicecontent(request, response);
	}
	
	protected void updateservicecontent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Service service = new Service();
		HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		ServiceDao serviceDao = new ServiceDao();
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		service.setId(request.getParameter("id"));
		service.setTitle(request.getParameter("title"));
		service.setContent(request.getParameter("content"));
		service.setContentType("html");
		service.setStatus(Boolean.parseBoolean(request.getParameter("status")));
		service.setModifiedBy(userid);
		service.setPageLink(request.getParameter("pagelink"));
		service.setModifiedDate(date.get());
		serviceDao.update(service);		
		session.setAttribute("tabactiveservices", "main");
		response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=service");
	
	}

}
