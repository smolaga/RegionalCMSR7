package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.regionalwebsite.bean.EmailBean;
import gov.bfar.cms.regionalwebsite.service.EmailService;
import gov.bfar.cms.regionalwebsite.service.imp.EmailServiceImp;

/**
 * Servlet implementation class SendEmailToMany
 */
@WebServlet("/SendEmailToMany")
public class SendEmailToMany extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendEmailToMany() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    sendtomany(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    sendtomany(request, response);
	}
	
	protected void sendtomany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    HttpSession session = request.getSession();
        String subject = request.getParameter("subject");
        String rcpt = request.getParameter("recipient");
        String[] recipientemails = rcpt.split("\\s*,\\s*");
        String message = request.getParameter("message");
        List<String> emails = new ArrayList<>(); 
        for (String string : recipientemails) {      
            emails.add(string);
        }
        EmailBean bean = new EmailBean(emails, subject, message);
        EmailService emailService = new EmailServiceImp();
        try {
            emailService.sendEmail(bean);
            session.setAttribute("sendingmessage", "Message Send!");
        } catch (Exception e) {
            session.setAttribute("sendingmessage", "Message Failed!");
        }
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=feedback");
    }

}
