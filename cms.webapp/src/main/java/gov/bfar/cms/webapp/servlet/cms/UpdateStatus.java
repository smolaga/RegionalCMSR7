package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.HomepageDao;
import gov.bfar.cms.domain.Homepage;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateStatus
 */
@WebServlet("/UpdateStatus")
public class UpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updateStatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updateStatus(request, response);
	}

	protected void updateStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionredirect = request.getParameter("status");
		HttpSession session = request.getSession();
	    String userid = (String)session.getAttribute("userid");
		String coltype = request.getParameter("type");
		String id = request.getParameter("contentid");
		request.setAttribute("getidcontent", id);
		HomepageDao homepageDao = new HomepageDao();
		Homepage homepage = homepageDao.findById(id);
		Homepage homepage2 = new Homepage();
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		homepage2.setId(homepage.getId());
		homepage2.setHead(homepage.getHead());
		homepage2.setShortContent(homepage.getShortContent());
		homepage2.setContent(homepage.getContent());
		homepage2.setContentType(homepage.getContentType());
		homepage2.setSeeAllLink(homepage.getSeeAllLink());
		homepage2.setPdfFilePath(homepage.getPdfFilePath());
		homepage2.setPageLink(homepage.getPageLink());
		homepage2.setLinkStatus(homepage.getLinkStatus());
		homepage2.setColumnType(homepage.getColumnType());
		homepage2.setSequence(homepage.getSequence());
		homepage2.setModifiedDate(date.get());
		homepage2.setModifiedBy(userid);
		
		if (coltype.equalsIgnoreCase("left")) {
			if (actionredirect.equalsIgnoreCase("show")) {
				homepage2.setStatus(true);
				homepageDao.update(homepage2);
				request.setAttribute("navigation", "../cms/pages/home/leftContent.jsp");				
			}
			else if (actionredirect.equalsIgnoreCase("hide")) {
				homepage2.setStatus(false);
				homepageDao.update(homepage2);
				request.setAttribute("navigation", "../cms/pages/home/leftContent.jsp");
			}
			
			else if (actionredirect.equalsIgnoreCase("updateall")) {					
				String ctype = request.getParameter("contentType");
				request.setAttribute("coltype", "left");			
					if (ctype.equalsIgnoreCase("pdf")) {
						request.setAttribute("navigation", "../cms/pages/home/updatepdfContent.jsp");
					}
					else if (ctype.equalsIgnoreCase("html")) {
						request.setAttribute("navigation", "../cms/pages/home/updateContent.jsp");
					}
					else if (ctype.equalsIgnoreCase("subhtml")) {
						request.setAttribute("navigation", "../cms/pages/home/updateSubHTMLContent.jsp");
					}
					else if (ctype.equalsIgnoreCase("subpdf")) {
                        request.setAttribute("navigation", "../cms/pages/home/updatePDFContent.jsp");
                    }
			}				
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}	
	
		else if (coltype.equalsIgnoreCase("right")) {
			if (actionredirect.equalsIgnoreCase("show")) {
				homepage2.setStatus(true);
				homepageDao.update(homepage2);
				request.setAttribute("navigation", "../cms/pages/home/rightContentList.jsp");
			}
			else if (actionredirect.equalsIgnoreCase("hide")) {
				homepage2.setStatus(false);
				homepageDao.update(homepage2);
				request.setAttribute("navigation", "../cms/pages/home/rightContentList.jsp");
			}
			else if (actionredirect.equalsIgnoreCase("updateall")) {					
				String ctype = request.getParameter("contentType");
				request.setAttribute("coltype", "right");			
					if (ctype.equalsIgnoreCase("pdf")) {
						request.setAttribute("navigation", "../cms/pages/home/updatepdfContent.jsp");
					}
					else if (ctype.equalsIgnoreCase("html")) {
						request.setAttribute("navigation", "../cms/pages/home/updateContent.jsp");
					}
			}
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
		
		else if (coltype.equalsIgnoreCase("center")) {

			if (actionredirect.equalsIgnoreCase("show")) {
				homepage2.setStatus(true);
				homepageDao.update(homepage2);
				request.setAttribute("navigation", "../cms/pages/home/centerContentList.jsp");
			}
			else if (actionredirect.equalsIgnoreCase("hide")) {
				homepage2.setStatus(false);
				homepageDao.update(homepage2);
				request.setAttribute("navigation", "../cms/pages/home/centerContentList.jsp");
			}
			else if (actionredirect.equalsIgnoreCase("updateall")) {
				request.setAttribute("coltype", "center");
				String ctype = request.getParameter("contentType");			
				if (ctype.equalsIgnoreCase("pdf")) {
					request.setAttribute("navigation", "../cms/pages/home/updatepdfContent.jsp");
				}
				else if (ctype.equalsIgnoreCase("html")) {
					request.setAttribute("navigation", "../cms/pages/home/updatehtmlContent.jsp");
				}
			}
			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
			dispatcher.forward(request, response);
			
		}
		
	}
}
