package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.exceptions.PersistenceException;

import gov.bfar.cms.dao.UserDao;
import gov.bfar.cms.domain.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/authenticate")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String LOGIN_PAGE = "/cms/pages/login/login.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			login(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			getException(e, request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, PersistenceException {
		try {
			login(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			getException(e, request, response);
		}

	}

	private void getException(Exception e, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (e instanceof PersistenceException) {
			request.setAttribute("invalidpw", "Cannot connect to the database");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(LOGIN_PAGE);
			dispatcher.forward(request, response);
		} else if (e instanceof NullPointerException) {
			if (session.getAttribute("userid") == null) {
				request.setAttribute("invalidpw", "Wrong Username or Password!");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(LOGIN_PAGE);
				dispatcher.forward(request, response);
			}
		}

		else if (e instanceof SQLException) {
			request.setAttribute("invalidpw", e);
		} else if (e instanceof IllegalStateException) {
			request.setAttribute("invalidpw", e);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(LOGIN_PAGE);
			dispatcher.forward(request, response);
		} else {
			request.setAttribute("invalidpw", "Please Login First!");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(LOGIN_PAGE);
			dispatcher.forward(request, response);
		}

	}

	protected void login(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NullPointerException, PersistenceException {
		HttpSession session = request.getSession();
		String username = request.getParameter("username");
		String userpass = request.getParameter("userpass");

		UserDao userDao = new UserDao();
		User user = userDao.login(username, userpass);
		session.setAttribute("userid", user.getId());
		session.setAttribute("userinfo", user);
		response.sendRedirect(request.getContextPath() + "/cms/cmsIndex.jsp");

	}

}
