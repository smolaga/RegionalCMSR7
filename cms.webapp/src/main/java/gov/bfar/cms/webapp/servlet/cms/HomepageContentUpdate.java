package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.HomepageDao;
import gov.bfar.cms.domain.Homepage;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateStatus
 */
@WebServlet("/HomepageContentUpdate")
public class HomepageContentUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomepageContentUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updateStatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updateStatus(request, response);
	}

    protected void updateStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String actionredirect = request.getParameter("status");
		String activetype = request.getParameter("activeat");
		String coltype = request.getParameter("columnType");
		String id = request.getParameter("id");
		String changecontenttype = "";
		changecontenttype = request.getParameter("cctype");
		session.setAttribute("tid", id);
		HomepageDao homepageDao = new HomepageDao();
		Homepage homepage = homepageDao.findById(id);
		if (changecontenttype != null) {
		    if (changecontenttype.equalsIgnoreCase("cctpdf")) {
	            request.setAttribute("changecontenttype", "pdf");
	            homepage.setContentType("pdf");
	        }else if(changecontenttype.equalsIgnoreCase("ccthtml")){
	            request.setAttribute("changecontenttype", "html");
	            homepage.setContentType("html");
	        }
        }
		Homepage homepage2 = new Homepage();
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		homepage2.setId(homepage.getId());
		homepage2.setHead(homepage.getHead());
		homepage2.setShortContent(homepage.getShortContent());
		homepage2.setContent(homepage.getContent());
		homepage2.setSeeAllLink(homepage.getSeeAllLink());
		homepage2.setPageLink(homepage.getPageLink());
		homepage2.setSubContentLink(homepage.getSubContentLink());
		homepage2.setLinkStatus(homepage.getLinkStatus());
		homepage2.setColumnType(homepage.getColumnType());
		homepage2.setContentType(homepage.getContentType());
		homepage2.setPdfFilePath(homepage.getPdfFilePath());
		homepage2.setHeadid(homepage.getHeadid());
		homepage2.setSequence(homepage.getSequence());
		homepage2.setModifiedDate(date.get());
		homepage2.setModifiedBy(homepage.getCreatedBy());
		
		if (coltype.equalsIgnoreCase("left")) {
			if (actionredirect.equalsIgnoreCase("show")) {
				
				if (activetype.equalsIgnoreCase("vop")) {
					homepage2.vopactivate();
					homepage2.setViewonlist(homepage.isViewonlist());
				}else if (activetype.equalsIgnoreCase("vol")) {
					homepage2.volactivate();
					homepage2.setViewonpage(homepage.isViewonpage());
				}else if(activetype.equalsIgnoreCase("sub") || activetype.equalsIgnoreCase("title")){
					homepage2.activate();
				}
				
				homepageDao.update(homepage2);
				if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
				    request.setAttribute("navigation", "../cms/pages/homepageleft/leftContent.jsp");
				}else if(homepage.getContentType().equalsIgnoreCase("html") || homepage.getContentType().equalsIgnoreCase("pdf")){
                    request.setAttribute("tid", homepage.getHeadid());
                    request.setAttribute("navigation", "../cms/pages/homepageleft/leftMainContentList.jsp");
                }
				else if(homepage.getContentType().equalsIgnoreCase("subhtml") || homepage.getContentType().equalsIgnoreCase("subpdf")){
				    request.setAttribute("tabactive", "sub");
				    request.setAttribute("navigation", "../cms/pages/homepageleft/leftContent.jsp");
                }
				
			}
			else if (actionredirect.equalsIgnoreCase("hide")) {
				
				if (activetype.equalsIgnoreCase("vop")) {
					homepage2.vopdeactive();
					homepage2.setViewonlist(homepage.isViewonlist());
				}else if (activetype.equalsIgnoreCase("vol")) {
					homepage2.voldeactive();
					homepage2.setViewonpage(homepage.isViewonpage());
				}else if(activetype.equalsIgnoreCase("sub") || activetype.equalsIgnoreCase("title")){
					homepage2.deactive();
				}
				
				homepageDao.update(homepage2);
				if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
                    request.setAttribute("navigation", "../cms/pages/homepageleft/leftContent.jsp");    
                }else if(homepage.getContentType().equalsIgnoreCase("html") || homepage.getContentType().equalsIgnoreCase("pdf")) {
                    request.setAttribute("tid", homepage.getHeadid());
                    request.setAttribute("navigation", "../cms/pages/homepageleft/leftMainContentList.jsp");
                }
                else if(homepage.getContentType().equalsIgnoreCase("subhtml") || homepage.getContentType().equalsIgnoreCase("subpdf")){
                    request.setAttribute("tabactive", "sub");
                    request.setAttribute("navigation", "../cms/pages/homepageleft/leftContent.jsp");
                }
			}
			
			else if (actionredirect.equalsIgnoreCase("updateall")) {
			    if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
	                request.setAttribute("tid", request.getParameter("id"));
	                request.setAttribute("navigation", "../cms/pages/homepageleft/leftMainContentList.jsp");
                }else if (homepage.getContentType().equalsIgnoreCase("html")) {
                        request.setAttribute("tid", homepage.getHeadid());
                        request.setAttribute("htmlmcid", homepage.getId());
                        request.setAttribute("updatemainhtmlpage", "../homepageleft/updateHTMLMainContent.jsp");
                        request.setAttribute("navigation", "../cms/pages/homepageleft/leftMainContentList.jsp"); 
                }else if (homepage.getContentType().equalsIgnoreCase("pdf")) {
                        request.setAttribute("tid", homepage.getHeadid());
                        request.setAttribute("pdfmcid", homepage.getId());
                        request.setAttribute("updatemainpdfpage", "../homepageleft/updatePDFMainContent.jsp");
                        request.setAttribute("navigation", "../cms/pages/homepageleft/leftMainContentList.jsp"); 
                }else if(homepage.getContentType().equalsIgnoreCase("subhtml")){
				    request.setAttribute("subhtmlid", homepage.getId());
				    request.setAttribute("navigation", "../cms/pages/homepageleft/updateSubHTMLContent.jsp");
                }
				else if( homepage.getContentType().equalsIgnoreCase("subpdf")){
				    request.setAttribute("subpdfid", homepage.getId());
				    request.setAttribute("navigation", "../cms/pages/homepageleft/updateSubPDFContent.jsp");
                }
                      
			}	
			
		 else if (actionredirect.equalsIgnoreCase("delete")) {
			if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
				homepageDao.delete(id);
				request.setAttribute("navigation", "../cms/pages/homepageleft/leftContent.jsp");
			} else if (homepage.getContentType().equalsIgnoreCase("html")
					|| homepage.getContentType().equalsIgnoreCase("pdf")) {
				homepageDao.delete(id);
				request.setAttribute("tid", homepage.getHeadid());
				request.setAttribute("navigation", "../cms/pages/homepageleft/leftMainContentList.jsp");
			} else if (homepage.getContentType().equalsIgnoreCase("subhtml")
					|| homepage.getContentType().equalsIgnoreCase("subpdf")) {
				homepageDao.delete(id);
				request.setAttribute("tabactive", "sub");
				request.setAttribute("navigation", "../cms/pages/homepageleft/leftContent.jsp");
			}
		 }
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
		 	}	
	
		else if (coltype.equalsIgnoreCase("right")) {
			if (actionredirect.equalsIgnoreCase("show")) {
				
				if (activetype.equalsIgnoreCase("vop")) {
					homepage2.vopactivate();
					homepage2.setViewonlist(homepage.isViewonlist());
				}else if (activetype.equalsIgnoreCase("vol")) {
					homepage2.volactivate();
					homepage2.setViewonpage(homepage.isViewonpage());
				}else if(activetype.equalsIgnoreCase("sub") || activetype.equalsIgnoreCase("title")){
					homepage2.activate();
				}
				
				homepageDao.update(homepage2);
				if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
				    request.setAttribute("navigation", "../cms/pages/homepageright/rightContent.jsp");
				}else if(homepage.getContentType().equalsIgnoreCase("html") || homepage.getContentType().equalsIgnoreCase("pdf")){
                    request.setAttribute("tid", homepage.getHeadid());
                    request.setAttribute("navigation", "../cms/pages/homepageright/rightMainContentList.jsp");
                }
				else if(homepage.getContentType().equalsIgnoreCase("subhtml") || homepage.getContentType().equalsIgnoreCase("subpdf")){
				    request.setAttribute("tabactive", "sub");
				    request.setAttribute("navigation", "../cms/pages/homepageright/rightContent.jsp");
                }
				
			}
			else if (actionredirect.equalsIgnoreCase("hide")) {
				
				if (activetype.equalsIgnoreCase("vop")) {
					homepage2.vopdeactive();
					homepage2.setViewonlist(homepage.isViewonlist());
				}else if (activetype.equalsIgnoreCase("vol")) {
					homepage2.voldeactive();
					homepage2.setViewonpage(homepage.isViewonpage());
				}else if(activetype.equalsIgnoreCase("sub") || activetype.equalsIgnoreCase("title")){
					homepage2.deactive();
				}
				
				homepageDao.update(homepage2);
				if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
                    request.setAttribute("navigation", "../cms/pages/homepageright/rightContent.jsp");    
                }else if(homepage.getContentType().equalsIgnoreCase("html") || homepage.getContentType().equalsIgnoreCase("pdf")) {
                    request.setAttribute("tid", homepage.getHeadid());
                    request.setAttribute("navigation", "../cms/pages/homepageright/rightMainContentList.jsp");
                }
                else if(homepage.getContentType().equalsIgnoreCase("subhtml") || homepage.getContentType().equalsIgnoreCase("subpdf")){
                    request.setAttribute("tabactive", "sub");
                    request.setAttribute("navigation", "../cms/pages/homepageright/rightContent.jsp");
                }
			}
			
			else if (actionredirect.equalsIgnoreCase("updateall")) {
			    if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
	                request.setAttribute("tid", request.getParameter("id"));
	                request.setAttribute("navigation", "../cms/pages/homepageright/rightMainContentList.jsp");
                }else if (homepage.getContentType().equalsIgnoreCase("html")) {
                        request.setAttribute("tid", homepage.getHeadid());
                        request.setAttribute("htmlmcid", homepage.getId());
                        request.setAttribute("updatemainhtmlpage", "../homepageright/updateHTMLMainContent.jsp");
                        request.setAttribute("navigation", "../cms/pages/homepageright/rightMainContentList.jsp"); 
                }else if (homepage.getContentType().equalsIgnoreCase("pdf")) {
                        request.setAttribute("tid", homepage.getHeadid());
                        request.setAttribute("pdfmcid", homepage.getId());
                        request.setAttribute("updatemainpdfpage", "../homepageright/updatePDFMainContent.jsp");
                        request.setAttribute("navigation", "../cms/pages/homepageright/rightMainContentList.jsp"); 
                }else if(homepage.getContentType().equalsIgnoreCase("subhtml")){
				    request.setAttribute("subhtmlid", homepage.getId());
				    request.setAttribute("navigation", "../cms/pages/homepageright/updateSubHTMLContent.jsp");
                }
				else if( homepage.getContentType().equalsIgnoreCase("subpdf")){
				    request.setAttribute("subpdfid", homepage.getId());
				    request.setAttribute("navigation", "../cms/pages/homepageright/updateSubPDFContent.jsp");
                }
                      
			}	
			else if (actionredirect.equalsIgnoreCase("delete")) {

				if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
					homepageDao.delete(id);
					request.setAttribute("navigation", "../cms/pages/homepageright/rightContent.jsp");
				} else if (homepage.getContentType().equalsIgnoreCase("html")
						|| homepage.getContentType().equalsIgnoreCase("pdf")) {
					homepageDao.delete(id);
					request.setAttribute("tid", homepage.getHeadid());
					request.setAttribute("navigation", "../cms/pages/homepageright/rightMainContentList.jsp");
				} else if (homepage.getContentType().equalsIgnoreCase("subhtml")
						|| homepage.getContentType().equalsIgnoreCase("subpdf")) {
					homepageDao.delete(id);
					request.setAttribute("tabactive", "sub");
					request.setAttribute("navigation", "../cms/pages/homepageright/rightContent.jsp");
				}
			}
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
		
		else if (coltype.equalsIgnoreCase("center")) {
			if (actionredirect.equalsIgnoreCase("show")) {
				
				if (activetype.equalsIgnoreCase("vop")) {
					homepage2.vopactivate();
					homepage2.setViewonlist(homepage.isViewonlist());
				}else if (activetype.equalsIgnoreCase("vol")) {
					homepage2.volactivate();
					homepage2.setViewonpage(homepage.isViewonpage());
				}else if(activetype.equalsIgnoreCase("sub") || activetype.equalsIgnoreCase("title")){
					homepage2.activate();
				}
				
				homepageDao.update(homepage2);
				if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
				    request.setAttribute("navigation", "../cms/pages/homepagecenter/centerContent.jsp");
				}else if(homepage.getContentType().equalsIgnoreCase("html") || homepage.getContentType().equalsIgnoreCase("pdf")){
                    request.setAttribute("tid", homepage.getHeadid());
                    request.setAttribute("navigation", "../cms/pages/homepagecenter/centerMainContentList.jsp");
                }
				else if(homepage.getContentType().equalsIgnoreCase("subhtml") || homepage.getContentType().equalsIgnoreCase("subpdf")){
				    request.setAttribute("tabactive", "sub");
				    request.setAttribute("navigation", "../cms/pages/homepagecenter/centerContent.jsp");
                }
				
			}
			else if (actionredirect.equalsIgnoreCase("hide")) {
				
				if (activetype.equalsIgnoreCase("vop")) {
					homepage2.vopdeactive();
					homepage2.setViewonlist(homepage.isViewonlist());
				}else if (activetype.equalsIgnoreCase("vol")) {
					homepage2.voldeactive();
					homepage2.setViewonpage(homepage.isViewonpage());
				}else if(activetype.equalsIgnoreCase("sub") || activetype.equalsIgnoreCase("title")){
					homepage2.deactive();
				}
				
				homepageDao.update(homepage2);
				if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
                    request.setAttribute("navigation", "../cms/pages/homepagecenter/centerContent.jsp");    
                }else if(homepage.getContentType().equalsIgnoreCase("html") || homepage.getContentType().equalsIgnoreCase("pdf")) {
                    request.setAttribute("tid", homepage.getHeadid());
                    request.setAttribute("navigation", "../cms/pages/homepagecenter/centerMainContentList.jsp");
                }
                else if(homepage.getContentType().equalsIgnoreCase("subhtml") || homepage.getContentType().equalsIgnoreCase("subpdf")){
                    request.setAttribute("tabactive", "sub");
                    request.setAttribute("navigation", "../cms/pages/homepagecenter/centerContent.jsp");
                }
			}
			
			else if (actionredirect.equalsIgnoreCase("updateall")) {
			    System.out.println(homepage.getContentType());
			    if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
	                request.setAttribute("tid", request.getParameter("id"));
	                request.setAttribute("navigation", "../cms/pages/homepagecenter/centerMainContentList.jsp");
                }else if (homepage.getContentType().equalsIgnoreCase("html")) {
                        request.setAttribute("tid", homepage.getHeadid());
                        request.setAttribute("htmlmcid", homepage.getId());
                        request.setAttribute("updatemainhtmlpage", "../homepagecenter/updateHTMLMainContent.jsp");
                        request.setAttribute("navigation", "../cms/pages/homepagecenter/centerMainContentList.jsp"); 
                }else if (homepage.getContentType().equalsIgnoreCase("pdf")) {
                        request.setAttribute("tid", homepage.getHeadid());
                        request.setAttribute("pdfmcid", homepage.getId());
                        request.setAttribute("updatemainpdfpage", "../homepagecenter/updatePDFMainContent.jsp");
                        request.setAttribute("navigation", "../cms/pages/homepagecenter/centerMainContentList.jsp"); 
                }else if(homepage.getContentType().equalsIgnoreCase("subhtml")){
				    request.setAttribute("subhtmlid", homepage.getId());
				    request.setAttribute("navigation", "../cms/pages/homepagecenter/updateSubHTMLContent.jsp");
                }
				else if( homepage.getContentType().equalsIgnoreCase("subpdf")){
				    request.setAttribute("subpdfid", homepage.getId());
				    request.setAttribute("navigation", "../cms/pages/homepagecenter/updateSubPDFContent.jsp");
                }
                      
			}	
			else if (actionredirect.equalsIgnoreCase("delete")) {

				if (homepage.getContentType().equalsIgnoreCase("maintitle")) {
					homepageDao.delete(id);
					request.setAttribute("navigation", "../cms/pages/homepagecenter/centerContent.jsp");
				} else if (homepage.getContentType().equalsIgnoreCase("html")
						|| homepage.getContentType().equalsIgnoreCase("pdf")) {
					homepageDao.delete(id);
					request.setAttribute("tid", homepage.getHeadid());
					request.setAttribute("navigation", "../cms/pages/homepagecenter/centerMainContentList.jsp");
				} else if (homepage.getContentType().equalsIgnoreCase("subhtml")
						|| homepage.getContentType().equalsIgnoreCase("subpdf")) {
					homepageDao.delete(id);
					request.setAttribute("tabactive", "sub");
					request.setAttribute("navigation", "../cms/pages/homepagecenter/centerContent.jsp");
				}
			}
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}	
		
	}
}
