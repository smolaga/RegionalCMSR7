package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.OtherlinksDao;
import gov.bfar.cms.domain.Otherlinks;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class OtherLinksUpdateStatus
 */
@WebServlet("/OtherLinksUpdateStatus")
public class OtherLinksUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OtherLinksUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        updatestatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatestatus(request, response);
	}
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionredirect = request.getParameter("status");
        String id = request.getParameter("contentid");
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        request.setAttribute("getidcontent", id);
        Optional<String> date= DateUtil.getFormattedDate("MM/dd/yy hh:mm:ss aa");
        OtherlinksDao otherlinksDao = new OtherlinksDao();
        Otherlinks otherlinks = otherlinksDao.findById(id);
        Otherlinks otherlinks2 = new Otherlinks();
        otherlinks2.setId(otherlinks.getId());
        otherlinks2.setTitle(otherlinks.getTitle());
        otherlinks2.setContent(otherlinks.getContent());
        otherlinks2.setPageLink(otherlinks.getPageLink());
        otherlinks2.setModifiedBy(userid);
        otherlinks2.setModifiedDate(date.get());
       
        
        if (actionredirect.equalsIgnoreCase("show")) {
            otherlinks2.setStatus(true);
            otherlinksDao.update(otherlinks2);
            otherlinksDao.setOneActiveStatus(otherlinks.getId());
            request.setAttribute("navigation", "../cms/pages/otherLinks/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("hide")) {
            otherlinks2.setStatus(false);
            otherlinksDao.update(otherlinks2);
            request.setAttribute("navigation", "../cms/pages/otherLinks/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("updateall")) {
            request.setAttribute("navigation", "../cms/pages/otherLinks/updateContent.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("delete")) {			
        	otherlinksDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/otherLinks/index.jsp");
		}
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
        dispatcher.forward(request, response);
    }

}
