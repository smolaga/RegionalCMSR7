package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.AboutUsDao;
import gov.bfar.cms.domain.AboutUs;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertAboutUs
 */
@WebServlet("/InsertAboutUs")
public class InsertAboutUs extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertAboutUs() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		insertContent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		insertContent(request, response);
	}
	
	protected void insertContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		HttpSession session = request.getSession();
		String userid = (String)session.getAttribute("userid");
		String uuid = BaseDomain.newUUID();
		AboutUs aboutUs = new AboutUs();
		AboutUsDao aboutUsDao = new AboutUsDao();
		aboutUs.setId(uuid);
		aboutUs.setTitle(request.getParameter("title"));
		aboutUs.setContent(request.getParameter("content"));
		aboutUs.setContentType("html");
		aboutUs.setPageLink("/WebsiteNavigation?id="+aboutUs.getId()+"&pageAction=aboutContentFrame");
		aboutUs.setSequence(Integer.parseInt(request.getParameter("sequence")));
		aboutUs.setStatus(false);
		aboutUs.setDateCreated(date.get());
		aboutUs.setCreatedBy(userid);
		aboutUs.setModifiedDate(date.get());
        aboutUs.setModifiedBy(userid);
		aboutUsDao.save(aboutUs);
		aboutUsDao.spAutoOrdering(aboutUs.getSequence(), aboutUs.getId(),0);
		session.setAttribute("tabactiveaboutus", "main");
		response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=about");
		
	}

}
