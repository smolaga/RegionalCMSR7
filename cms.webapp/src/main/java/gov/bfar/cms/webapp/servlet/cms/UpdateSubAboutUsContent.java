package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.AboutUsDao;
import gov.bfar.cms.domain.AboutUs;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateSubProfileContent
 */
@WebServlet("/UpdateSubAboutUsContent")
public class UpdateSubAboutUsContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateSubAboutUsContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatesub(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatesub(request, response);
	}
	protected void updatesub(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    AboutUs aboutUs = new AboutUs();
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
	    AboutUsDao profileDao = new AboutUsDao();
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        aboutUs.setId(request.getParameter("id"));
        aboutUs.setTitle(request.getParameter("title"));
        aboutUs.setContent(request.getParameter("content"));
        aboutUs.setContentType("subhtml");
        aboutUs.setStatus(Boolean.parseBoolean(request.getParameter("status")));
        aboutUs.setModifiedBy(userid);
        aboutUs.setSubContentLink(request.getParameter("pagelink"));
        aboutUs.setModifiedDate(date.get());
        profileDao.update(aboutUs);
        
        session.setAttribute("tabactiveaboutus", "sub");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=about");
        
    }

}
