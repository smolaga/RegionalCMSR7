package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.DownloadDao;
import gov.bfar.cms.domain.Download;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateSubDownloadContent
 */
@WebServlet("/UpdateSubDownloadContent")
public class UpdateSubDownloadContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateSubDownloadContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        updatesub(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatesub(request, response);
	}
	protected void updatesub(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Download download = new Download();
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
	    DownloadDao downloadDao = new DownloadDao();
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        download.setId(request.getParameter("id"));
        download.setTitle(request.getParameter("title"));
        download.setContent(request.getParameter("content"));
        download.setContentType("subhtml");
        download.setStatus(Boolean.parseBoolean(request.getParameter("status")));
        download.setModifiedBy(userid);
        download.setSubContentLink(request.getParameter("pagelink"));
        download.setModifiedDate(date.get());
        downloadDao.update(download);
        
        session.setAttribute("tabactivedownload", "sub");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=download");
 
    }

}
