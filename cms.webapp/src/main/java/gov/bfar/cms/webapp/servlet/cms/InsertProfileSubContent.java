package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ProfileDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Profile;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertProfileSubContent
 */
@WebServlet("/InsertProfileSubContent")
public class InsertProfileSubContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertProfileSubContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
	    subcontent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    subcontent(request, response);
	}
	protected void subcontent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        Profile profile = new Profile();
        ProfileDao profileDao = new ProfileDao();
        profile.setId(uuid);
        profile.setTitle(request.getParameter("title"));
        profile.setContent(request.getParameter("content"));
        profile.setContentType("subhtml");
        profile.setSubContentLink("/WebsiteNavigation?id="+profile.getId()+"&pageAction=profileContentFrame");
        profile.setStatus(true);
        profile.setDateCreated(date.get());
        profile.setCreatedBy(userid);
        profile.setModifiedDate(date.get());
        profile.setModifiedBy(userid);
        profileDao.save(profile);
        
        session.setAttribute("tabactiveprofile", "sub");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=profile");
    }


}
