package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.FooterDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Footer;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertFooter
 */
@WebServlet("/InsertFooter")
public class InsertFooter extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertFooter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    insertfooter(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    insertfooter(request, response);
	}
	protected void insertfooter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        String pageaction = request.getParameter("action");
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        String type = request.getParameter("type");
        FooterDao footerDao = new FooterDao();
        Footer footer = new Footer();
        if (type.equalsIgnoreCase("footer")) {	        
		        footer.setId(uuid);
		        footer.setTitle(request.getParameter("title"));
		        footer.setContent(request.getParameter("content"));
		        footer.setContentType("footer");
		        footer.setPageLink("/WebsiteNavigation");
		        footer.setStatus(false);
		        footer.setDateCreated(date.get());
		        footer.setCreatedBy(userid);
		        footer.setModifiedDate(date.get());
		        footer.setModifiedBy(userid);
		        footerDao.save(footer);		        
		        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=footer");
		        
        }else if(type.equalsIgnoreCase("cmsfooter")) {
	        try {
	            if (pageaction.equalsIgnoreCase("Save")) {
	            	footer.setId(uuid);
	 		        footer.setTitle(request.getParameter("ftitle"));
	 		        footer.setContent(request.getParameter("fcontent"));	 
	 		        footer.setContentType("cmsfooter");
	 		        footer.setPageLink("/WebsiteNavigation");
	 		        footer.setStatus(true);
	 		        footer.setDateCreated(date.get());
	 		        footer.setCreatedBy(userid);
	 		        footerDao.save(footer);	
	                request.setAttribute("insertfootermsg", "SUCCESSFULLY STORED!!!");
	            }else if(pageaction.equalsIgnoreCase("Update")){	
	                System.out.println("test");
	            	footer.setId(request.getParameter("fid"));
	            	footer.setTitle(request.getParameter("ftitle"));
	 		        footer.setContent(request.getParameter("fcontent"));
	 		        footer.setPageLink("/WebsiteNavigation");
	            	footer.setStatus(true);
	            	footer.setModifiedDate(date.get());
	            	footer.setModifiedBy(userid);
	            	footer.setContentType("cmsfooter");
	 		        footerDao.update(footer);
	                session.setAttribute("insertfootermsg", "SUCCESSFULLY UPDATED!!!");
	            }
	           
	         } catch (Exception e) {
	        	 session.setAttribute("insertfootermsg", "FAILED");
	         }
	        
	        Footer fup = footerDao.OneFooterDisplay();
	        try {
	        	session.setAttribute("ftitle", fup.getTitle());
	        	session.setAttribute("fcontent", fup.getContent());
	        	session.setAttribute("fid", fup.getId());
	        	session.setAttribute("upfmin", "true");
	        } catch (Exception e) {
	        	session.setAttribute("upfmin", "false");
	        }
	        session.setAttribute("tabactivesettings", "cmsfooter");
	        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=settings");
	     
        
    }
}
	}