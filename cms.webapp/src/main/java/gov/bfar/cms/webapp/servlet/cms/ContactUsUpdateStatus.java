package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ContactUsDao;
import gov.bfar.cms.domain.ContactUs;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class ContactUsUpdateStatus
 */
@WebServlet("/ContactUsUpdateStatus")
public class ContactUsUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContactUsUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatestatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatestatus(request, response);
	}
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String actionredirect = request.getParameter("status");
        String id = request.getParameter("contentid");
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        request.setAttribute("getidcontent", id);
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        ContactUsDao contactUsDao = new ContactUsDao();
        ContactUs contactUs = contactUsDao.findById(id);       
        ContactUs contactUs2 = new ContactUs();
        contactUs2.setId(contactUs.getId());
        contactUs2.setTitle(contactUs.getTitle());
        contactUs2.setContent(contactUs.getContent());
        contactUs2.setPageLink(contactUs.getPageLink());
        contactUs2.setModifiedBy(userid);
        contactUs2.setModifiedDate(date.get());
        
        if (actionredirect.equalsIgnoreCase("show")) {
            contactUs2.setStatus(true);
            contactUsDao.update(contactUs2);
            contactUsDao.setOneActiveStatus(contactUs.getId());
            request.setAttribute("navigation", "../cms/pages/contactus/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("hide")) {
            contactUs2.setStatus(false);
            contactUsDao.update(contactUs2);
            request.setAttribute("navigation", "../cms/pages/contactus/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("updateall")) {
            request.setAttribute("navigation", "../cms/pages/contactus/updateContent.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("delete")) {			
        	contactUsDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/contactus/index.jsp");
		}
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
        dispatcher.forward(request, response);
    }

}
