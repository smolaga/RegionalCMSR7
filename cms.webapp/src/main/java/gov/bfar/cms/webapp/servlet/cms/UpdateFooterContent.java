package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.FooterDao;
import gov.bfar.cms.domain.Footer;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateFooterContent
 */
@WebServlet("/UpdateFooterContent")
public class UpdateFooterContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateFooterContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatefooter(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatefooter(request, response);
	}
	protected void updatefooter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM-dd-yyyy hh:mm:ss aa");
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
	    FooterDao footerDao = new FooterDao();
	    Footer footer = new Footer();
	    footer.setId(request.getParameter("id"));
	    footer.setTitle(request.getParameter("title"));
        footer.setContent(request.getParameter("content"));
        footer.setContentType("footer");
        footer.setStatus(Boolean.parseBoolean(request.getParameter("status")));
        footer.setContentType("footer");
        footer.setModifiedBy(userid);
        footer.setPageLink(request.getParameter("pagelink"));
        footer.setModifiedDate(date.get());
        footerDao.update(footer);        
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=footer");     
    }
}

