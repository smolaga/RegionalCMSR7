package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.LawsDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Laws;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertLaws
 */
@WebServlet("/InsertLaws")
public class InsertLaws extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertLaws() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        insertcontent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    insertcontent(request, response);
	}
	protected void insertcontent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        Laws laws = new Laws();
        LawsDao lawsDao = new LawsDao();
        laws.setId(uuid);
        laws.setTitle(request.getParameter("title"));
        laws.setContent(request.getParameter("content"));
        laws.setContentType("html");
        laws.setPageLink("/WebsiteNavigation?id="+laws.getId()+"&pageAction=lawsContentFrame");
        laws.setStatus(false);
        laws.setDateCreated(date.get());
        laws.setCreatedBy(userid);
        laws.setModifiedDate(date.get());
        laws.setModifiedBy(userid);
        lawsDao.save(laws);
        
        session.setAttribute("tabactivelaws", "main");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=laws");
        
    }

}

