package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.LawsDao;
import gov.bfar.cms.domain.Laws;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class LawsUpdateStatus
 */
@WebServlet("/LawsUpdateStatus")
public class LawsUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LawsUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        updatestatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatestatus(request, response);
	}
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String actionredirect = request.getParameter("status");
        String id = request.getParameter("contentid");
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        request.setAttribute("getidcontent", id);
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        LawsDao lawsDao = new LawsDao();
        Laws laws = lawsDao.findById(id);
        Laws laws2 = new Laws();
        laws2.setId(laws.getId());
        laws2.setTitle(laws.getTitle());
        laws2.setContent(laws.getContent());
        laws2.setContentType(laws.getContentType());
        laws2.setPdfFilePath(laws.getPdfFilePath());
        laws2.setPageLink(laws.getPageLink());
        laws2.setModifiedBy(userid);
        laws2.setModifiedDate(date.get());
        
        if (actionredirect.equalsIgnoreCase("show")) {
            laws2.setStatus(true);
            lawsDao.update(laws2);
            lawsDao.setOneActiveStatus(laws.getId());
            request.setAttribute("navigation", "../cms/pages/laws/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("hide")) {
            laws2.setStatus(false);
            lawsDao.update(laws2);
            request.setAttribute("navigation", "../cms/pages/laws/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("updateall")) {
            String ctype = request.getParameter("contenttype");
            if (ctype.equalsIgnoreCase("html")) {
                request.setAttribute("navigation", "../cms/pages/laws/updateContent.jsp");
            }
            else if (ctype.equalsIgnoreCase("subhtml")) {
                request.setAttribute("navigation", "../cms/pages/laws/updateSubHTMLContent.jsp");
            }
            else if (ctype.equalsIgnoreCase("subpdf")) {
                request.setAttribute("navigation", "../cms/pages/laws/updateSubPDFContent.jsp");
            }
        }
        else if (actionredirect.equalsIgnoreCase("delete")) {			
        	lawsDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/laws/index.jsp");
		}
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
        dispatcher.forward(request, response);
    }

}
