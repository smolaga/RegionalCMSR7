package gov.bfar.cms.webapp.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationUtil {
	private static Properties prop = new Properties();
	private static Thread currentThread = Thread.currentThread();
	private static ClassLoader contextClassLoader = currentThread.getContextClassLoader();
	
	/*private static InputStream propertiesStream = contextClassLoader.getResourceAsStream("web-config.apache.properties");*/
	private static InputStream propertiesStream = contextClassLoader.getResourceAsStream("web-config.local.properties");
	/*private static InputStream propertiesStream = contextClassLoader.getResourceAsStream("web-config.server.properties");*/
	static {
		try {
			prop.load(propertiesStream);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			e.getMessage();
			
		}
	}

	public static String getConfig(String key) {
		return prop.getProperty(key);
	}
}