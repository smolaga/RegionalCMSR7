package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.DownloadDao;
import gov.bfar.cms.domain.Download;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class DownloadUpdateStatus
 */
@WebServlet("/DownloadUpdateStatus")
public class DownloadUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        updatestatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatestatus(request, response);
	}
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String actionredirect = request.getParameter("status");
        String id = request.getParameter("contentid");
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        request.setAttribute("getidcontent", id);
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        DownloadDao downloadDao = new DownloadDao();
        Download download = downloadDao.findById(id);
        Download downloads = new Download();
        downloads.setId(download.getId());
        downloads.setTitle(download.getTitle());
        downloads.setContent(download.getContent());
        downloads.setPageLink(download.getPageLink());
        downloads.setSequence(download.getSequence());
        downloads.setContentType(download.getContentType());
        downloads.setPdfFilePath(download.getPdfFilePath());               
        downloads.setModifiedBy(userid);
        downloads.setModifiedDate(date.get());
      
        
        if (actionredirect.equalsIgnoreCase("show")) {
            downloads.setStatus(true);
            downloadDao.update(downloads);
            request.setAttribute("navigation", "../cms/pages/download/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("hide")) {
            downloads.setStatus(false);
            downloadDao.update(downloads);
            request.setAttribute("navigation", "../cms/pages/download/index.jsp");
        }
        else if (actionredirect.equalsIgnoreCase("updateall")) {
            String ctype = request.getParameter("contenttype");
            if (ctype.equalsIgnoreCase("pdf")) {
                request.setAttribute("navigation", "../cms/pages/download/updatepdfContent.jsp");
            }
            else if (ctype.equalsIgnoreCase("html")) {
                request.setAttribute("navigation", "../cms/pages/download/updateContent.jsp");
            }
            else if (ctype.equalsIgnoreCase("subhtml")) {
                request.setAttribute("navigation", "../cms/pages/download/updateSubHTMLContent.jsp");
            }
            else if (ctype.equalsIgnoreCase("subpdf")) {
                request.setAttribute("navigation", "../cms/pages/download/updateSubPDFContent.jsp");
            }
        }
        else if (actionredirect.equalsIgnoreCase("delete")) {			
        	downloadDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/download/index.jsp");
		}
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
        dispatcher.forward(request, response);
    }
}
