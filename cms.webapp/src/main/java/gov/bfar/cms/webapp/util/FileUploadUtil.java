package gov.bfar.cms.webapp.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FileUploadUtil {
	private static Properties prop = new Properties();
	private static Thread currentThread = Thread.currentThread();
	private static ClassLoader contextClassLoader = currentThread.getContextClassLoader();
	
	private static InputStream propertiesStream = contextClassLoader.getResourceAsStream("file-config.properties");
	static {
		try {
			prop.load(propertiesStream);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			e.getMessage();
			
		}
	}

	public static String getConfig(String key) {
		return prop.getProperty(key);
	}
}