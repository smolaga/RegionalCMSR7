package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.EmailInquiryDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.EmailInquiry;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertEmailInquiry
 */
@WebServlet("/EmailInquirySender")
public class EmailInquirySender extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String CMS_INDEX = "/cms/cmsIndex.jsp";  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmailInquirySender() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        insertEmail(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    insertEmail(request, response);
	}
	protected void insertEmail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   String pageaction = request.getParameter("action");
	   String eiid = request.getParameter("id");	   
       Optional<String> date = DateUtil.getFormattedDate("mm/dd/yyyy hh:mm:ss aa");
       String uuid = BaseDomain.newUUID();
       HttpSession session = request.getSession();
       String userid = (String)session.getAttribute("userid");
       EmailInquiry emailInquiry = new EmailInquiry();
       EmailInquiryDao emailInquiryDao = new EmailInquiryDao();
       emailInquiry.setEmailaddress(request.getParameter("email"));
       emailInquiry.setPassword(request.getParameter("password"));
       try {
           if (pageaction.equalsIgnoreCase("Save")) {
               emailInquiry.setId(uuid);
               emailInquiry.setStatus(true);
               emailInquiry.setDateCreated(date.get());
               emailInquiry.setCreatedBy(userid);
               emailInquiry.setModifiedDate(date.get());
               emailInquiry.setModifiedBy(userid);
               emailInquiryDao.save(emailInquiry);
               request.setAttribute("insertmsg", "SUCCESSFULLY STORED!!!");
           }else if(pageaction.equalsIgnoreCase("Update")){              
               emailInquiry.setId(eiid);
               emailInquiry.setStatus(true);
               emailInquiry.setModifiedDate(date.get());
               emailInquiry.setModifiedBy(userid);
               emailInquiry.toString();
               emailInquiryDao.update(emailInquiry);
               request.setAttribute("insertmsg", "SUCCESSFULLY UPDATED!!!");
           }
          
        } catch (Exception e) {
            request.setAttribute("insertmsg", "FAILED");
        }
       
       EmailInquiry eiup = emailInquiryDao.OneEmailSend();
       try {
           eiup.getId();
           request.setAttribute("email", eiup.getEmailaddress());
           request.setAttribute("password", eiup.getPassword());
           request.setAttribute("id", eiup.getId());
           request.setAttribute("upemin", "true");
       } catch (Exception e) {
           request.setAttribute("upemin", "false");
       }
       session.setAttribute("tabactivesettings", "emailinquiry");
       request.setAttribute("navigation", "../cms/pages/settings/index.jsp");
       RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(CMS_INDEX);
       dispatcher.forward(request, response);
    }
}
