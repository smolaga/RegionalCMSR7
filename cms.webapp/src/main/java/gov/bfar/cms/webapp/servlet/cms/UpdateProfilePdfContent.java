package gov.bfar.cms.webapp.servlet.cms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;


import gov.bfar.cms.dao.ProfileDao;
import gov.bfar.cms.domain.Profile;

import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.util.DateUtil;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

/**
 * Servlet implementation class UpdateProfilePdfContent
 */
@WebServlet("/UpdateProfilePdfContent")
public class UpdateProfilePdfContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String UPLOAD_DIRECTORY ="";
	private String DOWNLOAD_DIRECTORY ="";

    /**
     * @throws FileNotFoundException 
     * @see HttpServlet#HttpServlet()
     */
    public UpdateProfilePdfContent() {
    	super();
    	UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("fisheriesprofilepdfup");
    	DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("fisheriesprofilepdfdl");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				savepdf(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		savepdf(request, response);
	}
	protected void savepdf(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Optional<String> pdfdate = DateUtil.getFormattedDate("MM-dd-yyyy");
		HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		String pdfuuid = BaseDomain.newUUID();
		String reqTitle = null;
		String filePath = null;
		String contentType = null;
		String sequence = null;
		String oldsequence = null;
		String uuid = null;
		boolean status = false;
		String pdffileupdatestatus = null;
		
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				String fname = null;
				String fsize = null;
				String ftype = null;
				List<FileItem> multiparts = new ServletFileUpload(
						new DiskFileItemFactory()).parseRequest(request);
				for (FileItem item : multiparts) {
					if (!item.isFormField()) {						
						fname = pdfuuid + "(" + pdfdate.get() + ")" + ".pdf";
						fsize = new Long(item.getSize()).toString();
						ftype = item.getContentType();
						item.write(new File(UPLOAD_DIRECTORY + fname));
						String rootfilepath = DOWNLOAD_DIRECTORY;
						filePath = rootfilepath.toString() + fname;
						String ext = FilenameUtils.getExtension(fname);						
						contentType = ext;
						
					}else{
						String fName = item.getFieldName();
						String value = item.getString();
						switch(fName)
						{
						case "id":
                            uuid = value;
                            break;
						case "title":
							reqTitle = value;
							break;
						case "status":
							status = Boolean.parseBoolean(value);
							break;
						case "sequence":
							sequence = value;
							break;
						case "oldsequence":
						    oldsequence = value;
                            break;
						case "pdffileupdatestatus":
							pdffileupdatestatus = value;
							break;
						}
						
					
					}
				}
			
				request.setAttribute("name", fname);
				request.setAttribute("size", fsize);
				request.setAttribute("type", ftype);
			} catch (Exception ex) {
				request.setAttribute("message", "File Upload Failed due to "
						+ ex);
			}
 
		} else {
			request.setAttribute("message",	"Sorry this Servlet only handles file upload request");
		}
				
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		Profile profile = new Profile();
		ProfileDao profileDao = new ProfileDao();
		profile.setId(uuid);
		if (pdffileupdatestatus.equalsIgnoreCase("true")) {
			profile.setPdfFilePath(filePath);
		}else{
			Profile pdffile = profileDao.findById(uuid);
			profile.setPdfFilePath(pdffile.getPdfFilePath());
		}
		profile.setTitle(reqTitle);
		profile.setSequence(Integer.parseInt(sequence));
		profile.setContentType(contentType);
		profile.setPageLink("/WebsiteNavigation?id="+profile.getId()+"&pageAction=profileContentFrame");
		profile.setStatus(status);
		profile.setModifiedDate(date.get());
		profile.setModifiedBy(userid);
		profileDao.update(profile);
		profileDao.spAutoOrdering(profile.getSequence(), profile.getId(),Integer.parseInt(oldsequence));
		session.setAttribute("tabactiveprofile", "main");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=profile");

	}


}
