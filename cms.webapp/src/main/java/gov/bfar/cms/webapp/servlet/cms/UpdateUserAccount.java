package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.UserDao;
import gov.bfar.cms.domain.User;

import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateLeftContent
 */
@WebServlet("/UpdateUserAccount")
public class UpdateUserAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateUserAccount() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		updateuserAccount(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		updateuserAccount(request, response);
	}

	protected void updateuserAccount(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		Optional<String> date = DateUtil.getFormattedDate("MM-dd-yyyy hh:mm:ss aa");
		UserDao userDao = new UserDao();
		User user = new User();
		user.setId(request.getParameter("id"));
		user.setFirstName(request.getParameter("firstName"));
		user.setMiddleName(request.getParameter("middleName"));
		user.setLastName(request.getParameter("lastName"));
		user.setDesignation(request.getParameter("designation"));
		user.setUserName(request.getParameter("userName"));
		user.setUserPassword(request.getParameter("userPassword"));
		user.setUserType(request.getParameter("userType"));
		user.setSecretQuestion(request.getParameter("secretQuestion"));
		user.setAnswer(request.getParameter("answer"));
		user.setModifiedDate(date.get());
		user.setModifiedBy(userid);
		if (user.getUserType().equalsIgnoreCase("Administrator")) {
        	user.setUserAccessContent("ALL");
        	user.setCanUpdate(true);
        	user.setCanShow(true);
		}else{
			user.setUserAccessContent(request.getParameter("useraccesscontent"));
			user.setCanUpdate(Boolean.parseBoolean(request.getParameter("canupdate")));
			user.setCanShow(Boolean.parseBoolean(request.getParameter("canshow")));
		}
		userDao.update(user);

		request.setAttribute("navigation", "../cms/pages/login/userList.jsp");
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
		dispatcher.forward(request, response);
	}
}



