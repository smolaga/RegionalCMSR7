package gov.bfar.cms.webapp.servlet.cms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import gov.bfar.cms.dao.DownloadDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Download;
import gov.bfar.cms.util.DateUtil;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

/**
 * Servlet implementation class UploadDownloadPdf
 */
@WebServlet("/UploadDownloadPdf")
public class UploadDownloadPdf extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String UPLOAD_DIRECTORY ="";
	private String DOWNLOAD_DIRECTORY ="";

    /**
     * @throws FileNotFoundException 
     * @see HttpServlet#HttpServlet()
     */
    public UploadDownloadPdf() {
    	super();
    	UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("downloadspdfup");
    	DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("downloadspdfdl");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        uploadpdf(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    uploadpdf(request, response);
	}
	protected void uploadpdf(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> pdfdate = DateUtil.getFormattedDate("MM-dd-yyyy");
        String pdfuuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        String reqTitle = null;
        String filePath = null;
        String contentType = null;
        String sequence = null;
        
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                String fname = null;
                String fsize = null;
                String ftype = null;
                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {                      
                        fname = pdfuuid + "(" + pdfdate.get() + ")" + ".pdf";
                        fsize = new Long(item.getSize()).toString();
                        ftype = item.getContentType();
                        item.write(new File(UPLOAD_DIRECTORY + fname));
                        String rootfilepath = DOWNLOAD_DIRECTORY;
                        filePath = rootfilepath.toString() + fname;
                        String ext = FilenameUtils.getExtension(fname);                     
                        contentType = ext;
                    }else{
                        String fName = item.getFieldName();
                        String value = item.getString();
                        switch(fName)
                        {
                        case "title":
                            reqTitle = value;
                            break;
                        case "sequence":
                            sequence = value;
                            break;
                        }               
                    }
                }
            
                request.setAttribute("name", fname);
                request.setAttribute("size", fsize);
                request.setAttribute("type", ftype);
            } catch (Exception ex) {
                request.setAttribute("message", "File Upload Failed due to "
                        + ex);
            }
 
        } else {
            request.setAttribute("message", "Sorry this Servlet only handles file upload request");
        }
            
      
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        Download download = new Download();
        DownloadDao downloadDao = new DownloadDao();
        download.setId(uuid);
        download.setPdfFilePath(filePath);
        download.setTitle(reqTitle);
        download.setContentType(contentType);
        download.setSequence(Integer.parseInt(sequence));
        download.setPageLink("/WebsiteNavigation?id="+download.getId()+"&pageAction=downloadContentFrame");
        download.setStatus(false);
        download.setDateCreated(date.get());
        download.setCreatedBy(userid);
        download.setModifiedDate(date.get());
        download.setModifiedBy(userid);
        downloadDao.save(download);
        downloadDao.spAutoOrdering(download.getSequence(), download.getId(),0);
        session.setAttribute("tabactivedownload", "main");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=download");
    }     
}
