package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.OtherlinksDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Otherlinks;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertOtherLinks
 */
@WebServlet("/InsertOtherLinks")
public class InsertOtherLinks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertOtherLinks() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        insertContent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    insertContent(request, response);
	}
	protected void insertContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        Otherlinks otherlinks = new Otherlinks();
        OtherlinksDao otherlinksDao = new OtherlinksDao();
        otherlinks.setId(uuid);
        otherlinks.setTitle(request.getParameter("title"));
        otherlinks.setContent(request.getParameter("content"));
        otherlinks.setPageLink("/WebsiteNavigation");
        otherlinks.setStatus(false);
        otherlinks.setDateCreated(date.get());
        otherlinks.setCreatedBy(userid);
        otherlinks.setModifiedDate(date.get());
        otherlinks.setModifiedBy(userid);
        otherlinksDao.save(otherlinks);
        request.setAttribute("navigation", "../cms/pages/otherLinks/index.jsp");
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
        dispatcher.forward(request, response);
                
    }

}
