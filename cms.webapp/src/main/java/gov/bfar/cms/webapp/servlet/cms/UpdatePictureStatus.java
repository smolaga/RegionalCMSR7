package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.PicturesDao;
import gov.bfar.cms.domain.Pictures;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdatePictureStatus
 */
@WebServlet("/UpdatePictureStatus")
public class UpdatePictureStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdatePictureStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		updatePictureStatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updatePictureStatus(request, response);
	}
	
	protected void updatePictureStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionredirect = request.getParameter("status");
		String coltype = request.getParameter("type");
		String id = request.getParameter("contentid");
		request.setAttribute("getidslider", id);
		HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		PicturesDao picturesDao = new PicturesDao();
		Pictures pictures = picturesDao.findById(id);	
		Pictures pictures2 = new Pictures();
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		pictures2.setId(pictures.getId());
		pictures2.setTitle(pictures.getTitle());
		pictures2.setTitleId(pictures.getTitleId());
		pictures2.setImage(pictures.getImage());
		pictures2.setImageFilePath(pictures.getImageFilePath());
		pictures2.setDateEvent(pictures.getDateEvent());
		pictures2.setType(pictures.getType());		
		pictures2.setContentType(pictures.getContentType());
		pictures2.setPageLink(pictures.getPageLink());
		pictures2.setSequence(pictures.getSequence());
		pictures2.setModifiedBy(userid);
		pictures2.setModifiedDate(date.get());
		
		if (coltype.equalsIgnoreCase("slider")) {
			if (actionredirect.equalsIgnoreCase("show")) {
				pictures2.setStatus(true);
				picturesDao.update(pictures2);
				request.setAttribute("navigation", "../cms/pages/slider/index.jsp");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
			else if (actionredirect.equalsIgnoreCase("hide")) {
				pictures2.setStatus(false);
				picturesDao.update(pictures2);
				request.setAttribute("navigation", "../cms/pages/slider/index.jsp");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
			else if (actionredirect.equalsIgnoreCase("updateall")) {
				request.setAttribute("imgid", pictures.getId());
				request.setAttribute("navigation", "../cms/pages/slider/index.jsp");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
			else if (actionredirect.equalsIgnoreCase("delete")) {				
				picturesDao.delete(id);
				request.setAttribute("navigation", "../cms/pages/slider/index.jsp");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
		}
		
		
			else if (coltype.equalsIgnoreCase("banner")) {
			if (actionredirect.equalsIgnoreCase("show")) {
				pictures2.setStatus(true);
				picturesDao.update(pictures2);
				picturesDao.setOneActiveStatus("banner", "banner" , pictures2.getId());
				request.setAttribute("navigation", "../cms/pages/banner/index.jsp");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
			else if (actionredirect.equalsIgnoreCase("hide")) {
				pictures2.setStatus(false);
				picturesDao.update(pictures2);
				request.setAttribute("navigation", "../cms/pages/banner/index.jsp");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
			else if (actionredirect.equalsIgnoreCase("updateall")) {
				request.setAttribute("imgid", pictures.getId());
				request.setAttribute("navigation", "../cms/pages/banner/index.jsp");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
			else if (actionredirect.equalsIgnoreCase("delete")) {				
				picturesDao.delete(id);
				request.setAttribute("navigation", "../cms/pages/banner/index.jsp");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}
		}
			else if (coltype.equalsIgnoreCase("gallery")) {
				if (actionredirect.equalsIgnoreCase("show")) {
					pictures2.setStatus(true);
					picturesDao.update(pictures2);
					if (pictures.getContentType().equalsIgnoreCase("albumtitle")) {
						request.setAttribute("navigation", "../cms/pages/gallery/galleryTitleList.jsp");
					}else if(pictures.getContentType().equalsIgnoreCase("albumimage")){
						request.setAttribute("navigation", "../cms/pages/gallery/galleryPictureList.jsp");
						request.setAttribute("tid", pictures.getTitleId());
					}
				}
				else if (actionredirect.equalsIgnoreCase("hide")) {
					pictures2.setStatus(false);
					picturesDao.update(pictures2);
					if (pictures.getContentType().equalsIgnoreCase("albumtitle")) {
						request.setAttribute("navigation", "../cms/pages/gallery/galleryTitleList.jsp");
					}else if(pictures.getContentType().equalsIgnoreCase("albumimage")){
						request.setAttribute("navigation", "../cms/pages/gallery/galleryPictureList.jsp");
						request.setAttribute("tid", pictures.getTitleId());
					}
				}
				else if (actionredirect.equalsIgnoreCase("delete")) {					
					picturesDao.delete(id);
					if (pictures.getContentType().equalsIgnoreCase("albumtitle")) {
						request.setAttribute("navigation", "../cms/pages/gallery/galleryTitleList.jsp");
					}else if(pictures.getContentType().equalsIgnoreCase("albumimage")){
						request.setAttribute("navigation", "../cms/pages/gallery/galleryPictureList.jsp");
						request.setAttribute("tid", pictures.getTitleId());
					}
				}
				else if (actionredirect.equalsIgnoreCase("updateall")) {
					
					String contentid = request.getParameter("contentid");
					String titleid = request.getParameter("titleid");
					if (pictures.getContentType().equalsIgnoreCase("albumtitle")) {
						request.setAttribute("tid", contentid);
					}else if(pictures.getContentType().equalsIgnoreCase("albumimage")){
					    request.setAttribute("updalbumimg", "true");
						request.setAttribute("tid", titleid);
						request.setAttribute("imgid", contentid);
					}
					
					request.setAttribute("navigation", "../cms/pages/gallery/galleryPictureList.jsp");
					
				}
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
				dispatcher.forward(request, response);
			}	
	}
}
