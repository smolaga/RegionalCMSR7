package gov.bfar.cms.webapp.servlet.cms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import gov.bfar.cms.dao.HomepageDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Homepage;
import gov.bfar.cms.util.DateUtil;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

/**
 * Servlet implementation class HomepageLeftSubContent
 */
@WebServlet("/HomepageLeftSubContent")
public class HomepageLeftSubContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String UPLOAD_DIRECTORY ="";
	private String DOWNLOAD_DIRECTORY ="";

    /**
     * @throws FileNotFoundException 
     * @see HttpServlet#HttpServlet()
     */
    public HomepageLeftSubContent() {
    	super();
    	UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("homepageleftpdfup");
    	DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("homepageleftpdfdl");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    subContent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    subContent(request, response);
	}
	
	protected void subContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    HttpSession session = request.getSession();
        String pageaction = request.getParameter("action");
        String userid = (String)session.getAttribute("userid");
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        
        
        String htmlsubcontentid = request.getParameter("htmlsubcontentid");
        String htmlsubcontenttitle = request.getParameter("htmlsubcontenttitle");
        String htmlsubcontent = request.getParameter("htmlsubcontent");
        String htmlsubpagelink = request.getParameter("htmlsubpagelink");
        String htmlsubcolumntype = request.getParameter("htmlsubcolumntype");
        String htmlsubcontenttype = request.getParameter("htmlsubcontenttype");
        Boolean htmlsubstatus = Boolean.parseBoolean(request.getParameter("htmlsubstatus"));
        
        if (pageaction.equalsIgnoreCase("insertsubhtml")) {
            Homepage insub = new Homepage();
            insub.setId(uuid);
            insub.setHead(htmlsubcontenttitle);
            insub.setContent(htmlsubcontent);
            insub.setSubContentLink("/WebsiteNavigation?id="+insub.getId()+"&pageAction=subContentFrame");
            insub.setColumnType(htmlsubcolumntype);
            insub.setContentType(htmlsubcontenttype);
            insub.activate();
            insub.setDateCreated(date.get());
            insub.setCreatedBy(userid);
            insub.setCreatedBy(userid);
            insub.setModifiedDate(date.get());
            insertSubHTML(insub);
           
        }
        else if (pageaction.equalsIgnoreCase("updatesubhtml")) {
            Homepage insub = new Homepage();
            insub.setId(htmlsubcontentid);
            insub.setHead(htmlsubcontenttitle);
            insub.setContent(htmlsubcontent);
            insub.setSubContentLink(htmlsubpagelink);
            insub.setColumnType(htmlsubcolumntype);
            insub.setContentType(htmlsubcontenttype);
            insub.setStatus(htmlsubstatus);
            insub.setModifiedDate(date.get());
            insub.setModifiedBy(userid);
            updateSubHTML(insub);
            
        }
        else if(pageaction.equalsIgnoreCase("insertsubpdf")){        	
        	Optional<String> pdfdate = DateUtil.getFormattedDate("MM-dd-yyyy");
            String reqTitle = null;
            String filePath = null;
            String coltype = null;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload(
                            new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {                            
                            fname = uuid + "(" + pdfdate.get() + ")" + ".pdf";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;
                        }else{
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch(fName)
                            {
                            case "pdfsubcontenttitle":
                                reqTitle = value;
                                break;
                            case "columnType":
                                coltype = value;
                                break;
                            }
                           
                        }
                    }                    
                    request.setAttribute("message", "File Uploaded Successfully");
                    request.setAttribute("name", fname);
                    request.setAttribute("size", fsize);
                    request.setAttribute("type", ftype);
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to "
                            + ex);
                }
     
            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
            Homepage in = new Homepage();
            in.setId(uuid);
            in.setHead(reqTitle);
            in.setPdfFilePath(filePath);
            in.setColumnType(coltype);
            in.setContentType("subpdf");
            in.setSubContentLink("/WebsiteNavigation?id="+in.getId()+"&pageAction=subContentFrame");
            in.activate();
            in.setCreatedBy(userid);
            in.setDateCreated(date.get());
            in.setModifiedDate(date.get());
            in.setModifiedBy(userid);
            insertSubPDF(in);           
        }else if (pageaction.equalsIgnoreCase("updatesubpdf")) {           
        	Optional<String> pdfdate = DateUtil.getFormattedDate("MM-dd-yyyy");
        	String pdfsubcontenttitle = null;
            String pdfsubcontentid = null;
            String pdfsubcontentlink = null;
            String pdfsubcontenttype = null;
            String pdfsubcolumntype = null;
            String pdffilepath = null;
            String pdffileupdatestatus = null;
            Boolean pdfsubstatus = false;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload(
                            new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {                           
                            fname = uuid + "(" + pdfdate.get() + ")" + ".pdf";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            pdffilepath = rootfilepath.toString() + fname;
                        }else{
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch(fName)
                            {
                            case "pdfsubcontenttitle":
                            	pdfsubcontenttitle = value;
                                break;
                            case "pdfsubcontentid":
                            	pdfsubcontentid = value;
                                break;
                            case "pdfsubcontentlink":
                            	pdfsubcontentlink = value;
                                break;
                            case "pdfsubcontenttype":
                            	pdfsubcontenttype = value;
                                break;
                            case "pdffileupdatestatus":
                            	pdffileupdatestatus = value;
                                break;
                            case "pdfsubcolumntype":
                            	pdfsubcolumntype = value;
                                break;
	                        case "pdfsubstatus":
	                        	pdfsubstatus = Boolean.parseBoolean(value);
	                            break;
	                        }
                            
                        }
                    }
                   
                    request.setAttribute("name", fname);
                    request.setAttribute("size", fsize);
                    request.setAttribute("type", ftype);
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to "
                            + ex);
                }
     
            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
            HomepageDao dao = new HomepageDao();
            Homepage pdffilestatus = dao.findById(pdfsubcontentid);
            
            Homepage up = new Homepage();
            up.setId(pdfsubcontentid);
            up.setHead(pdfsubcontenttitle);
            up.setSubContentLink(pdfsubcontentlink);
            up.setColumnType(pdfsubcolumntype);
            up.setContentType(pdfsubcontenttype);
            if (pdffileupdatestatus.equalsIgnoreCase("true")) {
                up.setPdfFilePath(pdffilepath);
            }else {
                up.setPdfFilePath(pdffilestatus.getPdfFilePath());
            }
            up.setStatus(pdfsubstatus);
            up.setModifiedBy(userid);
            up.setModifiedDate(date.get());
            updateSubPDF(up);
           
         }
        session.setAttribute("tabactiveleft", "sub");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=homepageleftcontent");
      
    }
	
	void insertSubHTML(Homepage insubhtml){
	    HomepageDao dao = new HomepageDao();
	    dao.save(insubhtml);
	}
	
	void updateSubHTML(Homepage upsubhtml){
        HomepageDao dao = new HomepageDao();
        dao.update(upsubhtml);
    }
	
	void insertSubPDF(Homepage insubpdfl){
        HomepageDao dao = new HomepageDao();
        dao.save(insubpdfl);
    }
	
	void updateSubPDF(Homepage upsubpdf){
        HomepageDao dao = new HomepageDao();
        dao.update(upsubpdf);
    }
	

}
