package gov.bfar.cms.webapp.servlet.cms;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import gov.bfar.cms.dao.PicturesDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Pictures;
import gov.bfar.cms.util.DateUtil;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

/**
 * Servlet implementation class InsertImage
 */
@WebServlet("/InsertPictures")
public class InsertPictures extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String UPLOAD_DIRECTORY ="";
	private String DOWNLOAD_DIRECTORY ="";
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertPictures() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		insertPictures(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		insertPictures(request, response);
	}

	protected void insertPictures(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
       
	
		HttpSession session = request.getSession();
		String userid = (String)session.getAttribute("userid");
		Pictures pictures = new Pictures();
		PicturesDao picturesDao = new PicturesDao();
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		String uuid = BaseDomain.newUUID();
		String type = request.getParameter("type");
		if (type.equalsIgnoreCase("slider")) {
			
			UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("sliderimageup");
	        DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("sliderimagedl");
			Optional<String> imgdate = DateUtil.getFormattedDate("MM-dd-yyyy");
			int sequence = 0; 
			String action= null;
			String imgchange = null;
			String imgid = null;
            String filePath = null;
            String sliderPageLink = null;
            String oldsequence = null;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload( new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {
                            fname = uuid + "(" + imgdate.get() + ")" + ".jpeg";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;
                        }else{
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch(fName)
                            {
                            case "sequence":
                            	sequence = Integer.parseInt(value);
                            	break;
                            case "action":
                            	action = value;
                            	break;
                            case "imgchange":
                            	imgchange = value;
                            	break;
                            case "imgid":
                            	imgid = value;
                            	break;
                            case "sliderPageLink":
                            	sliderPageLink = value;
                            	break;
                            case "oldsequence":
                                oldsequence = value;
                                break;
                            }
                        }
                    }
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to " + ex);
                }
     
            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
			
            Pictures picup = new Pictures();
			picup = picturesDao.findById(imgid);
			
            if (imgchange.equalsIgnoreCase("true")){
				pictures.setImageFilePath(filePath);
				
			}else {
				pictures.setImageFilePath(picup.getImageFilePath());
			}
			pictures.setType(request.getParameter("type"));
			pictures.setContentType(request.getParameter("type"));
			pictures.setSequence(sequence);
			if (action.equalsIgnoreCase("insertimg")) {
				pictures.setId(uuid);
				pictures.deactive();
				pictures.setPageLink(sliderPageLink);
				pictures.setDateCreated(date.get());
				pictures.setCreatedBy(userid);
				pictures.setModifiedDate(date.get());
				pictures.setModifiedBy(userid);
				picturesDao.save(pictures);
				picturesDao.spAutoOrdering(pictures.getSequence(), pictures.getId(), pictures.getType(), pictures.getContentType(), 0);
			}else {
				pictures.setPageLink(sliderPageLink);
				pictures.setId(picup.getId());
				pictures.setStatus(picup.isStatus());
				pictures.setModifiedBy(userid);
				pictures.setModifiedDate(date.get());
				picturesDao.update(pictures);
				picturesDao.spAutoOrdering(pictures.getSequence(), pictures.getId(), pictures.getType(), pictures.getContentType(), Integer.parseInt(oldsequence));
			}
			/*request.setAttribute("navigation", "../cms/pages/slider/index.jsp");*/
			response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=slider");
		}
		else if (type.equalsIgnoreCase("banner")) {
			UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("bannerimageup");
	        DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("bannerimagedl");
			Optional<String> imgdate = DateUtil.getFormattedDate("MM-dd-yyyy");
			String action= null;
			String imgchange = null;
			String imgid = null;
            String filePath = null;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload( new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                
                        if (!item.isFormField()) {
                            fname = uuid + "(" + imgdate.get() + ")" + ".jpeg";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + File.separator + fname));                           
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;
                        }else{
                            String fName = item.getFieldName();
                            String value = item.getString();
                            
                            switch(fName)
                            {
                            case "action":
                            	action = value;
                            	break;
                            case "imgchange":
                            	imgchange = value;
                            	break;
                            case "imgid":
                            	imgid = value;
                            	break;
                            }
                        }
                    }
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to " + ex);
                }
     
            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
            Pictures picup = new Pictures();
			picup = picturesDao.findById(imgid);
            if (imgchange.equalsIgnoreCase("true")){
				pictures.setImageFilePath(filePath);
			}else {
				pictures.setImageFilePath(picup.getImageFilePath());
			}
			pictures.setType(request.getParameter("type"));
			pictures.setContentType(request.getParameter("type"));
			pictures.setSequence(0);
			if (action.equalsIgnoreCase("insertimg")) {
				pictures.setId(uuid);
				pictures.deactive();
				pictures.setDateCreated(date.get());
				pictures.setCreatedBy(userid);
				pictures.setModifiedDate(date.get());
				pictures.setModifiedBy(userid);
				picturesDao.save(pictures);
			}else {
				pictures.setId(picup.getId());
				pictures.setStatus(picup.isStatus());
				pictures.setModifiedBy(userid);
				pictures.setModifiedDate(date.get());
				picturesDao.update(pictures);
			}			
			response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=banner");
		}
		
		else if (type.equalsIgnoreCase("cmsbanner")) {
			try {
			UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("cmsbannerimageup");
	        DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("cmsbannerimagedl");
			Optional<String> imgdate = DateUtil.getFormattedDate("MM-dd-yyyy");
			String action= null;
			String imgchange = null;
			String bid = null;
            String filePath = null;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload( new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {
                            fname = uuid + "(" + imgdate.get() + ")" + ".jpeg";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + File.separator + fname));                           
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;
                        }else{
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch(fName)
                            {
                            case "action":
                            	action = value;
                            	break;
                            case "imgchange":
                            	imgchange = value;
                            	break;
                            case "bid":
                            	bid = value;
                            	break;
                            }
                        }
                    }
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to " + ex);
                }
     
            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
            Pictures picup = new Pictures();
			picup = picturesDao.findById(bid);
            if (imgchange.equalsIgnoreCase("true")){
				pictures.setImageFilePath(filePath);
			}else {
				pictures.setImageFilePath(picup.getImageFilePath());
			}
            pictures.setType(request.getParameter("type"));
			pictures.setContentType(request.getParameter("type"));
			if (action.equalsIgnoreCase("Save")) {
				pictures.setId(uuid);
				pictures.setStatus(true);
				pictures.setDateCreated(date.get());
				pictures.setCreatedBy(userid);
				pictures.setModifiedDate(date.get());
				pictures.setModifiedBy(userid);
				picturesDao.save(pictures);
				request.setAttribute("insertbannermsg", "SUCCESSFULLY STORED!!!");
			 }else if(action.equalsIgnoreCase("Update")){
				pictures.setId(bid);
				pictures.setStatus(true);				
				pictures.setModifiedBy(userid);
				pictures.setModifiedDate(date.get());
				picturesDao.update(pictures);
				 session.setAttribute("insertbannermsg", "SUCCESSFULLY UPDATED!!!");
			}
		} catch (Exception e) {
			session.setAttribute("insertbannermsg", "FAILED");
        }
			session.setAttribute("tabactivesettings", "cmsbanner");
			response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=settings");
		}
	}
}
