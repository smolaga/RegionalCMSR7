package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ProfileDao;
import gov.bfar.cms.domain.Profile;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateSubProfileContent
 */
@WebServlet("/UpdateSubProfileContent")
public class UpdateSubProfileContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateSubProfileContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatesub(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatesub(request, response);
	}
	protected void updatesub(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Profile profile = new Profile();
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        ProfileDao profileDao = new ProfileDao();
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        profile.setId(request.getParameter("id"));
        profile.setTitle(request.getParameter("title"));
        profile.setContent(request.getParameter("content"));
        profile.setContentType("subhtml");
        profile.setStatus(Boolean.parseBoolean(request.getParameter("status")));
        profile.setModifiedBy(userid);
        profile.setSubContentLink("/WebsiteNavigation?id="+profile.getId()+"&pageAction=profileContentFrame");      
        profile.setModifiedDate(date.get());
        profileDao.update(profile);
        
        session.setAttribute("tabactiveprofile", "sub");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=profile");
    
    }

}
