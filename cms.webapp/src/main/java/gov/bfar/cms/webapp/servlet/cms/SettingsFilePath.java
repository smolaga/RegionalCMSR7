package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.FilePathDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.FilePath;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class SettingsFilePath
 */
@WebServlet("/SettingsFilePath")
public class SettingsFilePath extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SettingsFilePath() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		filepath(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		filepath(request, response);
	}
	
	protected void filepath(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 HttpSession session = request.getSession();
	    String userid = (String)session.getAttribute("userid");
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        
		String filetype = request.getParameter("filetype");
		String filepath = request.getParameter("filepath");
		String contenttype = request.getParameter("contenttype");
		String region = "010000000";
		
		FilePath fp = new FilePath();
		FilePathDao fpdao = new FilePathDao();
		
		if (filetype.equalsIgnoreCase("pdf")) {
			fp.setId(uuid);
			fp.setContentType(contenttype);
			fp.setFileType(filetype);
			fp.setFilePath(filepath);
			fp.setRegion(region);	
			fp.activate();
			fp.setCreatedBy(userid);
			fp.setDateCreated(date.get());
			fp.toString();
			fpdao.save(fp);
		}else if(filetype.equalsIgnoreCase("image")){
			fp.setId(uuid);
			fp.setContentType(contenttype);
			fp.setFileType(filetype);
			fp.setFilePath(filepath);
			fp.setRegion(region);
			fp.activate();
			fp.setCreatedBy(userid);
			fp.setDateCreated(date.get());
			fp.toString();
			fpdao.save(fp);
		}else if(filetype.equalsIgnoreCase("video")){
			fp.setId(uuid);
			fp.setContentType(contenttype);
			fp.setFileType(filetype);
			fp.setFilePath(filepath);
			fp.setRegion(region);
			fp.activate();
			fp.setCreatedBy(userid);
			fp.setDateCreated(date.get());
			fp.toString();
			fpdao.save(fp);
		}
		request.setAttribute("navigation", "../cms/pages/settings/index.jsp");
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
        dispatcher.forward(request, response);
	}

}
