package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.ProfileDao;
import gov.bfar.cms.domain.Profile;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class ProfileUpdateStatus
 */
@WebServlet("/ProfileUpdateStatus")
public class ProfileUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfileUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				updatestatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updatestatus(request, response);
	}
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionredirect = request.getParameter("status");
		String id = request.getParameter("contentid");
		HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		request.setAttribute("getidcontent", id);
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		ProfileDao profileDao = new ProfileDao();
		Profile profile = profileDao.findById(id);
		Profile profile2 = new Profile();
		profile2.setId(profile.getId());
		profile2.setTitle(profile.getTitle());
		profile2.setContent(profile.getContent());
		profile2.setContentType(profile.getContentType());
		profile2.setPdfFilePath(profile.getPdfFilePath());
		profile2.setSequence(profile.getSequence());
		profile2.setPageLink(profile.getPageLink());
		profile2.setModifiedBy(userid);
		profile2.setModifiedDate(date.get());
		
		if (actionredirect.equalsIgnoreCase("show")) {
			profile2.setStatus(true);
			profileDao.update(profile2);
			request.setAttribute("navigation", "../cms/pages/profile/index.jsp");
		}
		else if (actionredirect.equalsIgnoreCase("hide")) {
			profile2.setStatus(false);
			profileDao.update(profile2);
			request.setAttribute("navigation", "../cms/pages/profile/index.jsp");
		}
		else if (actionredirect.equalsIgnoreCase("updateall")) {
			String ctype = request.getParameter("contenttype");
			if (ctype.equalsIgnoreCase("pdf")) {
				request.setAttribute("navigation", "../cms/pages/profile/updatepdfContent.jsp");
			}
			else if (ctype.equalsIgnoreCase("html")) {
				request.setAttribute("navigation", "../cms/pages/profile/updateContent.jsp");
			}
			else if (ctype.equalsIgnoreCase("subhtml")) {
                request.setAttribute("navigation", "../cms/pages/profile/updateSubHTMLContent.jsp");
            }
			else if (ctype.equalsIgnoreCase("subpdf")) {
                request.setAttribute("navigation", "../cms/pages/profile/updateSubPDFContent.jsp");
            }
		}
		else if (actionredirect.equalsIgnoreCase("delete")) {			
			profileDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/profile/index.jsp");
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
		dispatcher.forward(request, response);
	}
}
	



