package gov.bfar.cms.webapp.servlet.cms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import gov.bfar.cms.dao.HomepageDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Homepage;
import gov.bfar.cms.util.DateUtil;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

/**
 * Servlet implementation class HomepageLeftContent
 */
@WebServlet("/HomepageLeftContent")
public class HomepageLeftContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	private String UPLOAD_DIRECTORY ="";
	private String DOWNLOAD_DIRECTORY ="";


    /**
     * @throws FileNotFoundException 
     * @see HttpServlet#HttpServlet()
     */
    public HomepageLeftContent() throws FileNotFoundException {
    	super();
    	UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("homepageleftpdfup");
    	DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("homepageleftpdfdl");
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    leftContent(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    leftContent(request, response);
	}
	
	protected void leftContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
	    String pageaction = request.getParameter("action");
	    String userid = (String)session.getAttribute("userid");
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        
       
        String titleid = request.getParameter("titleid");
        String title = request.getParameter("title");
        boolean titlestatus = Boolean.parseBoolean(request.getParameter("titlestatus"));
        int sequence = 0;
        try {
        	sequence = Integer.parseInt(request.getParameter("sequence"));
		} catch (Exception e) {
			sequence = 0;
		}
       
       
        String htmlcontentid = request.getParameter("htmlcontentid");
        String htmlcontenttitle = request.getParameter("htmlcontenttitle");
        String htmlshortcontent = request.getParameter("htmlshortcontent");
        String htmlcontent = request.getParameter("htmlcontent");
        String htmlpagelink = request.getParameter("htmlpagelink");
        String htmlcolumntype = request.getParameter("htmlcolumntype");
        String htmlcontenttype = request.getParameter("htmlcontenttype");
        Boolean htmlstatus = Boolean.parseBoolean(request.getParameter("htmlstatus"));
        Boolean htmlviewonpage = Boolean.parseBoolean(request.getParameter("htmlviewonpage"));
        Boolean htmlviewonlist = Boolean.parseBoolean(request.getParameter("htmlviewonlist"));
        
        String pdftitlecontent = request.getParameter("pdftitilecontent");
        String pdfcontent = request.getParameter("pdfcontent");
       
        HomepageDao homepageDao = new HomepageDao();
        String type = request.getParameter("columnType");
        if (pageaction.equalsIgnoreCase("addtitle")) {
            Homepage in = new Homepage();
            in.setId(uuid);
            in.setHead(title);
            in.setSequence(sequence);
            in.setColumnType(type);
            in.setContentType("maintitle");
            in.deactive();
            in.setCreatedBy(userid);
            in.setDateCreated(date.get());
            in.setModifiedDate(date.get());
            in.setModifiedBy(userid);
            insertTitle(in);
            titleid = in.getId();
            request.setAttribute("tid", in.getId());
        }else if(pageaction.equalsIgnoreCase("updatetitle")) {
            Homepage up = new Homepage();
            up.setId(titleid);
            up.setHead(title);
            up.setSequence(sequence);
            up.setColumnType(type);
            up.setContentType("maintitle");
            up.setStatus(titlestatus);
            up.setModifiedBy(userid);
            up.setModifiedDate(date.get());
            updateTitle(up,Integer.parseInt(request.getParameter("oldsequence")));
            request.setAttribute("tid", up.getId());
            request.setAttribute("tseq", up.getSequence());
            request.setAttribute("thead", up.getHead());
            request.setAttribute("columntype", up.getColumnType());
        }else if(pageaction.equalsIgnoreCase("inserthtml")){
            Homepage in = new Homepage();
            in.setId(uuid);
            in.setHead(htmlcontenttitle);
            in.setContent(htmlcontent);
            in.setShortContent(htmlshortcontent);
            in.setPageLink("/WebsiteNavigation?id="+in.getId()+"&pageAction=contentFrame");
            in.setColumnType(type);
            in.setContentType("html");
            in.setHeadid(titleid);
            in.vopdeactive();
            in.voldeactive();
            in.setCreatedBy(userid);
            in.setDateCreated(date.get());
            in.setModifiedDate(date.get());
            in.setModifiedBy(userid);
            insertHTML(in);
            request.setAttribute("tid", titleid);
            request.setAttribute("inserttabactive", "html");
        }
        else if(pageaction.equalsIgnoreCase("insertpdf")){
            Optional<String> pdfdate = DateUtil.getFormattedDate("MM-dd-yyyy");
            String reqTitle = null;
            String filePath = null;
            String mtid = null;
            String pdfshortcontent = null;
            String coltype = null;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload(
                            new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {
                            //fname = new File(item.getName()).getName();
                            fname = uuid + "(" + pdfdate.get() + ")" + ".pdf";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            filePath = rootfilepath.toString() + fname;
                        }else{
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch(fName)
                            {
                            case "pdfcontenttitle":
                                reqTitle = value;
                                break;
                            case "pdfshortcontent":
                            	pdfshortcontent = value;
                            	break;
                            case "titleid":
                                mtid = value;
                                titleid = value;
                                break;
                            case "columnType":
                                coltype = value;
                                break;
                            }
                           
                           
                        }
                    }
                                     
                    request.setAttribute("name", fname);
                    request.setAttribute("size", fsize);
                    request.setAttribute("type", ftype);
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to "
                            + ex);
                }
     
            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
            Homepage in = new Homepage();
            in.setId(uuid);
            in.setHead(reqTitle);
            in.setPdfFilePath(filePath);
            in.setColumnType(coltype);
            in.setContentType("pdf");
            in.setShortContent(pdfshortcontent);
            in.setPageLink("/WebsiteNavigation?id="+in.getId()+"&pageAction=contentFrame");
            in.setHeadid(mtid);
            in.vopdeactive();
            in.voldeactive();
            in.setCreatedBy(userid);
            in.setDateCreated(date.get());
            in.setModifiedDate(date.get());
            in.setModifiedBy(userid);
            insertPDF(in);
            request.setAttribute("tid", mtid);
            request.setAttribute("inserttabactive", "pdf");
        }
        else if(pageaction.equalsIgnoreCase("updatehtml")){
            Homepage up = new Homepage();
            up.setId(htmlcontentid);
            up.setHead(htmlcontenttitle);
            up.setContent(htmlcontent);
            up.setShortContent(htmlshortcontent);
            up.setPageLink(htmlpagelink);
            up.setColumnType(htmlcolumntype);
            up.setContentType(htmlcontenttype);
            up.setHeadid(titleid);
            up.setStatus(htmlstatus);
            up.setViewonpage(htmlviewonpage);
            up.setViewonlist(htmlviewonlist);
            up.setModifiedBy(userid);
            up.setModifiedDate(date.get());
            updateHTML(up);
            request.setAttribute("tid", titleid);
            request.setAttribute("tseq", sequence);
            request.setAttribute("thead", title);
            request.setAttribute("columntype", type);
            request.setAttribute("inserttabactive", "html");
        }
        else if(pageaction.equalsIgnoreCase("updatepdf")){
            Optional<String> pdfdate = DateUtil.getFormattedDate("MM-dd-yyyy");
            
            String pdfcontenttitle = null;
            String pdfcontentid = null;
            String pdfpagelink = null;
            String pdfshortcontent = null;
            String pdfcontenttype = null;
            String columnType = null;
            String pdffilepath = null;
            String pdffileupdatestatus = null;
            Boolean pdfstatus = false;
            Boolean pdfviewonpage = false;
            Boolean pdfviewonlist = false;
            String mtid = null;
            String mtitle = null;
            int msequence = 0;
            if (ServletFileUpload.isMultipartContent(request)) {
                try {
                    String fname = null;
                    String fsize = null;
                    String ftype = null;
                    List<FileItem> multiparts = new ServletFileUpload(
                            new DiskFileItemFactory()).parseRequest(request);
                    for (FileItem item : multiparts) {
                        if (!item.isFormField()) {
                            //fname = new File(item.getName()).getName();
                            fname = uuid + "(" + pdfdate.get() + ")" + ".pdf";
                            fsize = new Long(item.getSize()).toString();
                            ftype = item.getContentType();
                            item.write(new File(UPLOAD_DIRECTORY + fname));
                            String rootfilepath = DOWNLOAD_DIRECTORY;
                            pdffilepath = rootfilepath.toString() + fname;
                        }else{
                            String fName = item.getFieldName();
                            String value = item.getString();
                            switch(fName)
                            {
                            case "pdfcontenttitle":
                                pdfcontenttitle = value;
                                break;
                            case "pdfcontentid":
                                pdfcontentid = value;
                                break;
                            case "pdfshortcontent":
                            	pdfshortcontent = value;
                                break;
                            case "pdfpagelink":
                                pdfpagelink = value;
                                break;
                            case "pdfcontenttype":
                                pdfcontenttype = value;
                                break;
                            case "pdfstatus":
                            	pdfstatus = Boolean.parseBoolean(value);
                                break;
                            case "pdfviewonpage":
                            	pdfviewonpage = Boolean.parseBoolean(value);
                                break;
                            case "pdfviewonlist":
                            	pdfviewonlist = Boolean.parseBoolean(value);
                                break;
                            case "pdffileupdatestatus":
                                pdffileupdatestatus = value;
                                break;
                            case "titleid":
                                mtid = value;
                                titleid = value;
                                break;
                            case "title":
                                mtitle = value;
                                break;
                            case "sequence":
                                msequence = Integer.parseInt(value);
                                break;
                            case "columnType":
                                columnType = value;
                                break;
                            }
                           
                        }
                    }
                   
                    request.setAttribute("name", fname);
                    request.setAttribute("size", fsize);
                    request.setAttribute("type", ftype);
                } catch (Exception ex) {
                    request.setAttribute("message", "File Upload Failed due to "
                            + ex);
                }
     
            } else {
                request.setAttribute("message", "Sorry this Servlet only handles file upload request");
            }
            HomepageDao dao = new HomepageDao();
            Homepage pdffilestatus = dao.findById(pdfcontentid);
            
            Homepage up = new Homepage();
            up.setId(pdfcontentid);
            up.setHead(pdfcontenttitle);
            up.setPageLink(pdfpagelink);
            up.setShortContent(pdfshortcontent);
            up.setColumnType(columnType);
            up.setContentType(pdfcontenttype);
            if (pdffileupdatestatus.equalsIgnoreCase("true")) {
                up.setPdfFilePath(pdffilepath);
            }else {
                up.setPdfFilePath(pdffilestatus.getPdfFilePath());
            }
            up.setHeadid(mtid);
            up.setStatus(pdfstatus);
            up.setViewonpage(pdfviewonpage);
            up.setViewonlist(pdfviewonlist);
            up.setModifiedBy(userid);
            up.setModifiedDate(date.get());
            updatePDF(up);
            request.setAttribute("tid", mtid);
            request.setAttribute("tseq", msequence);
            request.setAttribute("thead", mtitle);
            request.setAttribute("columntype", columnType);
            request.setAttribute("inserttabactive", "pdf");
        }
        session.setAttribute("tabactiveleft", "main");
        response.sendRedirect("/BFAR-R7/HomepageContentUpdate?id="+titleid+"&status=updateall&columnType=left&contentType=maintitle");
      
    }
	
	public void insertTitle(Homepage inTitle){
	    HomepageDao in = new HomepageDao();	  
	    in.save(inTitle);
	    in.spAutoOrdering(inTitle.getSequence(), inTitle.getId(), inTitle.getColumnType(), inTitle.getContentType(),0);
	}
	
	public void updateTitle(Homepage upTitle,int oldseq){
	    System.out.println(oldseq);
	    HomepageDao up = new HomepageDao();	  
	    up.update(upTitle);
	    up.spAutoOrdering(upTitle.getSequence(), upTitle.getId(), upTitle.getColumnType(), upTitle.getContentType(),oldseq);
	}
	
	public void insertHTML(Homepage inCHTML){
        HomepageDao inHTML = new HomepageDao();
        inHTML.save(inCHTML);       
    }
	
    public void updateHTML(Homepage upCHTML){
        HomepageDao upHTML = new HomepageDao();
        upHTML.update(upCHTML);      
    }
    
    public void insertPDF(Homepage inCPDF){
        HomepageDao inPDF = new HomepageDao();
        inPDF.save(inCPDF);        
    }
    
    public void updatePDF(Homepage upCPDF){
        HomepageDao upPDF = new HomepageDao();
        upPDF.update(upCPDF);       
    }
	

}
