package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.OtherlinksDao;
import gov.bfar.cms.domain.Otherlinks;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateOtherLink
 */
@WebServlet("/UpdateOtherLink")
public class UpdateOtherLink extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateOtherLink() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        updatecontent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatecontent(request, response);
	}
	protected void updatecontent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM-dd-yyyy hh:mm:ss aa");
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
	    OtherlinksDao otherlinksDao = new OtherlinksDao();
	    Otherlinks otherlinks = new Otherlinks();
	    otherlinks.setId(request.getParameter("id"));
	    otherlinks.setTitle(request.getParameter("title"));
	    otherlinks.setContent(request.getParameter("content"));
	    otherlinks.setStatus(Boolean.parseBoolean(request.getParameter("status")));
	    otherlinks.setModifiedBy(userid);
	    otherlinks.setPageLink(request.getParameter("pagelink"));
	    otherlinks.setModifiedDate(date.get());
	    otherlinksDao.update(otherlinks);
	    
	    request.setAttribute("navigation", "../cms/pages/otherLinks/index.jsp");
	    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
	    dispatcher.forward(request, response);
    }

}
