package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import gov.bfar.cms.dao.HomepageDao;
import gov.bfar.cms.domain.Homepage;
import gov.bfar.cms.webapp.util.CMSFiles;

/**
 * Servlet implementation class GetCMSFiles
 */
@WebServlet("/GetCMSFiles")
public class GetCMSFiles extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCMSFiles() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    getfiles(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    getfiles(request, response);
	}
	
	protected void getfiles(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String pageaction = request.getParameter("action");
	    CMSFiles cmsFiles = new CMSFiles();
	    
	    if (pageaction.equalsIgnoreCase("homepageleft")) {
	        List<CMSFiles> newgetFiles = cmsFiles.file("C:/Users/Administrator/Desktop/CMSREGIONALWEBSITE/cms.regionalwebsite/src/main/webapp/files/homepageleft/pdf");
	        HomepageDao dao = new HomepageDao();
	        List<Homepage> fileList = new ArrayList<>();
	        for (CMSFiles fname : newgetFiles) {
	            try {
	                Homepage homepagenew = dao.findNewPDFFiles("left", "pdf", "subpdf", fname.getFileName());	                
	            } catch (Exception e) {	                
	            }
	        }
        }
    }

}
