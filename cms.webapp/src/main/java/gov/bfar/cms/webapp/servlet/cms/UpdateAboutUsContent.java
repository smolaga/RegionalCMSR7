package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import gov.bfar.cms.dao.AboutUsDao;
import gov.bfar.cms.domain.AboutUs;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class UpdateAboutUsContent
 */
@WebServlet("/UpdateAboutUsContent")
public class UpdateAboutUsContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateAboutUsContent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		updateaboutuscontent(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		updateaboutuscontent(request, response);
	}
	
	protected void updateaboutuscontent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		Optional<String> date = DateUtil.getFormattedDate("MM-dd-yyyy hh:mm:ss aa");
		HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		AboutUsDao aboutUsDao = new AboutUsDao();
		AboutUs aboutUs = new AboutUs();
		aboutUs.setId(request.getParameter("id"));
		aboutUs.setTitle(request.getParameter("title"));
		aboutUs.setContent(request.getParameter("content"));
		aboutUs.setContentType("html");
		aboutUs.setSequence(Integer.parseInt(request.getParameter("sequence")));
		aboutUs.setStatus(Boolean.parseBoolean(request.getParameter("status")));
		aboutUs.setModifiedBy(userid);
		aboutUs.setPageLink(request.getParameter("pagelink"));
		aboutUs.setModifiedDate(date.get());
		aboutUsDao.update(aboutUs);
		aboutUsDao.spAutoOrdering(aboutUs.getSequence(), aboutUs.getId(), Integer.parseInt(request.getParameter("oldsequence")));
		System.out.println("OLD SEQUENCE "+Integer.parseInt(request.getParameter("oldsequence")));
		session.setAttribute("tabactiveaboutus", "main");
		response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=about");		
	}
}

