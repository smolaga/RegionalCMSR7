package gov.bfar.cms.webapp.servlet.cms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import gov.bfar.cms.dao.DownloadDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Download;
import gov.bfar.cms.util.DateUtil;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

/**
 * Servlet implementation class UpdateSubDownloadPdfContent
 */
@WebServlet("/UpdateSubDownloadPdfContent")
public class UpdateSubDownloadPdfContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String UPLOAD_DIRECTORY ="";
	private String DOWNLOAD_DIRECTORY ="";

    /**
     * @throws FileNotFoundException 
     * @see HttpServlet#HttpServlet()
     */
    public UpdateSubDownloadPdfContent() {
    	super();
    	UPLOAD_DIRECTORY = ConfigurationUtil.getConfig("downloadspdfup");
    	DOWNLOAD_DIRECTORY = ConfigurationUtil.getConfig("downloadspdfdl");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        updatesubpdf(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    updatesubpdf(request, response);
	}
	protected void updatesubpdf(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> pdfdate = DateUtil.getFormattedDate("MM-dd-yyyy");
	    HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        String pdfuuid = BaseDomain.newUUID();
        String reqTitle = null;
        String filePath = null;
        String contentType = null;
        String uuid = null;
        String pdffileupdatestatus = null;
        boolean status = false;
       
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                String fname = null;
                String fsize = null;
                String ftype = null;
                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {                      
                        fname = pdfuuid + "(" + pdfdate.get() + ")" + ".pdf";
                        fsize = new Long(item.getSize()).toString();
                        ftype = item.getContentType();
                        item.write(new File(UPLOAD_DIRECTORY + fname));
                        String rootfilepath = DOWNLOAD_DIRECTORY;
                        filePath = rootfilepath.toString() + fname;
                        String ext = FilenameUtils.getExtension(fname);                      
                        contentType = ext;                     
                    }else{
                        String fName = item.getFieldName();
                        String value = item.getString();
                        switch(fName)
                        {
                            case "title":
                                reqTitle = value;
                                break;
                            case "id":
                                uuid = value;
                                break;                         
                            case "status":
                            	status = Boolean.parseBoolean(value);
                            	break;
                            case "pdffileupdatestatus":
                            	pdffileupdatestatus = value;
                            	break;
                        
                        }                        
                    
                    }
                }              
                request.setAttribute("name", fname);
                request.setAttribute("size", fsize);
                request.setAttribute("type", ftype);
            } catch (Exception ex) {
                request.setAttribute("message", "File Upload Failed due to "
                        + ex);
            }
 
        } else {
            request.setAttribute("message", "Sorry this Servlet only handles file upload request");
        }
       
        
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");      
        Download download = new Download();
        DownloadDao downloadDao = new DownloadDao();
        download.setId(uuid);
        if (pdffileupdatestatus.equalsIgnoreCase("true")) {
        	download.setPdfFilePath(filePath);
		}else{
			Download pdffile = downloadDao.findById(uuid);
			download.setPdfFilePath(pdffile.getPdfFilePath());
		}
        download.setTitle(reqTitle);
        download.setContentType("subpdf");
        download.setSubContentLink("/WebsiteNavigation?id="+download.getId()+"&pageAction=downloadContentFrame");
        download.setStatus(status);
        download.setModifiedDate(date.get());
        download.setModifiedBy(userid);
        downloadDao.update(download);
        
        session.setAttribute("tabactivedownload", "sub");
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=download");
  
    }


}

