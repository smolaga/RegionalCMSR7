package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.regionalwebsite.bean.EmailBean;
import gov.bfar.cms.regionalwebsite.config.EmailConfig;
import gov.bfar.cms.regionalwebsite.service.EmailService;
import gov.bfar.cms.regionalwebsite.service.imp.EmailServiceImp;

/**
 * Servlet implementation class ReplayEmailInfo
 */
@WebServlet("/ReplayEmailInfo")
public class ReplayEmailInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReplayEmailInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    replayinfo(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    replayinfo(request, response);
	}
	protected void replayinfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    HttpSession session = request.getSession();
	    String id = request.getParameter("id");
	    String rcpt = request.getParameter("recipient");
	    
	    String subject = request.getParameter("subject");
	    String message = request.getParameter("message");
        EmailBean bean = new EmailBean(Arrays.asList(rcpt), subject, message);
        EmailConfig config = new EmailConfig();
        EmailService emailService = new EmailServiceImp();
        try {
            emailService.sendEmail(bean);
            session.setAttribute("sendingmessage", "Message Sent!");
        } catch (Exception e) {
            session.setAttribute("sendingmessage", "Message Failed!");
        }
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=viewemail&id="+id);
    }

}
