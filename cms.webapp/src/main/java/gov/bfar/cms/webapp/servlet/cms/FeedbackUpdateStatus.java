package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.FeedbackDao;
import gov.bfar.cms.domain.Feedback;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class FeedbackUpdateStatus
 */
@WebServlet("/FeedbackUpdateStatus")
public class FeedbackUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FeedbackUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				updatestatus(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		updatestatus(request, response);
	}
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionredirect = request.getParameter("status");
        String id = request.getParameter("contentid");
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        request.setAttribute("getidcontent", id);
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        FeedbackDao feedbackDao = new FeedbackDao();
        Feedback feedback = feedbackDao.findById(id);
        Feedback feedbacks = new Feedback();
        feedbacks.setId(feedback.getId());
        feedbacks.setRecipientAddress(feedback.getRecipientAddress());
        feedbacks.setSubject(feedback.getSubject());
        feedbacks.setContent(feedback.getContent());
        feedbacks.setDateCreated(feedback.getDateCreated());

        if (actionredirect.equalsIgnoreCase("viewemail")) {
            if (!feedback.isMarkasRead()) {
                feedbacks.setMarkasRead(true);
                feedbackDao.update(feedbacks);
            }
            response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=viewemail&id="+feedback.getId());
        } else if (actionredirect.equalsIgnoreCase("delete")) {
            feedbackDao.delete(id);
            response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=feedback");
        }
    }

}
