package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.AboutUsDao;
import gov.bfar.cms.domain.AboutUs;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class AboutUsUpdateStatus
 */
@WebServlet("/AboutUsUpdateStatus")
public class AboutUsUpdateStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AboutUsUpdateStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		updatestatus(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		updatestatus(request, response);
	}
	
	protected void updatestatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String actionredirect = request.getParameter("status");
		String id = request.getParameter("contentid");
		HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
		request.setAttribute("getidcontent", id);
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
		AboutUsDao aboutUsDao = new AboutUsDao();
		AboutUs aboutUs = aboutUsDao.findById(id);		
		AboutUs aboutUs2 = new AboutUs();
		aboutUs2.setId(aboutUs.getId());
		aboutUs2.setTitle(aboutUs.getTitle());
		aboutUs2.setContent(aboutUs.getContent());
		aboutUs2.setContentType(aboutUs.getContentType());
		aboutUs2.setPdfFilePath(aboutUs.getPdfFilePath());
		aboutUs2.setSequence(aboutUs.getSequence());
		aboutUs2.setPageLink(aboutUs.getPageLink());
		aboutUs2.setModifiedBy(userid);
		aboutUs2.setModifiedDate(date.get());
		
		if (actionredirect.equalsIgnoreCase("show")) {
			aboutUs2.setStatus(true);
			aboutUsDao.update(aboutUs2);
			request.setAttribute("navigation", "../cms/pages/aboutus/index.jsp");
		}
		else if (actionredirect.equalsIgnoreCase("hide")) {
			aboutUs2.setStatus(false);
			aboutUsDao.update(aboutUs2);
			request.setAttribute("navigation", "../cms/pages/aboutus/index.jsp");
		}
		else if (actionredirect.equalsIgnoreCase("updateall")) {
			String ctype = request.getParameter("contenttype");
			if (ctype.equalsIgnoreCase("pdf")) {
				request.setAttribute("navigation", "../cms/pages/aboutus/updatepdfContent.jsp");
			}
			else if (ctype.equalsIgnoreCase("html")) {
				request.setAttribute("navigation", "../cms/pages/aboutus/updateContent.jsp");
			}
			else if (ctype.equalsIgnoreCase("subpdf")) {
				request.setAttribute("navigation", "../cms/pages/aboutus/updateSubPDFContent.jsp");
			}
			else if (ctype.equalsIgnoreCase("subhtml")) {
				request.setAttribute("navigation", "../cms/pages/aboutus/updateSubHTMLContent.jsp");
			}
		}
		else if (actionredirect.equalsIgnoreCase("delete")) {			
			aboutUsDao.delete(id);
			request.setAttribute("navigation", "../cms/pages/aboutus/index.jsp");
		}
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/cmsIndex.jsp");
		dispatcher.forward(request, response);
	}

}
