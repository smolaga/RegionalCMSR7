package gov.bfar.cms.webapp.servlet.cms;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.UserDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.User;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertUserAccount
 */
@WebServlet("/InsertUserAccount")
public class InsertUserAccount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertUserAccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		insertContent(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    insertContent(request, response);
	}
	protected void insertContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        String uuid = BaseDomain.newUUID();
        HttpSession session = request.getSession();
        String userid = (String)session.getAttribute("userid");
        String firstName = request.getParameter("firstName");
        String middleName = request.getParameter("middleName");
        String lastName = request.getParameter("lastName");
        String designation = request.getParameter("designation");
        String usertype = request.getParameter("userType");
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        String secretQuestion = request.getParameter("secretQuestion");
        String answer = request.getParameter("answer");
        String status = request.getParameter("status");
        String dateCreated = request.getParameter("dateCreated");
        String createdBy = request.getParameter("createdBy");
        boolean canupdate = Boolean.parseBoolean(request.getParameter("canupdate"));
        boolean canshow = Boolean.parseBoolean(request.getParameter("canshow"));
        String useraccesscontent = request.getParameter("useraccesscontent");
        UserDao dao = new UserDao();
        User user = new User();
        user.setId(uuid);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        user.setDesignation(designation);
        user.setUserType(usertype);
		user.setUserName(userName);
        user.setUserPassword(password);
        user.setSecretQuestion(secretQuestion);
        user.setAnswer(answer);
        user.setStatus(false);
        user.setDateCreated(date.get());
        user.setCreatedBy(userid); 
        user.setModifiedDate(date.get());
        user.setModifiedBy(userid);
        if (usertype.equalsIgnoreCase("Administrator")) {
        	user.setUserAccessContent("ALL");
        	user.setCanUpdate(true);
        	user.setCanShow(true);
		}else{
			user.setUserAccessContent(useraccesscontent);
			user.setCanUpdate(canupdate);
			user.setCanShow(canshow);
		}
        if ( user.getUserType()!=null){
        	  dao.save(user);	
        }
        response.sendRedirect("/BFAR-R7/CMSNavigation?pageAction=viewuser");

    }

}
