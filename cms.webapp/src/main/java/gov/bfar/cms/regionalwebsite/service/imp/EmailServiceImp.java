package gov.bfar.cms.regionalwebsite.service.imp;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import gov.bfar.cms.regionalwebsite.bean.EmailBean;
import gov.bfar.cms.regionalwebsite.config.EmailConfig;
import gov.bfar.cms.regionalwebsite.service.EmailService;

public class EmailServiceImp implements EmailService {

    private static final Logger LOGGER = Logger.getLogger(EmailServiceImp.class.getName());

    private EmailConfig emailConfig = new EmailConfig();

    @Override
    public void sendEmail(EmailBean emailBean) {
        LOGGER.log(Level.INFO, "Sending email to repients ....");
        try {
            InternetAddress[] address = new InternetAddress[emailBean.getTo().size()];
            for (int i = 0; i <= emailBean.getTo().size() - 1; i++) {
                address[i] = new InternetAddress(emailBean.getTo().get(i));
            }
            Message message = new MimeMessage(emailConfig.emailSession());
            message.setFrom(new InternetAddress(EmailConfig.USERNAME));
            message.setRecipients(Message.RecipientType.CC, address);

            message.setSubject(emailBean.getSubject());
            message.setText(emailBean.getMessage());

            Transport.send(message);
            LOGGER.log(Level.INFO, "Sending email success !");

        } catch (MessagingException e) {

            LOGGER.log(Level.SEVERE, e.getMessage(), emailBean.toString());
        }
    }
    
    public static void main(String[] args) {
        EmailConfig emailConfig = new EmailConfig();
        System.out.println(emailConfig.USERNAME);
        System.out.println(emailConfig.PASSWORD);
        EmailBean bean = new EmailBean(Arrays.asList(EmailConfig.USERNAME), "e.son1628@gmail.com", "Subject : eto ang subject \nking ina ka !");
        EmailService emailService = new EmailServiceImp();
        emailService.sendEmail(bean);
    }

}
