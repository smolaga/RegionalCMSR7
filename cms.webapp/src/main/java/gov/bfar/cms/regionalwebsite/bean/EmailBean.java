package gov.bfar.cms.regionalwebsite.bean;

import java.util.List;

public class EmailBean {

    private List<String> to;
    private String subject;
    private String message;

    public EmailBean() {
        // TODO Auto-generated constructor stub
    }

    public EmailBean(List<String> to, String subject, String message) {
        this.to = to;
        this.subject = subject;
        this.message = message;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "EmailBean [to=" + to + ", subject=" + subject + ", message=" + message + "]";
    }

}
