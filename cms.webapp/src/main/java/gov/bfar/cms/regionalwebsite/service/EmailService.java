package gov.bfar.cms.regionalwebsite.service;

import gov.bfar.cms.regionalwebsite.bean.EmailBean;

public interface EmailService {

    void sendEmail(EmailBean emailBean);
}
