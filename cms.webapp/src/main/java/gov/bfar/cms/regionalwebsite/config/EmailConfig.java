package gov.bfar.cms.regionalwebsite.config;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import gov.bfar.cms.dao.EmailInquiryDao;
import gov.bfar.cms.domain.EmailInquiry;
import gov.bfar.cms.webapp.util.ConfigurationUtil;

public class EmailConfig {

    private EmailInquiryDao emailInquiryDao = new EmailInquiryDao();
    private EmailInquiry emailInquiry;
    
    public static String USERNAME;
    public static String PASSWORD;
    
    private static final String SMTP_HOST = "smtp.gmail.com";
    private static final String SMTP_PORT = ConfigurationUtil.getConfig("smtpport");
    
    public EmailConfig() {
        emailInquiry = emailInquiryDao.OneEmailSend();
        USERNAME = emailInquiry.getEmailaddress();
        PASSWORD = emailInquiry.getPassword();
    }

    private Properties emailProps() {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", SMTP_HOST);
        props.put("mail.smtp.port", SMTP_PORT);
        props.put("mail.smtp.starttls.required", "true");

        return props;
    }

    public Session emailSession() {

        Session session = Session.getInstance(emailProps(), new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD);
            }
        });

        return session;
    }
}
