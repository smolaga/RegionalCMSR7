<%@page import="org.apache.commons.lang.ArrayUtils"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%@page import="gov.bfar.cms.dao.FooterDao"%>
<%@page import="gov.bfar.cms.domain.Footer"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${pageContext.request.contextPath}/cms/media/css/cms.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/cms/media/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/media/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/extensions/Buttons/css/buttons.dataTables.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/cms/media/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet">

<link href="${pageContext.request.contextPath}/cms/media/css/jquery-ui.min.css" rel="stylesheet">

<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/media/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/extensions/Buttons/js/jszip.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/extensions/Buttons/js/pdfmake.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/extensions/Buttons/js/vfs_fonts.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/extensions/Buttons/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/DataTables-1.10.11/extensions/Buttons/js/buttons.print.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/text_editor.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/form-validator/jquery.form-validator.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/clipboard.js-master/dist/clipboard.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/script.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/multipleFileupload.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%
    String nav = "";
	User user = new User();
	user= (User) session.getAttribute("userinfo");
	String uac = user.getUserAccessContent();
	String[] useraccesscontent = uac.split("\\s*,\\s*");
	try{
	    if (request.getAttribute("navigation") == null) {
	        nav = "../cms/homepage.jsp";
	    } else {
	        nav = request.getAttribute("navigation").toString();
	    }
	    
	    if((User) session.getAttribute("userinfo") == null){%>
			<jsp:forward page="/cms/pages/login/login.jsp"></jsp:forward>
	    <%}
	}catch(Exception e){%>
	    	<jsp:forward page="/cms/pages/login/login.jsp"></jsp:forward>
	 <%}%>
<title>Region Content Management System</title>

</head>

<body>
	<div class="row" style="box-shadow: 0 3px 3px 1px #000000">
		<nav class="navbar navbar-inverse cmsNavBGColor navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
            	</button>
				
				<strong><a class="navbar-brand"	href="/BFAR-R7/CMSNavigation?pageAction=home">REGIONAL CMS</a></strong>
			</div>
		
			<div class="collapse navbar-collapse navbar-ex1-collapse" id="navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="/BFAR-R7/CMSNavigation?pageAction=home"><i class="fa fa-home fa-lg" aria-hidden="true"></i>&nbsp;Home</a></li>
					<% if (user.getUserType().equalsIgnoreCase("Administrator"))
					{
					%>
					<li><a href="/BFAR-R7/CMSNavigation?pageAction=adduser"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Add User</a></li>
					<li><a href="/BFAR-R7/CMSNavigation?pageAction=viewuser"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;View User</a></li>
					<li><a href="/BFAR-R7/CMSNavigation?pageAction=feedback"><i class="fa fa-comments" aria-hidden="true"></i>&nbsp;Feedbacks</a><li>
					<li><a href="/BFAR-R7/CMSNavigation?pageAction=settings"><i class="fa fa-cogs" aria-hidden="true"></i>&nbsp;Settings</a><li>
					<%} %>
					<li><a href="http://region7.bfar.da.gov.ph/" target="blank"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;Regional Website</a><li>
					<li><a href="/BFAR-R7/CMSNavigation?pageAction=logout"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a></li>
				</ul>
			</div>
		</div>
		</nav>
	</div>
	<div class="container">
	<div class="row">
		<div class="text-center" style="margin-top: 50px;">
	<%try{
		PicturesDao picturesDao = new PicturesDao();
		Pictures pictures = picturesDao.OneBannerDisplay();
		if(pictures.isStatus()){%>
		<img class="img-responsive" style="margin-top: 50px; box-shadow: 0 3px 3px 1px #000000" src="<%=pictures.getImageFilePath()%>" alt="CMS BANNER" height="100%" width="100%" >
	<%}}catch(Exception e){
		e.getMessage();}%>
		</div>
	</div>
	</div>


		<div class="container" style="background-color: #33b2f7; color:#052E57; border-radius: 2px; text-align: center;  box-shadow: 0 3px 3px 1px #000000">
		<br>
		<div class="row">
			<%if (ArrayUtils.contains(useraccesscontent, "BANNER")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=banner">Banner</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "SLIDER")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=slider">Slider</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "HOMEPAGELEFT")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=homepageleftcontent">Homepage Left Content</a></strong><br> 	
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "HOMEPAGECENTER")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=homepagecentercontent">Homepage Center Content</a></strong><br> 
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "HOMEPAGERIGHT")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=homepagerightcontent">Homepage Right Content</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "ABOUTUS")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=about">About Us</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "SERVICES")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=service">Services</a></strong><br>	
				</div>
			<%}%>			
			<%if (ArrayUtils.contains(useraccesscontent, "GALLERYIMAGE")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=gallery">Gallery Image</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "GALLERYVIDEOS")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=video">Gallery Videos</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "FISHERIESPROFILE")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=profile">Fisheries Profile</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "CONTACTUS")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=contactus">Contact Us</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "FOOTER")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=footer">Footer</a></strong><br>	
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "DOWNLOADS")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=download">Downloads</a></strong><br>		
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "LAWSANDREGULATION")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=laws">Laws and Regulation</a></strong><br>
				</div>
			<%}%>
			<%if (ArrayUtils.contains(useraccesscontent, "OTHERLINKS")|| uac.equalsIgnoreCase("ALL")) {%>
				<div class="col-md-3" >
					<strong><a href="/BFAR-R7/CMSNavigation?pageAction=otherlinks">Other Links</a></strong><br>	
				</div>
			<%}%>
		</div>
		<br>
	</div>
	<div class="container">
		<jsp:include page="<%=nav%>"></jsp:include>
	</div>
	



<div class="container" style="background-color: #01005e">    
           <div class="cmsfooter">      
			    <div class="col-md-12 text-center" >
			     
			<% try{ FooterDao footerDao = new FooterDao();
					Footer footer = footerDao.OneFooterDisplay(); %>
					<%=footer.getContent() %>
					<%}catch(Exception e){
						e.getMessage();
					 }%>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	if ($('#sequence').val() == 1) {
		$("#sequence").prop('readOnly','readOnly');
	}
	 // Restrict presentation length
	$('#presentation').restrictLength( $('#pres-max-length'));
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="popover"]').popover({
	        placement: 'bottom',
	        delay: {
	            show: 500,
	            hide: 100
	        }
	    });
    $('[data-toggle="popover"]').click(function () {
        setTimeout(function () {
            $('.popover').fadeOut('slow');
        }, 200);
    });

	});
try{
$.validate({
   modules : 'location, date, security, file',
   onModulesLoaded : function() {
     $('#country').suggestCountry();
     
   }
 });
}
catch(e){
}
</script>
</body>
</html>

