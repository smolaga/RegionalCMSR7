<%@page import="gov.bfar.cms.domain.Service"%>
<%@page import="gov.bfar.cms.dao.ServiceDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/text_editor.js"></script>
<script type="text/javascript">
function PreviewImage() {
    pdffile=document.getElementById("uploadPDF").files[0];
    pdffile_url=URL.createObjectURL(pdffile);
    $('#viewerupdate').attr('src',pdffile_url);
    $('#viewerupdate').attr('width','100%');
    $('#viewerupdate').attr('height','1100px');
    $('#dvpdf').addClass('hide');
    $('#dvpdfupdate').removeClass('hide');
    $('#pdffileupdatestatus').val('true');
}
</script>
<% String id = (String) request.getAttribute("getidcontent");
	ServiceDao serviceDao = new ServiceDao();
	Service service = serviceDao.findById(id);
	
	System.out.println("PDF filepath " + service.getPdfFilePath());
	String pdffp = service.getPdfFilePath();
%>
<div class="row">
<div class="wrapper">
		<form method="POST" action="/BFAR-R7/UpdateSubServicePdfContent"  enctype="multipart/form-data" onsubmit="uploading();">
			<div class="row">
				<div class="col-md-12">
					<h4>Title</h4>
				</div>
				<div class="col-md-12">
					<input type="text" name="title" required class="form-control" value="<%=service.getTitle()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>PDF File</h4>
				</div>
				<div class="col-md-12">
					<label>Choose a file : </label>							
				</div>
				<div class="col-md-12">
					<input type="hidden" name="pdffileupdatestatus" id="pdffileupdatestatus" value="false">
					<input id="uploadPDF" type="file" name="file" onchange="PreviewImage();" class="btn btn-primary"/>&nbsp;
				</div>
				<div class="col-md-12" id="dvpdf">
					<iframe id="viewer" title="PDF in an i-Frame" src="<%=service.getPdfFilePath() %>" frameborder="0" scrolling="no" height="1100" width="100%" ></iframe>
				</div>
				 <div class="col-md-12" id="dvpdfupdate" class="hide">
					<iframe id="viewerupdate" frameborder="0" scrolling="no" ></iframe>
				</div> 
			</div>		
			<input type="hidden" name="id" value="<%=service.getId() %>"/>
			<input type="hidden" name="status" value="<%=service.isStatus() %>"/>
			<input type="hidden" name="pagelink" value="<%=service.getSubContentLink() %>"/>
			<input type="hidden" name="createdby" value="<%=service.getCreatedBy() %>"/>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsavehleft" value="Save Content" />
				</div>
			</div>
		</form>
	</div>
	</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>