<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.ServiceDao"%>
<%@page import="gov.bfar.cms.domain.Service"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
%>


<div class="row">
<div class="col-md-12" style="margin-bottom: 2%"><div class="headtitle">Services (Main Content)</div>
<a href="/BFAR-R7/CMSNavigation?type=service&pageAction=insertContent"><input type="submit" class="btn btn-primary" value="Add HTML" ></a>
</div>
</div>

<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
<p style="color:blue"><i>Note: Only One HTML Content should be display.</i></p>
	<table class="table table-hover table-bordered dataTables">
		<thead>
			<tr>
				<td>Title</td>
				<td>Date Created</td>				
				<td>Status</td>
				<%if(user.isCanUpdate()){%>
				<td>Update</td>
				<%}%>	
				<%if(user.isCanShow()){%>
				<td>View in Page</td>
				<%} %>				
				<td>Copy Link</td>
				<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
				<td>Delete</td>
				<%} %>	
			</tr>
		</thead>
		<tbody>
			<%
				ServiceDao serviceDao = new ServiceDao();
				List<Service> services = serviceDao.serviceContentList("html", "pdf");
			%>
			<%
			
			for(Service service : services){
				
			%>
			<tr>
		
				<td><%=service.getTitle()%></td>
				<td><%=service.getDateCreated() %></td>
				
				<%if(service.isStatus()){%>
				    <td>Active</td>
						<%}else{%>
								<td>Inactive</td>
						<%}%>
				<%if(user.isCanUpdate()){%>
				<%if(service.getContentType().equalsIgnoreCase("pdf")){%>
						<td class="text-center">
						<a href="/BFAR-R7/ServiceUpdateStatus?contentid=<%=service.getId()%>&status=updateall&contenttype=<%=service.getContentType() %>"><img src="/BFAR-R7/cms/media/img/updatePDF.png"></a>
						</td>
						<%}else if(service.getContentType().equalsIgnoreCase("html")){ %>
						<td class="text-center">
						<a href="/BFAR-R7/ServiceUpdateStatus?contentid=<%=service.getId()%>&status=updateall&contenttype=<%=service.getContentType() %>"><img src="/BFAR-R7/cms/media/img/updateHTML.png"></a>
						</td>
						<%} %>
						<%} %>
			<%if(user.isCanShow()){%>
				<%if(service.isStatus()){%>
					<td class="text-center">	
						<a href="/BFAR-R7/ServiceUpdateStatus?contentid=<%=service.getId()%>&status=hide"><abbr title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a>
					</td> 
					<%} else{%>
					<td class="text-center">	
						<a href="/BFAR-R7/ServiceUpdateStatus?contentid=<%=service.getId()%>&status=show"><abbr title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a>
					</td>
					<%} %>
			<%}%>			
				<td class="text-center"><button class="btn btn-primary btnCopy" data-toggle="popover" data-content="Copied!" data-clipboard-text="<%=service.getPageLink()%>">Copy</button></td>
				<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
				<td class="text-center"><a href="/BFAR-R7/ServiceUpdateStatus?contentid=<%=service.getId()%>&status=delete">
					<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
				<%}%>
			</tr>
			<%
				}
			%>
			</tbody>
			
	</table>
	</div>
	</div>
	</div>