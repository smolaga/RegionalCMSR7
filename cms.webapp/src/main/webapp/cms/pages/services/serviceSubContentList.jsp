<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.ServiceDao"%>
<%@page import="gov.bfar.cms.domain.Service"%>
<%@page import="java.util.List"%>

<div class="row">
<div class="col-md-12" style="margin-bottom: 2%"><div class="headtitle">Services (Sub Content)</div>
<a href="/BFAR-R7/CMSNavigation?type=service&pageAction=insertsubContent"><input type="submit" class="btn btn-primary" value="Add HTML" ></a>
<a href="/BFAR-R7/CMSNavigation?type=service&pageAction=insertsubpdfContent"><input type="submit" class="btn btn-primary" value="Add PDF" ></a>
</div>
</div>

<div class="row">
<div class="col-md-12">
	<table class="table table-hover table-bordered dataTables" style="width:100%">
		<thead>
			<tr>
				<td>Title</td>
				<td>Date Created</td>
				<td>Content Type</td>
				<td>Update</td>
				<td>Copy Link</td>
				<td>Delete</td>
			</tr>
		</thead>
		<tbody>
			<%
				ServiceDao serviceDao = new ServiceDao();
				List<Service> services = serviceDao.serviceContentList("subhtml", "subpdf");
			%>
			<%
			
			for(Service service : services){
				
			%>
			<tr>
				<td><%=service.getTitle() %></td>
				<td><%=service.getDateCreated() %></td>
				<%if(service.getContentType().equalsIgnoreCase("subpdf")){%>
						<td class="text-center">
						<img src="/BFAR-R7/cms/media/img/updatePDF.png">
						</td>
						<%}else if(service.getContentType().equalsIgnoreCase("subhtml")){ %>
						<td class="text-center">
						<img src="/BFAR-R7/cms/media/img/updateHTML.png">
						</td>
						
						<%} %>
				
					<td class="text-center"><a href="/BFAR-R7/ServiceUpdateStatus?contentid=<%=service.getId()%>&status=updateall&contenttype=<%=service.getContentType()%>"><input type="submit" class="btn btn-primary" value="Update"></a></td>
					<td class="text-center"><button class="btn btn-primary btnCopy" data-toggle="popover" data-content="Copied!" data-clipboard-text="<%=service.getSubContentLink()%>">Copy</button></td>
					<td class="text-center"><a href="/BFAR-R7/ServiceUpdateStatus?contentid=<%=service.getId()%>&status=delete">
			    <input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
				</tr>
			<%
				}
			%>
			</tbody>
			
	</table>
	</div>
	</div>