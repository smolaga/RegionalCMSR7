<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.UserDao"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%@page import="java.util.List"%>
<script type="text/javascript">
function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>
<%User userinfo = new User();
userinfo = (User)session.getAttribute("userinfo");%>
<%if((User) session.getAttribute("userinfo") == null){%>
<jsp:forward page="/cms/pages/login/login.jsp"></jsp:forward>
<%}%>
	<div class="row">
	<div class="wrapper">

<div class="col-md-12"><div class="headtitle">User Lists</div></div>

<div class="row">
<div class="col-md-12">
	<table class="table table-hover table-bordered dataTables">
		<thead>
			<tr>
				<td>Name</td>
				<td>User Type</td>
				<td>Update</td>
				<td>Remove</td>
			</tr>
		</thead>
		<tbody>

		 <%
			UserDao userDao = new UserDao();
				List<User> user = userDao.findAll();
			%>
			<%
			
			for(User user2 : user){
				
			%>
			<tr>
				<td><%=user2.getFirstName() + " " + user2.getMiddleName() + " "+ user2.getLastName() %></td>
				<td><%=user2.getDesignation()%></td>
				<td style="text-align: center"><a href="/BFAR-R7/AccountUpdateStatus?contentid=<%=user2.getId()%>&status=updateall"><input type="submit" class="btn btn-primary" value="update"></a></td>
				<td style="text-align: center"><a href="/BFAR-R7/DeleteUserAccount?id=<%=user2.getId()%>"><input type="button" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="delete"></a></td>
			</tr>
			<%
				}
			%> 
			</tbody>
			
	</table>
	</div>
</div>
</div>	
</div>