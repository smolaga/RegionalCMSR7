<%@page import="gov.bfar.cms.domain.User"%>
<%@page import="gov.bfar.cms.dao.UserDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/text_editor.js"></script>	
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
	<% String id = (String) request.getAttribute("getidcontent");
	UserDao userDao = new UserDao();
	User user = userDao.findById(id);
	System.out.println(id);
	%>
<body>
<%User userinfo = new User();
userinfo = (User)session.getAttribute("userinfo");%>
<%if((User) session.getAttribute("userinfo") == null){%>
<jsp:forward page="/cms/pages/login/login.jsp"></jsp:forward>
<%}%>
<script type="text/javascript">
	$(document).ready(function(){
		var usertype = "<%=user.getUserType() %>";
		$('#type option[value="<%=user.getUserType() %>"]').attr("selected", "selected");
		if(usertype == "Post User"){
			$('#puaccess').removeClass('hide');
		}else{
			$('#puaccess').addClass('hide');
		}
		
		getuseraccesscontentlist();
		canupdatestatus();
		canshowstatus();
		$(".cmscontent").click(function(){
			showSelectedUserAccessContent();
		});
		$('#cbcanupdate').click(function(){
			$('#canupdate').val($('#cbcanupdate:checked').val());
		});
		$('#cbcanshow').click(function(){
			$('#canshow').val($('#cbcanshow:checked').val());
		});
		$('#allcontent').click(function(){
			if($("#allcontent").is(':checked')){
				$(".cmscontent").prop('checked', true);
				showSelectedUserAccessContent();
				$(".cmscontent").attr("disabled", true);
			} else {
				$(".cmscontent").prop('checked', false);
				showSelectedUserAccessContent();
				getuseraccesscontentlist();
				$(".cmscontent").attr("disabled", false);
			}
			
		});
		<%-- $('#question').val('<%=user.getSecretQuestion() %>'); --%>

	});
	
	
function showSelectedUserAccessContent(){
	var uac = $(".cmscontent:checked").map(function () {return this.value;}).get().join(",").toUpperCase();
  $('#useraccesscontent').val(uac);
}
function getuseraccesscontentlist(){
	var uac = "<%=user.getUserAccessContent()%>";
	var temp = new Array();
	temp = uac.split(",");
	temp.forEach(function(item){
	    console.log('input[type="checkbox"][value="' + item + '"]');
		$('input[type="checkbox"][value="' + item + '"]').attr('checked', true);
	});
	showSelectedUserAccessContent();
}
function canupdatestatus(){
	var cbcup = <%=user.isCanUpdate()%>;
	if(cbcup == true){
		$('#cbcanupdate').attr('checked', true);
		$('#canupdate').val($('#cbcanupdate:checked').val());
	}
}
function canshowstatus(){
	var cbcsh = <%=user.isCanShow()%>;
	if(cbcsh == true){
		$('#cbcanshow').attr('checked', true);
		$('#canshow').val($('#cbcanshow:checked').val());
	}
}
</script>

<div class="row">
	<div class="wrapper">
		<form method="POST" action="/BFAR-R7/UpdateUserAccount">
		<input type="hidden" value="<%=user.getId()%>" name="id">
		<input type="hidden" name="createdby" value="<%=user.getCreatedBy() %>"/>
			<div class="row">
				<div class="col-md-12">
					<h4>First Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="firstName" class="form-control" value="<%=user.getFirstName()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Middle Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="middleName" class="form-control"  value="<%=user.getMiddleName()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Last Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="lastName" class="form-control" value="<%=user.getLastName()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Designation:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="designation" class="form-control" value="<%=user.getDesignation()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Select Type of User:</h4>
				</div>
				<div class="col-md-8">
					<select name="userType" id="type" class="form-control">
					  <option value="0">--Select--</option>
					  <option value="Administrator">Administrator</option>
					  <option value="Post User">Post User</option>
					 </select>
				</div>
			</div>
			<div id="puaccess" class="row hide">
				<br>
				<div class="col-md-3">
					<label><input type="checkbox" value="true" id="cbcanupdate">can update content?</label>
					<input type="hidden" name="canupdate" id="canupdate">
				</div>
				<div class="col-md-3">
					<label><input type="checkbox" value="true" id="cbcanshow">can show content?</label>
					<input type="hidden" name="canshow" id="canshow">
				</div>
				<br>
				<div class="col-md-12">
					<h4>Select Content Access:</h4>
					<input type="hidden"  name="useraccesscontent" id="useraccesscontent" class="form-control">
				</div>
				<div class="col-md-3">
					<label><input type="checkbox" class="cmscontent" value="BANNER">Banner</label><br>
					<label><input type="checkbox" class="cmscontent" value="SLIDER">Slider</label><br>
					<label><input type="checkbox" class="cmscontent" value="HOMEPAGELEFT">Homepage Left Content</label><br>
					<label><input type="checkbox" class="cmscontent" value="HOMEPAGECENTER">Homepage Center Content</label><br>					
				</div>
				<div class="col-md-3">
				<label><input type="checkbox" class="cmscontent" value="HOMEPAGERIGHT">Homepage Right Content</label><br>
					<label><input type="checkbox" class="cmscontent" value="ABOUTUS">About Us</label><br>					
					<label><input type="checkbox" class="cmscontent" value="SERVICES">Services</label><br>
					<label><input type="checkbox" class="cmscontent" value="GALLERYIMAGE">Gallery Image</label><br>					
				</div>
				<div class="col-md-3">	
					<label><input type="checkbox" class="cmscontent" value="GALLERYVIDEOS">Gallery Videos</label><br>				
					<label><input type="checkbox" class="cmscontent" value="FISHERIESPROFILE">Fisheries Profile</label><br>
					<label><input type="checkbox" class="cmscontent" value="CONTACTUS">Contact Us</label><br>
					<label><input type="checkbox" class="cmscontent" value="FOOTER">Footer</label><br>					
				</div>
				<div class="col-md-3">				
					<label><input type="checkbox" class="cmscontent" value="DOWNLOADS">Downloads</label><br>					
					<label><input type="checkbox" class="cmscontent" value="LAWSANDREGULATION">Laws and Regulation</label><br>	
					<label><input type="checkbox" class="cmscontent" value="OTHERLINKS">Other Links</label><br>						
					<label><input type="checkbox" id="allcontent" value="ALL">ALL</label><br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>User Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="userName" class="form-control" value="<%=user.getUserName()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Password:</h4>
				</div>
				<div class="col-md-8">
					<input type="password" name="userPassword" class="form-control" value="<%=user.getUserPassword()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4 class="hidden">Select Secret Question:</h4>
				</div>
				<div class="col-md-8">
					<select class="hidden" name="secretQuestion" id="question" class="form-control">
					  <option value="0">--Select--</option>
					  <option value="What is your favorite pet">What is your favorite pet?</option>
					  <option value="What is your youngest brother's birthday">What is your youngest brother's birthday?</option>
					  <option value="When you were young, what did you want to be when you grew up"> When you were young, what did you want to be when you grew up?</option>
					  <option value="What was your childhood nickname">What was your childhood nickname?</option>
					  <option value="What is your oldest cousin's first and last name">What is your oldest cousin's first and last name?</option>
					 </select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4 class="hidden">Answer</h4>
				</div>
				<div class="col-md-8">
					<input class="hidden" type="text" name="answer" class="form-control" value="<%=user.getAnswer()%>"/>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsaveleft" value="Update Content" />
				</div>
			</div>
		</form>
	</div>
	</div>
	
</body>
</html>

<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>