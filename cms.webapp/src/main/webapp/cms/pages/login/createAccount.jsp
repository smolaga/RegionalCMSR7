<%@page import="gov.bfar.cms.domain.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%User userinfo = new User();
userinfo = (User)session.getAttribute("userinfo");%>
<%if((User) session.getAttribute("userinfo") == null){%>
<jsp:forward page="/cms/pages/login/login.jsp"></jsp:forward>
<%}%>
<div class="row">
<div id="wrapper">
	<div style="padding: 2cm; background-color: white;  box-shadow: 0 3px 3px 1px #000000">
		<form method="POST" action="/BFAR-R7/InsertUserAccount">
			<div class="row">
			<div class="headtitle">ADD USER</div>
				<div class="col-md-12">
					<h4>First Name:</h4>
				</div>
				<div class="col-md-8">
					<!-- <input type="text" name="firstName" class="form-control" data/> -->
					<input name="firstName" class="form-control" 			
					 aria-describedby="name-format" aria-required="true" pattern="[a-z A-Z]{2,}" 
					 oninvalid="setCustomValidity('Input must be letters only (*Minimum of 2 characters)')" onchange="try{setCustomValidity('')}catch(e){}"
					 autocomplete="off">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Middle Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="middleName" class="form-control" 
					aria-describedby="name-format" aria-required="true" pattern="[a-z A-Z]{2,}" 
					oninvalid="setCustomValidity('Input must be letters only (*Minimum of 2 characters)')" onchange="try{setCustomValidity('')}catch(e){}"
					autocomplete="off">  		  
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Last Name:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="lastName" class="form-control"
					aria-describedby="name-format" aria-required="true" pattern="[a-z A-Z]{2,}" 
					 required oninvalid="setCustomValidity('Input must be letters only (*Minimum of 2 characters)')" onchange="try{setCustomValidity('')}catch(e){}"
					 autocomplete="off">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Designation:</h4>
				</div>
				<div class="col-md-8">
					<input type="text" name="designation" class="form-control" required autocomplete="off">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Select Type of User:</h4>
				</div>
				<div class="col-md-8">
					<select name="userType" data-validation="required" id="usertype" class="form-control" autocomplete="off">
					  <option value="">--Select--</option>
					  <option value="Administrator">Administrator</option>
					  <option value="Post User">Post Users</option>
					 </select>
				</div>
			</div>
			<div id="puaccess" class="row hide">
				<br>
				<div class="col-md-3">
					<label><input type="checkbox" value="true" id="cbcanupdate">can update content?</label>
					<input type="hidden" name="canupdate" id="canupdate">
				</div>
				<div class="col-md-3">
					<label><input type="checkbox" value="true" id="cbcanshow">can show content?</label>
					<input type="hidden" name="canshow" id="canshow">
				</div>
				<br>
				<div class="col-md-12">
					<h4>Select Content Access:</h4>
					<input type="hidden"  name="useraccesscontent" id="useraccesscontent" class="form-control">
				</div>
				<div class="col-md-3">
					<label><input type="checkbox" class="cmscontent" value="banner">Banner</label><br>
					<label><input type="checkbox" class="cmscontent" value="slider">Slider</label><br>
					<label><input type="checkbox" class="cmscontent" value="homepageleft">Homepage Left Content</label><br>
					<label><input type="checkbox" class="cmscontent" value="homepagecenter">Homepage Center Content</label><br>					
				</div>
				<div class="col-md-3">
				<label><input type="checkbox" class="cmscontent" value="homepageright">Homepage Right Content</label><br>
					<label><input type="checkbox" class="cmscontent" value="aboutus">About Us</label><br>					
					<label><input type="checkbox" class="cmscontent" value="services">Services</label><br>
					<label><input type="checkbox" class="cmscontent" value="galleryimages">Gallery Image</label><br>					
				</div>
				<div class="col-md-3">	
					<label><input type="checkbox" class="cmscontent" value="galleryvideos">Gallery Videos</label><br>				
					<label><input type="checkbox" class="cmscontent" value="fisheriesprofile">Fisheries Profile</label><br>
					<label><input type="checkbox" class="cmscontent" value="contactus">Contact Us</label><br>
					<label><input type="checkbox" class="cmscontent" value="footer">Footer</label><br>					
				</div>
				<div class="col-md-3">				
					<label><input type="checkbox" class="cmscontent" value="downloads">Downloads</label><br>					
					<label><input type="checkbox" class="cmscontent" value="lawsandregulation">Laws and Regulation</label><br>	
					<label><input type="checkbox" class="cmscontent" value="otherlinks">Other Links</label><br>						
					<label><input type="checkbox" id="allcontent" value="all">ALL</label><br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>User Name:</h4>
					<span id="userNotification" style="color:red"><i>Username is already in used. Please choose another username.</i></span>
				</div>
				<div class="col-md-8">
			<input name="userName" id="userName" data-validation="length alphanumeric"  class="form-control"
			 data-validation-length="3-12" 
			 data-validation-error-msg="Username must be an alphanumeric value (3-12 chars)" onblur="samplejax()" autocomplete="off">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Password:</h4>
				</div>
				<div class="col-md-8">					
					<input name="password_confirmation" type="password" class="form-control" data-validation-length="6-12" data-validation="strength" 
					 data-validation-strength="2" autocomplete="off">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Repeat Password:</h4>
				</div>
				<div class="col-md-8">
					<input name="password" type="password" autocomplete="off" data-validation="confirmation" class="form-control" autocomplete="off">
				</div>
			</div>
    
			<div class="row">
				<div class="col-md-12">
					<h4 class="hidden">Select Secret Question:</h4>
				</div>
				<div class="col-md-8">
					<select class="hidden" name="secretQuestion"  class="form-control" data-validation="required" autocomplete="off">
					  <option value="">--Select--</option>
					  <option value="What is your favorite pet?">What is your favorite pet?</option>
					  <option value="What is your youngest brother's birthday?">What is your youngest brother's birthday?</option>
					  <option value="When you were young, what did you want to be when you grew up?"> When you were young, what did you want to be when you grew up?</option>
					  <option value="What was your childhood nickname?">What was your childhood nickname?</option>
					  <option value="What is your oldest cousin's first and last name?">What is your oldest cousin's first and last name?</option>
					 </select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4 class="hidden">Answer</h4>
				</div>
				<div class="col-md-8">
			<!-- 		<input type="text" name="answer" class="form-control"/> -->
					<input type="hidden" name="answer" class="form-control" required autocomplete="off">
				</div>
			</div>
			
			<br><br>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsavehleft" value="Save Content" />
				</div>
			</div>
			
		</form>
		<!-- <button onclick="samplejax()">sample </button> -->
	</div>
	</div>
	</div>
<script>
$(document).ready(function(){
	$(".cmscontent").click(function(){
		showSelectedUserAccessContent();
	});
	$('#cbcanupdate').click(function(){
		$('#canupdate').val($('#cbcanupdate:checked').val());
	});
	$('#cbcanshow').click(function(){
		$('#canshow').val($('#cbcanshow:checked').val());
	});
	$('#allcontent').click(function(){
		if($("#allcontent").is(':checked')){
			$(".cmscontent").prop('checked', true);
			showSelectedUserAccessContent();
			$(".cmscontent").attr("disabled", true);
		} else {
			$(".cmscontent").prop('checked', false);
			showSelectedUserAccessContent();
			$(".cmscontent").attr("disabled", false);
		}
	});
	$('#usertype').change(function(){
		var val = $(this).val();
		if(val == "Post User"){
			$('#puaccess').removeClass('hide');
		}else{
			$('#puaccess').addClass('hide');
		}
	});
	$("#userNotification").hide();
	
});

function showSelectedUserAccessContent()
{
	var uac = $(".cmscontent:checked").map(function () {return this.value;}).get().join(",").toUpperCase();
  $('#useraccesscontent').val(uac);
  
}
	function samplejax(){
		$.ajax({
			  method: "GET",
			  url: "validatebyUsername",
			  data:{username:$("#userName").val()}
			})
			  .done(function(userCount) {
			    if(userCount==1){
			    	$("#userNotification").show(500);
			    }
			    else
			    	$("#userNotification").hide(500);
			  })
			  .error(function(){
				  alert("error");
			  });
		
	}
</script>
<!-- <script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode == 32);
}
</script> -->

<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>