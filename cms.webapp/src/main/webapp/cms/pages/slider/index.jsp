<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
    String action = "";
	String btnimage = "";
	Pictures imgBanner = new Pictures();
	PicturesDao imgDao = new PicturesDao();
	if(request.getAttribute("imgid") != null){
		action = "updateimg";
		btnimage = "Update Image";
		imgBanner = imgDao.findById(request.getAttribute("imgid").toString());
	}else{
		action = "insertimg";
		btnimage = "Insert Image";
		imgBanner.setImageFilePath("");
		imgBanner.setId("");
		imgBanner.setPageLink("");
		int seq = imgDao.getSuggestedSequence("slider","slider");
		imgBanner.setSequence(seq);
	}
%>
<script type='text/javascript'>
	function bannerImage() {
		pdffile = document.getElementById("uploadIMG").files[0];
		pdffile_url = URL.createObjectURL(pdffile);
		$('#viewer').attr('src', pdffile_url);
		$('#viewer').attr('width', '100%');
		$('#viewer').attr('height', '100%');
	}
	$(document).ready(function() {
		if($('#txtLink').val().length > 0){
			$("#addLink").show();
		}
		if ($('#slidersequence').val() == 1 && $('#insertimg').val() == 'Insert Image'){
			$('#slidersequence').prop('readOnly','readOnly');
		}
		$("#chkaddLink").click(function() {
			if ($(this).is(":checked")) {
				$("#addLink").show();
			} else {
				$("#addLink").hide();
			}
		});
		$("#insertimg").click(function(){
			if ($('#uploadIMG').get(0).files.length === 0) {
				alert("No files selected.");
				return false;
			}
		});
		$(function() {
			$("#uploadIMG").change(function() {
				bannerImage();
				$("#imgchange").val("true");
			});
		});
		
	});
	function overlay() {
		el = document.getElementById("overlay");
		el.style.visibility = (el.style.visibility == "visible") ? "hidden"
				: "visible";
	}

</script>
<div class="row" style="background-color: white; box-shadow: 0 3px 3px 1px #000000">
	<div class="col-md-12"></div>
	<div class="col-md-12" style="margin-top:20px">
		<div class="headtitle">Slider</div>
	</div>
	<div class="col-md-12"><br></div>
	<form action="/BFAR-R7/InsertPictures?type=slider" method="POST" enctype="multipart/form-data" onsubmit="uploading();">
		<div class="col-md-2">
			<label>Choose a file : </label>
		</div>
		<div class="col-md-10">
			<input id="uploadIMG" type="file" class="btn btn-primary" name="file" class="btn btn-primary"/>
		</div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-2"><label>Order : </label></div>
		<div class="col-md-3">
		<input type="hidden" name="oldsequence" value="<%=imgBanner.getSequence()%>" /> 
		<input type="text" class="form-control" id="slidersequence" name="sequence" value="<%=imgBanner.getSequence()%>"  aria-describedby="name-format" aria-required="true" pattern="[0-9]+" required oninvalid="setCustomValidity('Please Enter Number only')"
					 onchange="try{setCustomValidity('')}catch(e){}"></div>		 	

		<div class="col-md-12">&nbsp;</div>
		 <!-- charm -->
		<div class="col-md-2"><input type="checkbox" id="chkaddLink">Add Link? </div>
			<div class="col-md-6" id="addLink" style="display:none;" >
			<input type="text" class="form-control" id="txtLink" name="sliderPageLink" value="<%=imgBanner.getPageLink()%>"/>
			</div>
			 <!-- charm -->
			 <hr>
			 <div class="col-md-12">&nbsp;</div>
			 <div class="col-md-12">
			<input type="hidden" name="imgchange" id="imgchange" value="false">
			<input type="hidden" name="type" value="slider">
			<input type="hidden" name="action" id="action" value="<%=action%>">
			<input type="hidden" name="imgid" value="<%=imgBanner.getId()%>">
			<input type="submit" class="btn btn-primary" id="<%=action%>" value="<%=btnimage%>"/>
		</div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-12" align="center">
			<%if(imgBanner.getImageFilePath().equalsIgnoreCase("")){%>
			<div style="width: 50%; height: 50%; border: 5px groove black;">
				<img alt="PREVIEW IMAGE" class="img-responsive" id="viewer">
			</div>
			<% }else{%>
			<div style="width: 50%; height: 50%; border: 5px groove black;" class="bg-primary">
				<label>New</label>
				<img  class="img-responsive" id="viewer">
			</div>
			<br>
			<div style="width: 50%; height: 50%; border: 5px groove black;" class="bg-primary">
				<label>Recent</label>
				<img class="img-responsive" src="<%=imgBanner.getImageFilePath()%>">
			</div>
			<% } %>
		</div>
	</form>
	<div class="col-md-12"><br></div>
	<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
		<table class="table table-hover table-bordered dataTables">
			<thead>
				<tr>
					<td>Image</td>
					<td>Date Created</td>
					<td>Order</td>
					<td>Status</td>
					<%if(user.isCanShow()){%>
					<td>View in Page</td>
					<%} %>
					<%if(user.isCanUpdate()){%>
					<td>Update</td>
					<%} %>
					<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
					<td>Delete</td>
					<%} %>
				</tr>
			</thead>
			<tbody>
				<%
					PicturesDao picturesDao = new PicturesDao();
					List<Pictures> pictures = picturesDao.sliderListAll();
				%>
				<%
					for (Pictures pictures2 : pictures) {
				%>
				<tr>
					<td style="width: 20%; height: 50%;"><img class="img-responsive" width="100%" height="100%" src="<%=pictures2.getImageFilePath()%>"></td>
					<td><%=pictures2.getDateCreated()%></td>
					<td class="text-center"><%=pictures2.getSequence()%></td>
					<%if(pictures2.isStatus()){%>
				    <td class="text-center">Active</td>
						<%}else{%>
								<td class="text-center">Inactive</td>
						<%}%>
				<%if(user.isCanShow()){%>
					<%if (pictures2.isStatus()) {%>
					<td class="text-center"><a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures2.getId()%>&status=hide&type=slider"><abbr
							title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a></td>
					<%} else {%>
					<td class="text-center"><a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures2.getId()%>&status=show&type=slider"><abbr
							title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a></td>
					<%}%>
				<%} %>
				<%if(user.isCanUpdate()){%>
					<td class="text-center"><a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures2.getId()%>&status=updateall&type=slider"><input type="submit" class="btn btn-primary" value="Update"></a></td>
				<%} %>
				<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
					<td class="text-center"><a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures2.getId()%>&status=delete&type=slider">
					<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
				<%} %>
				</tr>
				<% } %>
			</tbody>
		</table>
		</div>
	</div>
	<div class="col-md-12"><br></div>
	<div class="col-md-12"><br></div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>