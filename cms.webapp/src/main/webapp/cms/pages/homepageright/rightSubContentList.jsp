<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="java.util.List"%>

<script type="text/javascript">
function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>


<div class="row">
<div class="col-md-12"><div class="headtitle">Homepage Right Sub Content</div></div>
<div class="col-md-12">
<a href="/BFAR-R7/CMSNavigation?type=right&pageAction=insertsubContent"><input type="submit" class="btn btn-primary" value="Add HTML" ></a>
<a href="/BFAR-R7/CMSNavigation?type=right&pageAction=insertsubpdfContent"><input type="submit" class="btn btn-primary" value="Add Pdf" ></a>
</div>
</div><br>

<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
	<table class="table table-hover table-bordered dataTables" style="width:100%">
		<thead>
			<tr>
				<td>Title</td>
				<td>Date Created</td>
				<td>Content Type</td>
				<td>Update</td>
				<td>Copy Link</td>
				<td>Delete</td>
				</tr>
		</thead>
		<tbody>
			<%
				
				HomepageDao homepageDao = new HomepageDao();
				List<Homepage> homepages = homepageDao.subContentList("right", "subhtml", "subpdf");
			%>
			<%
				for (Homepage homepage : homepages) {
			%>
			<tr>
				<td><%=homepage.getHead() %></td>
				<td><%=homepage.getDateCreated() %></td>				
				<%if(homepage.getContentType().equalsIgnoreCase("subpdf")){%>
				<td class="text-center">
				<img src="/BFAR-R7/cms/media/img/updatePDF.png">
				</td>
				<%}else if(homepage.getContentType().equalsIgnoreCase("subhtml")){ %>
				<td class="text-center">
				<img src="/BFAR-R7/cms/media/img/updateHTML.png">
				</td>
				
				<%} %>  
				
							
				<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=homepage.getId()%>&status=updateall&columnType=right&contentType=<%=homepage.getContentType()%>"><input type="submit" class="btn btn-primary" value="Update"></a></td>
				<td class="text-center"><button class="btn btn-primary btnCopy" data-toggle="popover" data-content="Copied!" data-clipboard-text="<%=homepage.getSubContentLink()%>">Copy</button></td>
				<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=homepage.getId()%>&status=delete&columnType=right">
				<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
				</tr>
			<%
				}
			%>
			</tbody>
			
	</table>
	</div>
	</div>
</div>
