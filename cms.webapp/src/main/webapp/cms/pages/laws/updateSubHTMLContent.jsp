<%@page import="gov.bfar.cms.domain.Laws"%>
<%@page import="gov.bfar.cms.dao.LawsDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<% String id = (String) request.getAttribute("getidcontent");
LawsDao lawsDao = new LawsDao();
Laws laws = lawsDao.findById(id);
%>

<div class="row">
<div class="wrapper">
		<form method="POST" action="/BFAR-R7/UpdateSubLawsContent" onsubmit="uploading();">
			<div class="row">
				<div class="col-md-12">
					<h4>Title</h4>
				</div>
				<div class="col-md-12">
					<input type="text" required name="title" class="form-control" value="<%=laws.getTitle()%>"/>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<h4>Content</h4>
					<br>
					<textarea id="tacontent" class="mceEditor" style="height: 500px" name="content" ><%=laws.getContent() %></textarea>
				</div>
			</div>
			<input type="hidden" name="id" value="<%=laws.getId() %>"/>
			<input type="hidden" name="status" value="<%=laws.isStatus() %>"/>
			<input type="hidden" name="pagelink" value="<%=laws.getSubContentLink() %>"/>
			<input type="hidden" name="createdby" value="<%=laws.getCreatedBy() %>"/>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsavehleft" value="Save Content" />
				</div>
			</div>
		</form>
		</div></div>