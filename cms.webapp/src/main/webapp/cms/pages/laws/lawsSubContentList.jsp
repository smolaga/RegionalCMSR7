<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.LawsDao"%>
<%@page import="gov.bfar.cms.domain.Laws"%>
<%@page import="java.util.List"%>


<div class="row">
<div class="col-md-12" style="margin-bottom: 2%"><div class="headtitle">Laws and Regulation (Sub Content)</div>
<a href="/BFAR-R7/CMSNavigation?type=laws&pageAction=insertsubContent"><input type="submit" class="btn btn-primary" value="Add HTML" ></a>
<a href="/BFAR-R7/CMSNavigation?type=laws&pageAction=insertsubpdfContent"><input type="submit" class="btn btn-primary" value="Add PDF" ></a>
</div>
</div>

<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
	<table class="table table-hover table-bordered dataTables" style="width:100%">
		<thead>
			<tr>
				<td>Title</td>
				<td>Date Created</td>
				<td>Content Type</td>
				<td>Update</td>
				<td>Copy Link</td>
				<td>Delete</td>
			</tr>
		</thead>
		<tbody>
			<%
			LawsDao lawsDao = new LawsDao();
				List<Laws> laws = lawsDao.lawsContentList("subhtml", "subpdf");
			%>
			<%
			
			for(Laws laws2 : laws){
				
			%>
			<tr>
				<td><%=laws2.getTitle() %></td>
				<td><%=laws2.getDateCreated() %></td>
				<%if(laws2.getContentType().equalsIgnoreCase("subpdf")){%>
						<td class="text-center">
						<img src="/BFAR-R7/cms/media/img/updatePDF.png">
						</td>
						<%}else if(laws2.getContentType().equalsIgnoreCase("subhtml")){ %>
						<td class="text-center">
						<img src="/BFAR-R7/cms/media/img/updateHTML.png">
						</td>
						
						<%} %>
				
					<td class="text-center"> <a href="/BFAR-R7/LawsUpdateStatus?contentid=<%=laws2.getId()%>&status=updateall&contenttype=<%=laws2.getContentType()%>"><input type="submit" class="btn btn-primary" value="Update"></a></td>
					<td class="text-center"><button class="btn btn-primary btnCopy" data-toggle="popover" data-content="Copied!" data-clipboard-text="<%=laws2.getSubContentLink()%>">Copy</button></td>
					<td class="text-center"><a href="/BFAR-R7/LawsUpdateStatus?contentid=<%=laws2.getId()%>&status=delete">
					<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
			</tr>
			<%
				}
			%>
			</tbody>
			
	</table>
	</div>
	</div>
	</div>