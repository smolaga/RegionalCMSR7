<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<div class="row">
<div class="wrapper">
		<form method="POST" action="/BFAR-R7/InsertLawsSubContent" onsubmit="uploading();">
			<div class="row">
			<div class="subtitle">Laws and Regulation (Sub Content) (add HTML)</div>
				<div class="col-md-12">
					<h4>Title</h4>
				</div>
				<div class="col-md-12">
					<input type="text" required name="title" class="form-control"/>
				</div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col-md-12">
					<h4>Content</h4>
					<br>
					<textarea id="tacontent" class="mceEditor" style="height: 500px" name="content"></textarea>
				</div>
				<div class="col-md-2"><br></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsavehleft" value="Save Content" />
				</div>
			</div>
		</form>
		</div></div>
