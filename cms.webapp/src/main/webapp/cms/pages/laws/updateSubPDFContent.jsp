<%@page import="gov.bfar.cms.domain.Laws"%>
<%@page import="gov.bfar.cms.dao.LawsDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/cms/media/js/text_editor.js"></script>
<script type="text/javascript">
function PreviewImage() {
    pdffile=document.getElementById("uploadPDF").files[0];
    pdffile_url=URL.createObjectURL(pdffile);
    $('#viewerupdate').attr('src',pdffile_url);
    $('#viewerupdate').attr('width','100%');
    $('#viewerupdate').attr('height','1100px');
    $('#dvpdf').addClass('hide');
    $('#dvpdfupdate').removeClass('hide');
    $('#pdffileupdatestatus').val('true');
}
</script>
<% String id = (String) request.getAttribute("getidcontent");
LawsDao lawsDao = new LawsDao();
Laws laws = lawsDao.findById(id);
	
	System.out.println("PDF filepath " + laws.getPdfFilePath());
	String pdffp = laws.getPdfFilePath();
%>
<div class="row">
<div class="wrapper">
		<form method="POST" action="/BFAR-R7/UpdateSubLawsPdfContent"  enctype="multipart/form-data" onsubmit="uploading();">
			<div class="row">
				<div class="col-md-12">
					<h4>Title</h4>
				</div>
				<div class="col-md-12">
					<input type="text" required name="title" class="form-control" value="<%=laws.getTitle()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>PDF File</h4>
				</div>
				<div class="col-md-12">
					<label>Choose a file : </label>							
				</div>
				<div class="col-md-12">
					<input type="hidden" name="pdffileupdatestatus" id="pdffileupdatestatus" class="form-control" value="false"/>
					<input id="uploadPDF" type="file" name="file" onchange="PreviewImage();" class="btn btn-primary"/>&nbsp;
				</div>
				<div class="col-md-12" id="dvpdf">
					<iframe id="viewer" title="PDF in an i-Frame" src="<%=laws.getPdfFilePath() %>" frameborder="0" scrolling="no" height="1100" width="100%" ></iframe>
				</div>
				 <div class="col-md-12" id="dvpdfupdate" class="hide">
					<iframe id="viewerupdate" frameborder="0" scrolling="no" ></iframe>
				</div> 
			</div>		
			<input type="hidden" name="id" value="<%=laws.getId() %>"/>
			<input type="hidden" name="status" value="<%=laws.isStatus() %>"/>
			<input type="hidden" name="pagelink" value="<%=laws.getSubContentLink() %>"/>
			<input type="hidden" name="createdby" value="<%=laws.getCreatedBy() %>"/>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsavehleft" value="Save Content" />
				</div>
			</div>
		</form>
	</div>
	</div>