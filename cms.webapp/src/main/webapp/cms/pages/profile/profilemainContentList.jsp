<%@page import="gov.bfar.cms.dao.ProfileDao"%>
<%@page import="gov.bfar.cms.domain.Profile"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
%>


<div class="row">
<div class="col-md-12" style="margin-bottom: 2%"><div class="headtitle">Fisheries Profile (Main Content)</div>
<a href="/BFAR-R7/CMSNavigation?type=profile&pageAction=insertContent"><input type="submit" class="btn btn-primary" value="Add HTML" ></a>
<a href="/BFAR-R7/CMSNavigation?type=profile&pageAction=insertpdfContent"><input type="submit" class="btn btn-primary" value="Add PDF" ></a>
</div>
</div>

<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
	<table class="table table-hover table-bordered dataTables">
		<thead>
			<tr>
				<td>Title</td>
				<td>Date Created</td>
				<td>Order</td>				
				<td>Status</td>
				<%if(user.isCanUpdate()){%>
				<td>Update</td>
				<%} %>
				<%if(user.isCanShow()){%>
				<td>View in Page</td>
				<%} %>
				<td>Copy Link</td>
				<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
				<td>Delete</td>
				<%} %>
			</tr>
		</thead>
		<tbody>
			<%
				ProfileDao profileDao = new ProfileDao();
				List<Profile>profiles = profileDao.profileContentList("html", "pdf");
			%>
			<%
				for (Profile profile : profiles) {
			%>
			<tr>
				<td><%=profile.getTitle() %></td>
				<td><%=profile.getDateCreated() %></td>
				<td style="text-align: center"><%=profile.getSequence() %></td>
				
				<%if(profile.isStatus()){%>
				    <td class="text-center">Active</td>
						<%}else{%>
								<td class="text-center">Inactive</td>
						<%}%>
				<%if(user.isCanUpdate()){%>
				<%if(profile.getContentType().equalsIgnoreCase("pdf")){%>
						<td class="text-center">
						<a href="/BFAR-R7/ProfileUpdateStatus?contentid=<%=profile.getId()%>&status=updateall&contenttype=<%=profile.getContentType()%>"><img src="/BFAR-R7/cms/media/img/updatePDF.png"></a>
						</td>
						<%}else if(profile.getContentType().equalsIgnoreCase("html")){ %>
						<td class="text-center">
						<a href="/BFAR-R7/ProfileUpdateStatus?contentid=<%=profile.getId()%>&status=updateall&contenttype=<%=profile.getContentType()%>"><img src="/BFAR-R7/cms/media/img/updateHTML.png"></a>
						</td>
					<%}%>	
					<%} %>
			<%if(user.isCanShow()){%>
				<%if(profile.isStatus()){%>
					<td class="text-center">	
						<a href="/BFAR-R7/ProfileUpdateStatus?contentid=<%=profile.getId()%>&status=hide"><abbr title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a>
					</td> 
					<%} else{%>
					<td class="text-center">	
						<a href="/BFAR-R7/ProfileUpdateStatus?contentid=<%=profile.getId()%>&status=show"><abbr title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a>
					</td>
					<%} %>
					<%} %>
					<td class="text-center"><button class="btn btn-primary btnCopy" data-toggle="popover" data-content="Copied!" data-clipboard-text="<%=profile.getPageLink()%>">Copy</button></td>
					<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
					<td class="text-center"><a href="/BFAR-R7/ProfileUpdateStatus?contentid=<%=profile.getId()%>&status=delete">
					<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
					<%} %>
			</tr>
			<%
				}
			%>
			</tbody>
			
	</table>
	</div>
	</div>
	</div>