<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.FeedbackDao"%>
<%@page import="gov.bfar.cms.domain.Feedback"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="gov.bfar.cms.domain.User"%>

			<%User user = new User();
            user = (User)session.getAttribute("userinfo");%>
            <%if((User) session.getAttribute("userinfo") == null){%>
			<jsp:forward page="/cms/pages/login/login.jsp"></jsp:forward>
			<%}%>
<div class="row" style="background-color:white;box-shadow: 0 3px 3px 1px #000000">
	<div class="col-md-12">&nbsp;</div>
	<!-- <div class="col-md-12"><a href="/BFAR-R7/CMSNavigation?pageAction=compose"><input class="btn btn-primary" type="submit" value="COMPOSE"></a></div> -->
	<div class="col-md-12">&nbsp;</div>
	<div class="col-md-12">
		<table class="table table-striped dataTables">
			<thead>
				<tr>
					<td>FROM</td>
					<td>SUBJECT</td>
					<td>DATE</td>
					<td></td>
					<%
						if (user.getUserType().equalsIgnoreCase("Administrator")) {
					%>
				<td>Delete</td>
				<%
						}
					%>
				</tr>
			</thead>
			<tbody>
			<%
				FeedbackDao feedbackDao = new FeedbackDao();
				List<Feedback>feedbacks = feedbackDao.getNewestFeedback();
			%>
			<%
				for (Feedback feedback : feedbacks) {
			%>
			
			<%if(!feedback.isMarkasRead()){%>
				<tr class="align-left" style="font-weight: bolder;">
					<td><%=feedback.getRecipientAddress()%></td>
					<td><%=feedback.getSubject() %></td>
					<td><%=feedback.getDateCreated() %></td>
					
					<td><a href="/BFAR-R7/FeedbackUpdateStatus?status=viewemail&contentid=<%=feedback.getId()%>"><input type="submit" class="btn btn-primary" value="Open"/></a></td>
					<%
						if (user.getUserType().equalsIgnoreCase("Administrator")) {
					%>
				<td class="text-center"><a href="/BFAR-R7/FeedbackUpdateStatus?contentid=<%=feedback.getId()%>&status=delete">
				<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
				<%}%>
				</tr>	
			<%}else{%>	
				<tr class="align-left">
					<td><%=feedback.getRecipientAddress()%></td>
					<td><%=feedback.getSubject() %></td>
					<td><%=feedback.getDateCreated() %></td>
					
					<td><a href="/BFAR-R7/FeedbackUpdateStatus?status=viewemail&contentid=<%=feedback.getId()%>"><input type="submit" class="btn btn-primary" value="Open"/></a></td>
					<%
						if (user.getUserType().equalsIgnoreCase("Administrator")) {
					%>
				<td class="text-center"><a href="/BFAR-R7/FeedbackUpdateStatus?contentid=<%=feedback.getId()%>&status=delete">
				<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
				<%}%>
				</tr>	
				<%}%>
							
				<%}%>	
			</tbody>
		</table>
	</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>