<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.FeedbackDao"%>
<%@page import="gov.bfar.cms.domain.Feedback"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<% String id = (String) request.getAttribute("id");
FeedbackDao feedbackDao = new FeedbackDao();
Feedback feedback = feedbackDao.findById(id);
%>
<div class="row" style="background-color:white;box-shadow: 0 3px 3px 1px #000000">
	<div class="col-md-12">&nbsp;</div>
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<h4>From</h4>
			</div>
			<div class="col-md-12">
				<label style="color:blue"><%=feedback.getRecipientAddress()%></label>
				
			</div>
			<div class="col-md-12">
				<h4>Subject</h4>
			</div>
			<div class="col-md-12">
				<label style="color:blue"><%=feedback.getSubject()%></label>
			</div>
			<div class="col-md-12">
				<h4>Date Sent</h4>
			</div>
			<div class="col-md-12">
				<label style="color:blue"><%=feedback.getDateCreated()%></label>
			</div>
			<div class="col-md-12">
				<h4>Message</h4>
				<br>
				<label style="color:blue"><%=feedback.getContent() %></label>
			</div>
		</div>
	</div>
			
	<form method="POST" action="/BFAR-R7/ReplayEmailInfo" onsubmit="uploading();">	
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<h4>Replay Message *</h4>
				</div>
				<div class="col-md-3">
					<label>To :</label>
				</div>
				<div class="col-md-9">
					<label><%=feedback.getRecipientAddress()%></label>
					<input type="hidden" value="<%=feedback.getRecipientAddress()%>" class="form-control" name="recipient"/>
				</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-3">
					<label>Subject :</label>
				</div>
				<div class="col-md-9">
					<textarea rows="2" name="subject" class="form-control"></textarea>
				</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-3">
					<label>Message</label>
				</div>
				<div class="col-md-12">
						<textarea id="tacontent" class="form-control" rows="6" name="message"></textarea>
				</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-12">
					<input type="hidden" name="id" value="<%=feedback.getId() %>"/>	
					<input type="submit" class="btn btn-primary" value="Send">
					<label>${sendingmessage}</label>
					<% session.removeAttribute("sendingmessage"); %>		
				</div>
			</div>
		</div>
	</form>
	<div class="col-md-12">&nbsp;</div>
	<div class="col-md-6">
		<div class="panel-group" id="accordion">
		   <%List<Feedback> fb = feedbackDao.recipientFeedbackList(feedback.getRecipientAddress(),id);
				for(Feedback fblist : fb){%>
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">
						   <a data-toggle="collapse" data-parent="#accordion" href="#<%=fblist.getId() %>"><%=fblist.getSubject()%>  (<i><%=fblist.getRecipientAddress()%></i>)</a>
						</h4>
					</div>
					<div id="<%=fblist.getId() %>" class="panel-collapse collapse">
						<div class="panel-body">
						   <%=fblist.getContent() %>
						</div>
					</div>
				</div>
			<%}%>
	   </div>
	</div>
	<div class="col-md-12">&nbsp;</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>