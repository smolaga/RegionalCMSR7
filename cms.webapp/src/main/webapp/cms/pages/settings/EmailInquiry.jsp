<%@page import="gov.bfar.cms.domain.EmailInquiry"%>
<%@page import="gov.bfar.cms.dao.EmailInquiryDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript">
	$(document).ready(function(){
		var upstat = ${upemin}
		var emailadd = "${email}"
		var pass= "${password}"
		var id= "${id}"
		if(upstat == false){
			$("#btnemi").val("Save");
		}
		else{
			$("#emailadd").val(emailadd);
			$("#password").val(pass);
			$("#id").val(id);
			$("#btnemi").val("Update");
		}
	});
</script>
<form method="POST" action="/BFAR-R7/EmailInquirySender" onsubmit="uploading();">
	<div class="row">
		<div class="col-md-12">
			<h4>Email Address:</h4>
		</div>
		<div class="col-md-6">
			<input type="hidden" id="id" name="id"/>
			<input type="text" name="email" id="emailadd" required class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="setCustomValidity('You have not given a correct e-mail address')"
						 onchange="try{setCustomValidity('')}catch(e){}"/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h4>Password:</h4>
		</div>
		<div class="col-md-6">
			<input type="password" name="password" id="password" required class="form-control"/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<br><input type="submit" id="btnemi" class="btn btn-primary" name="action" /><br><br>
			<label style="color:red">${insertmsg}</label>
		</div>
	</div>
</form>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>