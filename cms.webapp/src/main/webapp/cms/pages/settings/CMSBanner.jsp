<%@page import="java.io.File"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<script type='text/javascript'>
$(document).ready(function(){
	var upstat = "${upbmin}"
	var id= "${bid}"
	if(upstat == "false"){
		$("#btnb").val("Save");
	}
	else{		
		$("#bid").val(id);
		$("#btnb").val("Update");
	}
});
	function bannerImage() {
		pdffile = document.getElementById("uploadIMG").files[0];
		pdffile_url = URL.createObjectURL(pdffile);
		$('#viewer').attr('src', pdffile_url);
		$('#viewer').attr('width', '100%');
		$('#viewer').attr('height', '100%');
	}
	$(document).ready(function() {
		$(function() {
			$("#uploadIMG").change(function() {
				bannerImage();
				$("#imgchange").val("true");
			});
		});
	});
</script>
<div class="row">
	<div class="col-md-12"></div>
	<div class="col-md-12" style="margin-top:20px">
		<div class="headtitle">CMS Banner</div>
	</div>
	<div class="col-md-12">
		<br>
	</div>
	<form action="/BFAR-R7/InsertPictures?type=cmsbanner" method="POST" enctype="multipart/form-data" onsubmit="uploading();">
		<%
			Pictures imgCmsBanner = new Pictures();
			if (session.getAttribute("bid") != null) {
				PicturesDao imgDao = new PicturesDao();
				imgCmsBanner = imgDao.findById(session.getAttribute("bid").toString());
			} else {
				imgCmsBanner.setImageFilePath(null);
			}
		%>
		<div class="col-md-2">
			<label>Choose a file : </label>
		</div>
		<div class="col-md-3">
			<input id="uploadIMG" type="file" name="bfile" class="btn btn-primary"/>
		</div>
		<div class="col-md-12">
			<input type="hidden" name="imgchange" id="imgchange" value="false">
			<input type="hidden" id="bid" name="bid"/>
			<br><input type="submit" id="btnb" class="btn btn-primary" name="action" onclick="return insertPdf('uploadIMG')" /><br><br>
						<label style="color:red">${insertbannermsg}</label>
						<% session.removeAttribute("insertbannermsg"); %>
		</div>
		<div class="col-md-12">
			<br>
		</div>
		<div class="col-md-12" align="center">
			<%
				if (imgCmsBanner.getImageFilePath() == null) {
			%>
			<div style="width: 50%; height: 50%; border: 5px groove blue;">
				<img alt="PREVIEW IMAGE" class="img-responsive" id="viewer">
			</div>
			<%
				} else {
			%>
			<div style="width: 50%; height: 50%; border: 5px groove black;" class="bg-primary">
				<label>New</label> <img class="img-responsive" id="viewer">
			</div>
			<br>
			<div style="width: 50%; height: 50%; border: 5px groove black;"
				class="bg-primary">
				<label>Recent</label> <img class="img-responsive" src="<%=imgCmsBanner.getImageFilePath()%>">
			</div>
			<%
				}
			%>
		</div>
	</form>
	<div class="col-md-12">
		<br>
	</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<% session.removeAttribute("upbmin"); %>
<% session.removeAttribute("bid"); %>