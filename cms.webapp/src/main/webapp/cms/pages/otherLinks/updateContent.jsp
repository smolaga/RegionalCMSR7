<%@page import="gov.bfar.cms.domain.Otherlinks"%>
<%@page import="gov.bfar.cms.dao.OtherlinksDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

</head>
	<% String id = (String) request.getAttribute("getidcontent");
	OtherlinksDao otherlinksDao = new OtherlinksDao();
	Otherlinks otherlinks = otherlinksDao.findById(id);
	%>
<body>
<div class="row">
<div class="wrapper">
		<form method="POST" action="/BFAR-R7/UpdateOtherLink" onsubmit="uploading();">
			<div class="row">
				<div class="col-md-12">
					<h4>Title</h4>
				</div>
				<div class="col-md-12">
					<input type="text"  required name="title" class="form-control" value="<%=otherlinks.getTitle()%>"/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h4>Content</h4>
					<br>
					<textarea id="tacontent" class="mceEditor" style="height: 500px" name="content"><%=otherlinks.getContent() %></textarea>
				</div>
			</div>
			<input type="hidden" name="id" value="<%=otherlinks.getId() %>"/>
			<input type="text" name="status" value="<%=otherlinks.isStatus() %>"/>
			<input type="hidden" name="pagelink" value="<%=otherlinks.getPageLink() %>"/>
			<input type="hidden" name="createdby" value="<%=otherlinks.getCreatedBy() %>"/>
			<div class="row">
				<div class="col-md-12">
					<br><input type="submit" class="btn btn-primary" name="btnsavehleft" value="Save Content" />
				</div>
			</div>
		</form>
	</div>
	</div>