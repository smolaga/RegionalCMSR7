<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.OtherlinksDao"%>
<%@page import="gov.bfar.cms.domain.Otherlinks"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
%>

<div class="row" style="background-color:white; box-shadow: 0 3px 3px 1px #000000">
<div class="col-md-12" style="margin-bottom: 2%; margin-top: 20px"><div class="headtitle">Other Links</div>
<a href="/BFAR-R7/CMSNavigation?type=otherlinks&pageAction=insertContent"><input type="submit" class="btn btn-primary" value="Add HTML" ></a>
</div>
</div>

<div class="row" style="background-color:white; box-shadow: 0 3px 3px 1px #000000">
<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
 <p style="color:blue"><i>Note: Only One HTML Content should be display.</i></p>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<td>Title</td>
				<td>Date Created</td>
				<td>Status</td>
				<%if(user.isCanShow()){%>
				<td>View in Page</td>
				<%} %>
				<%if(user.isCanUpdate()){%>
				<td>Update</td>
				<%} %>
				<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
				<td>Delete</td>
				<%} %>
			</tr>
		</thead>
		<tbody>
			<%
			OtherlinksDao otherlinksDao = new OtherlinksDao();
				List<Otherlinks> otherlinks = otherlinksDao.otherlinksFindAllContent();
			%>
			<%
			
			for(Otherlinks otherlinks2 : otherlinks){
				
			%>
			<tr>
		
				<td><%=otherlinks2.getTitle()%></td>
				<td><%=otherlinks2.getDateCreated() %></td>
				<%if(otherlinks2.isStatus()){%>
				    <td class="text-center">Active</td>
						<%}else{%>
								<td class="text-center">Inactive</td>
						<%}%>
				<%if(user.isCanShow()){%>
				<%if(otherlinks2.isStatus()){%>
					<td class="text-center">	
						<a href="/BFAR-R7/OtherLinksUpdateStatus?contentid=<%=otherlinks2.getId()%>&status=hide"><abbr title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a>
					</td> 
					<%} else{%>
					<td class="text-center">	
						<a href="/BFAR-R7/OtherLinksUpdateStatus?contentid=<%=otherlinks2.getId()%>&status=show"><abbr title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a>
					</td>
					<%} %>
				<%} %>
				<%if(user.isCanUpdate()){%>
				<td class="text-center"><a href="/BFAR-R7/OtherLinksUpdateStatus?contentid=<%=otherlinks2.getId()%>&status=updateall"><input type="image" src="/BFAR-R7/cms/media/img/updateHTML.png"></a></td>
				<%} %>
				<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
				<td class="text-center"><a href="/BFAR-R7/OtherLinksUpdateStatus?contentid=<%=otherlinks2.getId()%>&status=delete">
					<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
				<%} %>
			</tr>
			<%
				}
			%>
			</tbody>
			
	</table>
	</div>
	</div>
	</div>
</div>
	
