<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.User"%>

<script type="text/javascript">
	function overlay() {
		el = document.getElementById("overlay");
		el.style.visibility = (el.style.visibility == "visible") ? "hidden"
				: "visible";
	}
</script>
<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
%>
<div class="row">

	<div class="col-md-12">
		<div class="headtitle">Homepage Left Title List</div>
	</div>
	<div class="col-md-12">
		<a href="/BFAR-R7/CMSNavigation?type=left&pageAction=insertcontent"><input type="submit" class="btn btn-primary" value="Add Title"></a> 
		<!-- <a href="/BFAR-R7/CMSNavigation?type=left&pageAction=insertpdfcontent"><input type="submit" class="btn btn-primary" value="Add Pdf"></a> -->
	</div>
	<div class="col-md-12">&nbsp;</div>
	</div>
<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
		<table class="table table-hover table-bordered dataTables">
			<thead>
				<tr>
					<td>Title</td>
					<td>Date Created</td>
					<td>Order</td>
					<td>Status</td>
				<%if(user.isCanShow()){%>
					<td>View in Page</td>
				<%} %>
					<td>Update Title/ Add Content</td>
					<td>Delete</td>
				</tr>
			</thead>
			<tbody>
				<%
				    HomepageDao homepageDao = new HomepageDao();
				    List<Homepage> homepages = homepageDao.titleList("left", "maintitle");
				%>
				<%
				    for (Homepage homepage : homepages) {
				%>
				<tr>
					<td><%=homepage.getHead()%></td>
					<td><%=homepage.getDateCreated()%></td>
					<td><%=homepage.getSequence()%></td>
					<%
					    if (homepage.getContentType().equalsIgnoreCase("pdf")) {
					%>
					<td class="text-center"><img src="/BFAR-R7/cms/media/img/updatePDF.png">
					</td>
					<%
					    } else if (homepage.getContentType().equalsIgnoreCase("html")) {
					%>
					<td class="text-center"><img src="/BFAR-R7/cms/media/img/updateHTML.png">
					</td>
					<%
					    }
					%>
					<%if(homepage.isStatus()){%>
				    <td class="text-center">Active</td>
				<%}else{%>
					<td class="text-center">Inactive</td>
				<%}%>
			<%if(user.isCanShow()){%>					<%
					    if (homepage.isStatus()) {
					%>
					<td class="text-center"><a
						href="/BFAR-R7/HomepageContentUpdate?id=<%=homepage.getId()%>&status=hide&activeat=title&columnType=left"><abbr
							title="Hide"><button onclick='overlay()'
									class="btn btn-warning">Hide&nbsp;</button></abbr></a></td>
					<%
					    } else {
					%>
					<td class="text-center"><a
						href="/BFAR-R7/HomepageContentUpdate?id=<%=homepage.getId()%>&status=show&activeat=title&columnType=left"><abbr
							title="Show"><button onclick='overlay()'
									class="btn btn-primary">Show</button></abbr></a></td>
					<%
					    }
					%>
			<%} %>
					<td class="text-center"><a
						href="/BFAR-R7/HomepageContentUpdate?id=<%=homepage.getId()%>&status=updateall&columnType=left&contentType=<%=homepage.getContentType()%>">
						<input type="submit" class="btn btn-primary" value="Open"></a></td>
						
						<td class="text-center"><a href="/BFAR-R7/HomepageContentUpdate?id=<%=homepage.getId()%>&status=delete&columnType=left">
							<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
				</tr>
				<%
				    }
				%>
			</tbody>

		</table>
		</div>
	</div>
	</div>


