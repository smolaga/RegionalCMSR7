<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	String col = request.getAttribute("columntype").toString();
	HomepageDao dao = new HomepageDao();
	int seq = dao.getSuggestedSequence("maintitle", "center");
%>
<div class="panel panel-default">
	<!-- <div class="panel-heading">
		
	</div> -->
	<div class="row" style="background-color:white; box-shadow: 0 3px 3px 1px #000000">
	<div class="panel-body">
		<div class="row">
			<form method="POST" action="/BFAR-R7/HomepageCenterContent" onsubmit="uploading();">
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-2">
					<label>Main Title:</label>
				</div>
				<div class="col-md-7">
					<input type="text" name="title" required class="form-control" value="" placeholder="Title" /> 
				</div>
				<div class="col-md-1">
					<label>Order:</label>
				</div>
				<div class="col-md-2">
					<input type="text" name="sequence" class="form-control" placeholder="No." id="sequence" value="<%=seq%>"
					aria-describedby="name-format" aria-required="true" pattern="[0-9]+" required oninvalid="setCustomValidity('Please Enter Number Only')"
					 onchange="try{setCustomValidity('')}catch(e){}"/>
				</div>
				<input type="hidden" name="columnType" class="form-control" value="<%=col%>" />
				<div class="col-md-1">
					<input type="hidden" name="action" value="addtitle">
					<input type="submit" class="btn btn-primary" name="" id="addTitle" value="Add Title" >
				</div>
			</form>
				<div class="col-md-1">
					<input type="submit" class="btn btn-primary" onclick="goBack()"  value="Cancel" />
				</div>
		</div>
	</div>
</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>