<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
  
<div class="row">
<div class="wrapper">			
				<div class="row">
			<form method="POST" action="/BFAR-R7/HomepageCenterSubContent?action=insertsubpdf" enctype="multipart/form-data" onsubmit="uploading();">
					<div class="col-md-2">
						<label>Content Title</label>
					</div>
					<div class="col-md-4">
						<input type="text" required name="pdfsubcontenttitle" class="form-control" />
					</div>
					<div class="col-md-12">
						<label>Choose a file : </label>
					</div>
					<div class="col-md-12">
						<input id="uploadPDF" type="file" name="pdffile" onchange="PreviewImage(this.id,'viewer');" class="btn btn-primary" />
						<input type="hidden" name="columnType" value="center" />
					</div>
					<div class="col-md-12">
						<iframe id="viewer" frameborder="0" scrolling="no"></iframe>
					</div>
					<div class="col-md-12">&nbsp;
					</div>
					<div class="col-md-2" align="left">
						<input type="hidden" name="action" value="insertsubpdf">
						<input type="submit" value="Submit PDF" name="categoryaction" onclick="return insertPdf('uploadPDF')" class="btn btn-primary">
					</div>
			</form>
					<div class="col-md-1">
						<input type="submit" class="btn btn-primary" onclick="goBack()"  value="Cancel" />
					</div>
		</div>
	</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>