<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%
	HomepageDao dao = new HomepageDao();
	Homepage title = dao.findById(request.getAttribute("tid").toString());
	Homepage pdfmaincontent = dao.findById(request.getAttribute("pdfmcid").toString());
	if(request.getAttribute("changecontenttype")!=null){
	    pdfmaincontent.setContentType("pdf");
	}
%>
<div class="row">
	<div class="col-md-12 text-right"><i><b><a href="/BFAR-R7/HomepageContentUpdate?id=<%=pdfmaincontent.getId()%>&status=updateall&columnType=center&cctype=ccthtml">change content type to html?</a></b></i></div>
	<div class="col-md-12">&nbsp;</div>
	<form method="POST" action="/BFAR-R7/HomepageCenterContent?action=updatepdf&sequence=<%=title.getSequence()%>" enctype="multipart/form-data" onsubmit="uploading();">
		<div class="col-md-2">
			<label>Content Title</label>
		</div>
		<div class="col-md-4">
			<input type="text" name="pdfcontenttitle" class="form-control" value="<%=pdfmaincontent.getHead()%>"/>
			<input type="hidden" name="pdfcontentid" class="form-control" value="<%=pdfmaincontent.getId()%>"/>
			<input type="hidden" name="pdfpagelink" class="form-control" value="<%=pdfmaincontent.getPageLink()%>"/>
			<input type="hidden" name="pdfcolumntype" class="form-control" value="<%=pdfmaincontent.getColumnType()%>"/>
			<input type="hidden" name="pdfcontenttype" class="form-control" value="<%=pdfmaincontent.getContentType()%>"/>
			<input type="hidden" name="pdffilepath" class="form-control" value="<%=pdfmaincontent.getPdfFilePath()%>"/>
			<input type="hidden" name="pdfstatus" class="form-control" value="<%=pdfmaincontent.isStatus()%>"/>
			<input type="hidden" name="pdfviewonpage" class="form-control" value="<%=pdfmaincontent.isViewonpage()%>"/>
			<input type="hidden" name="pdfviewonlist" class="form-control" value="<%=pdfmaincontent.isViewonlist()%>"/>
			<input type="hidden" name="pdffileupdatestatus" id="pdffileupdatestatus" class="form-control" value="false"/>
		</div>
		<div class="col-md-12">
			<label>Short Content</label>
		</div>
		<div class="col-md-3">
			<textarea id="uppdfshortcontent" style="height: 300px;" class="mceEditor" name="pdfshortcontent"><%=pdfmaincontent.getShortContent()%></textarea>
		</div>
		<div class="col-md-12">&nbsp;</div>
		<div class="col-md-2">
			<label>Choose a file : </label>
		</div>
		<div class="col-md-10">
			<input id="uploadPDFUpdate" type="file" name="pdffile" onchange="PreviewImage(this.id,'viewerup')" class="btn btn-primary"/>
		</div>
		<div class="col-md-12" id="dvpdf">
			<%if(pdfmaincontent.getPdfFilePath() != null){ %>
			<iframe id="viewerUpdate" title="PDF in an i-Frame" src="<%=pdfmaincontent.getPdfFilePath() %>" frameborder="0" scrolling="no" height="1100" width="100%" ></iframe>
			<%} %>
		</div>
		 <div class="col-md-12" id="dvpdfupdate">
			<iframe id="viewerup" frameborder="0" scrolling="no"></iframe>
		</div> 
		<div class="col-md-12">&nbsp;
			<input type="hidden" name="titleid" value="<%=title.getId()%>" />
			<input type="hidden" name="title" value="<%=title.getHead()%>" />
			<input type="hidden" name="sequence" value="<%=title.getSequence()%>" />
			<input type="hidden" name="columnType" value="<%=title.getColumnType()%>" />
		</div>
		<div class="col-md-2" align="left">
			<input type="hidden" name="action" value="insertpdf">
			<input type="submit" value="Submit PDF" name="categoryaction" class="btn btn-primary">
		</div>
	</form>
		<div class="col-md-1">
			<input type="submit" class="btn btn-primary" onclick="goBack()"  value="Cancel" />
		</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>