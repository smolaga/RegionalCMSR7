<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<script type="text/javascript">
function bannerImage() {
	pdffile = document.getElementById("imgfileupload").files[0];
	pdffile_url = URL.createObjectURL(pdffile);
	$('#viewer').attr('src', pdffile_url);
	$('#viewer').attr('width', '100%');
	$('#viewer').attr('height', '100%');
}
$(document).ready(function(){
	$("#addTitle").click(function(){
		if ($('#imgfileupload').get(0).files.length === 0) {
			alert("No files selected.");
			return false;
		}
	});
	$(function() {
		$("#imgfileupload").change(function() {
			bannerImage();
		});
	});
});
</script>
<%
	PicturesDao dao = new PicturesDao();
	int seq = dao.getSuggestedSequence("gallery", "albumtitle");
%>
<div class="row" style="background-color:white; box-shadow: 0 3px 3px 1px #000000">
	<div class="panel-body">
		<div class="row">
			<form method="POST" action="/BFAR-R7/GalleryPictures?action=addtitle" enctype="multipart/form-data" onsubmit="uploading();">
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-2">
					<label>Gallery Title:</label>
				</div>
				<div class="col-md-12">
					<input type="text" name="gallerytitle" required class="form-control" value="" placeholder="Title" /> 
				</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-12">
					<label>Order:</label>
				</div>
				<div class="col-md-3">
					<input type="text" name="gallerysequence" class="form-control" placeholder="No." id="sequence" value="<%=seq%>"
					aria-describedby="name-format" aria-required="true" pattern="[0-9]+" required oninvalid="setCustomValidity('Please Enter Number Only')"
					 onchange="try{setCustomValidity('')}catch(e){}"/>
				</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-12">
					<label>Choose a file : </label>							
				</div>
				<div class="col-md-12">
					<input id="imgfileupload" name="imgupload" type="file" onchange="PreviewImage(this.id,'viewer');" class="btn btn-primary"/>
				</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-12" align="center">
					<div style="width: 50%; height: 50%; border: 5px groove black;">
						<img alt="PREVIEW IMAGE" class="img-responsive" id="viewer">
					</div>
				</div>
				<div class="col-md-12">&nbsp;</div>
				<input type="hidden" name="gallerytype" class="form-control" value="gallery" />
				<input type="hidden" name="gallerycontenttype" class="form-control" value="albumtitle" />
				<div class="col-md-1">
					<input type="submit" class="btn btn-primary" name="" id="addTitle" value="Add Title" >
				</div>
			</form>
				<div class="col-md-1">
					<input type="submit" class="btn btn-primary" onclick="goBack()"  value="Cancel" />
				</div>
		</div>
	</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>