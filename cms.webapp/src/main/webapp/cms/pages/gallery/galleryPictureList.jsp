<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%User user = new User();
            user = (User)session.getAttribute("userinfo");%>
            <%if((User) session.getAttribute("userinfo") == null){%>
			<jsp:forward page="/cms/pages/login/login.jsp"></jsp:forward>
			<%}%>
<%
	PicturesDao dao = new PicturesDao();
	Pictures title = dao.findById(request.getAttribute("tid").toString());
	String tid = request.getAttribute("tid").toString();
	System.out.println("TITLE ID: " + title.getId());
%>
<script type='text/javascript'>
	function bannerImage() {
		pdffile = document.getElementById("imgfileupload").files[0];
		pdffile_url = URL.createObjectURL(pdffile);
		$('#viewer').attr('src', pdffile_url);
		$('#viewer').attr('width', '100%');
		$('#viewer').attr('height', '100%');
	}
	function titleImage() {
		pdffile = document.getElementById("imgtitlefileupload").files[0];
		pdffile_url = URL.createObjectURL(pdffile);
		$('#imgtitleviewer').attr('src', pdffile_url);
		$('#imgtitleviewer').attr('width', '100%');
		$('#imgtitleviewer').attr('height', '100%');
	}
	$(document).ready(function() {
		if ($('#imagesequence').val() == 1 && $('#insertimg').val() == 'Add Image'){
			$('#imagesequence').prop('readOnly','readOnly');
		}
		$("#insertimg").click(function(){
			if ($('#imgfileupload').get(0).files.length === 0) {
				alert("No files selected.");
				return false;
			}
		});
		$(function() {
			$("#imgfileupload").change(function() {
				bannerImage();
				$("#imgchange").val("true");
			});
			$("#imgtitlefileupload").change(function() {
				titleImage();
				$("#imgtitlechange").val("true");
			});
		});
		 var upalimg = '${updalbumimg}'
		 if(upalimg == 'true'){
			 $("#dvinalbumimg").hide();
			 $("#dvupalbumimg").show();
		 }else{
			 $("#dvinalbumimg").show();
			 $("#dvupalbumimg").hide();
		 }
	});
	$(function() {
		
		var imgseq = $("#imagesequence").val();
		if (imgseq == 0) {
			$("#imagesequence").val("");
		}
	});
</script>

<div class="row"
	style="background-color: white; box-shadow: 0 3px 3px 1px #000000">
	<div class="panel-body">
		<div class="row">
			<form method="POST" action="/BFAR-R7/GalleryPictures?action=updatetitle" enctype="multipart/form-data"  onsubmit="uploading();">
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-2">
					<label>Gallery Title:</label>
				</div>
				<div class="col-md-12">
					<input type="text" name="gallerytitle" required class="form-control"
						placeholder="Title" required value="<%=title.getTitle()%>" />
				</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-12">
					<label>Order:</label>
				</div>
				<div class="col-md-3">
					<input type="hidden" name="titleoldsequence" value="<%=title.getSequence()%>" /> 
					<input type="text" name="gallerysequence" class="form-control"
						placeholder="No." value="<%=title.getSequence()%>" 
						aria-describedby="name-format" aria-required="true" pattern="['1-9]+" 
						required oninvalid="setCustomValidity('Please Enter Number except 0')"
						 onchange="try{setCustomValidity('')}catch(e){}"/>
				</div>
				<div class="col-md-2">
					<label>Date Events:</label>
				</div>
				<div class="col-md-2">
					<input type="date" name="gallerytitledateevent" class="form-control datepicker" placeholder="Date." value="<%=title.getDateEvent()%>"/>
				</div>
				<div class="col-md-12">&nbsp;</div>
					<div class="col-md-4">
						<input id="imgtitlefileupload" name="imgupload" type="file" class="btn btn-primary"/>
					</div>
				<div class="col-md-12">&nbsp;</div>
				<div class="col-md-12" align="center">
					<%
						if (title.getImageFilePath().equalsIgnoreCase("")) {
					%>
					<div style="width: 50%; height: 50%; border: 5px groove black;">
						<img alt="PREVIEW IMAGE" class="img-responsive" id="imgtitleviewer">
					</div>
					<%
						} else {
					%>
					<div style="width: 50%; height: 50%; border: 5px groove black;"
						class="bg-primary">
						<label>New</label> <img class="img-responsive" id="imgtitleviewer">
					</div>
					<br>
					<div style="width: 50%; height: 50%; border: 5px groove black;"
						class="bg-primary">
						<label>Recent</label> <img class="img-responsive"
							src="<%=title.getImageFilePath()%>">
					</div>
					<%
						}
					%>
				</div>
				<div class="col-md-12">&nbsp;</div>
				<input type="hidden" name="gallerytitleid" class="form-control"
					value="<%=title.getId()%>" /> <input type="hidden"
					name="gallerytype" class="form-control"
					value="<%=title.getType()%>" /> <input type="hidden"
					name="gallerycontenttype" class="form-control"
					value="<%=title.getContentType()%>" /> <input type="hidden"
					name="gallerypagelink" class="form-control"
					value="<%=title.getPageLink()%>" /> <input type="hidden"
					name="gallerystatus" class="form-control"
					value="<%=title.isStatus()%>" />
				<div class="col-md-1">
					<input type="hidden" name="imgtitlechange" id="imgtitlechange" value="false"> 
					<input type="submit" class="btn btn-primary" name="" id="addTitle" value="Update">
				</div>
			</form>
			<div class="col-md-1">
				<input type="submit" class="btn btn-primary" onclick="goBack()"
					value="Cancel" />
			</div>
			<div class="col-md-12">&nbsp;</div>
		</div>
		<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
				<table class="table table-hover table-bordered dataTables">
					<thead>
						<tr>
							<td>Image Title</td>
							<td>Date Event</td>
							<td>Image</td>
							<td>Date Created</td>
							<td>Order</td>
							<td>Status</td>
							<%if(user.isCanUpdate()){%>
							<td>Update</td><%}%>
						<% if (user.getUserType().equalsIgnoreCase("Administrator"))
							{
						%>
							<td>View in Page</td>
							<td>Delete</td>
							<%} %>
							
						</tr>
					</thead>
					<tbody>
						<%
							PicturesDao picdao = new PicturesDao();
							List<Pictures> pictures = picdao.galleryImageListAll(tid);
							for (Pictures pictures2 : pictures) {
						%>
						<tr>
							<td><%=pictures2.getTitle()%></td>
							<td><%=pictures2.getDateEvent()%></td>
							<td style="width: 20%; height: 50%;"><img
								class="img-responsive" width="100%" style="height: 200px;"
								src="<%=pictures2.getImageFilePath()%>"></td>
							<td><%=pictures2.getDateCreated()%></td>
							<td class="text-center"><%=pictures2.getSequence()%></td>
							<%if(pictures2.isStatus()){%>
							    <td class="text-center">Active</td>
							<%}else{%>
								<td class="text-center">Inactive</td>
							<%}%>
							<%if(user.isCanUpdate()){%>
								<td class="text-center"><a
								href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures2.getId()%>&titleid=<%=tid%>&status=updateall&type=gallery"><input
									type="submit" class="btn btn-primary" value="Update"></a></td>
							<%}%>
						<% if (user.getUserType().equalsIgnoreCase("Administrator"))
							{
						%>
							<%
								if (pictures2.isStatus()) {
							%>
							<td class="text-center"><a
								href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures2.getId()%>&status=hide&type=gallery"><abbr
									title="Hide"><button onclick='overlay()'
											class="btn btn-warning">Hide&nbsp;</button></abbr></a></td>
							<%
								} else {
							%>
							<td class="text-center"><a
								href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures2.getId()%>&status=show&type=gallery"><abbr
									title="Show"><button onclick='overlay()'
											class="btn btn-primary">Show</button></abbr></a></td>
							<%
								}
							%>
							<td class="text-center"><a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures2.getId()%>&status=delete&type=gallery">
							<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
						<%} %>
						
						</tr>
						<%
							}
						%>
					</tbody>
				</table>
			</div>
			</div>
			
			<div class="col-md-12">&nbsp;</div>
			<div class="col-md-12">&nbsp;</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<label>Album Pictures</label>
			</div>
			<div class="panel-body">
				<div class="row" id="dvinalbumimg">
					<form method="POST" action="/BFAR-R7/GalleryPictures?action=insertalbumimage&tid=<%=tid%>" enctype="multipart/form-data"  onsubmit="uploading();">
						<div class="col-md-12">
							<label>Choose a file for Album Images : </label>
						</div>
						<div class="col-md-4">
							<input id="albumimgfileupload" name="albumimgfileupload" class="btn btn-primary" type="file" multiple="multiple"/>
						</div>
						<div class="col-md-3">
							<input type="submit" class="btn btn-info" name="btnclear" style="display:none;" id="btnclear" value="CLEAR">
						</div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-12"><output id="result" ></output></div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-12">
							<input type="submit" class="btn btn-primary" name="btnaddimg" id="btnaddimg" value="UPLOAD IMAGE">
						</div>
					</form>
				</div>
				<div class="row" id="dvupalbumimg">
					<form method="POST"
						action="/BFAR-R7/GalleryPictures?action=updatealbumimage"
						enctype="multipart/form-data"  onsubmit="uploading();">
						<%
							String imgUpId = "";
							String action = "";
							String btnimage = "";
							Pictures imgUpdate = new Pictures();
							if (request.getAttribute("imgid") != null) {
								PicturesDao imageDao = new PicturesDao();
								imgUpdate = imageDao.findById(request.getAttribute("imgid").toString());
								action = "updateimage";
								btnimage = "Update Image";
							} else {
								int checkseq = dao.firstSequenceDeterminerGallery("gallery","albumtitle", title.getId());
								int seq = 0;
								if(checkseq == 0 ){
									seq=checkseq+1;
								}else{
									seq = dao.getSuggestedSequenceGallery("gallery", "albumimage", title.getId());
								}
								imgUpdate.setId("");
								imgUpdate.setTitle("");
								imgUpdate.setSequence(seq);
								imgUpdate.setDateEvent("");
								imgUpdate.setImageDescription("");
								imgUpdate.setImageFilePath("");
								imgUpdate.deactive();
								action = "insertimg";
								btnimage = "Add Image";
							}
						%>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-2">
							<label>Image Title:</label>
						</div>
						<div class="col-md-4">
							<input type="text" name="imagetitle" class="form-control"
								placeholder="Title" value="<%=imgUpdate.getTitle()%>" />
						</div>
						<div class="col-md-1">
							<label>Order:</label>
						</div>
						<div class="col-md-3">
							<input type="hidden" name="galleryoldsequence" value="<%=imgUpdate.getSequence()%>" /> 
							<input type="text" name="imagesequence" id="imagesequence" required
								class="form-control" placeholder="No."
								value="<%=imgUpdate.getSequence()%>" aria-required="true" pattern="[0-9]+" 
								required oninvalid="setCustomValidity('Please Enter Number Only')"
						 onchange="try{setCustomValidity('')}catch(e){}"/>
						 
						 
						</div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-2">
							<label>Date Events:</label>
						</div>
						<div class="col-md-2">
							<input type="date" name="imagedateevent"
								class="form-control datepicker" placeholder="Date."
								value="<%=imgUpdate.getDateEvent()%>" />
						</div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-2"><label>Image Description:</label></div>
						<div class="col-md-8">
							<textarea id="tacontent" class="mceEditor" style="height: 200px;" name="imgdescription"><%=imgUpdate.getImageDescription()%></textarea>
						</div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-2"><label>Choose File: </label></div>
						<div class="col-md-4">
							<input id="imgfileupload" name="imgupload" type="file" class="btn btn-primary"/>
							<textarea id="imgcontaner" name="image" class="hide"><%=imgUpdate.getImage()%></textarea>
						</div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-12" align="center">
							<%
								if (imgUpdate.getImageFilePath().equalsIgnoreCase("")) {
							%>
							<div style="width: 50%; height: 50%; border: 5px groove black;">
								<img alt="PREVIEW IMAGE" class="img-responsive" id="viewer">
							</div>
							<%
								} else {
							%>
							<div style="width: 50%; height: 50%; border: 5px groove black;"
								class="bg-primary">
								<label>New</label> <img class="img-responsive" id="viewer">
							</div>
							<br>
							<div style="width: 50%; height: 50%; border: 5px groove black;"
								class="bg-primary">
								<label>Recent</label> <img class="img-responsive"
									src="<%=imgUpdate.getImageFilePath()%>">
							</div>
							<%
								}
							%>
						</div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-12">
							<input type="hidden" name="imageid" value="<%=imgUpdate.getId()%>" /> 
							<input type="hidden" name="gallerytitleid" value="<%=title.getId()%>" /> 
							<input type="hidden" name="imagecontenttype" value="albumimage">
							<input type="hidden" name="imagetype" value="gallery"> 
							<input type="hidden" name="pagelink" value="<%=title.getPageLink()%>" />
							<input type="hidden" name="imagestatus"	value="<%=imgUpdate.isStatus()%>"> 
							<input type="hidden" name="action" value="<%=action%>"> 
							<input type="hidden" name="imgchange" id="imgchange" value="false"> 
							<input type="submit" class="btn btn-primary" name="<%=action%>"	id="<%=action%>" value="<%=btnimage%>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>