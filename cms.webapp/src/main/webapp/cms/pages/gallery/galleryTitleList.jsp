<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>
<%@page import="gov.bfar.cms.domain.User"%>
<%
    User user = new User();
    user= (User) session.getAttribute("userinfo");
    
    if((User) session.getAttribute("userinfo") == null){
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/cms/login.jsp");
        dispatcher.forward(request, response);
    }
%>

<div class="row" style="background-color:white;  box-shadow: 0 3px 3px 1px #000000">
	<div class="col-md-12">
		
				<div class="row">
					<div class="col-md-12" style="margin-top:20px"><div class="headtitle">Gallery</div></div>
					<div class="col-md-12">
					<a href="/BFAR-R7/CMSNavigation?type=gallery&pageAction=insertContent"><input type="submit" class="btn btn-primary" value="Add Image Title" ></a>
					</div>
					<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
						<table class="table table-hover table-bordered dataTables ">
							<thead>
								<tr>
									<td>Title</td>
									<td>Date Created</td>
									<td>Order</td>
									<td>Status</td>
									<%if(user.isCanShow()){%>
									<td>View in Page</td>
									<%} %>
									<td>Update Title/ Add Image</td>
									<td>Copy Link</td>
									<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
									<td>Delete</td>
									<%} %>
								</tr>
							</thead>
							<tbody>
								<% try{
									PicturesDao dao = new PicturesDao();
									List<Pictures> pList = dao.galleryTitleList();
									for(Pictures pictures : pList){ %>
								<tr>
									<td><%=pictures.getTitle() %></td>
									<td><%=pictures.getDateCreated() %></td>
									<td class="text-center"><%=pictures.getSequence() %></td>
											<%if(pictures.isStatus()){%>
									    <td class="text-center">Active</td>
									<%}else{%>
										<td class="text-center">Inactive</td>
									<%}%>
							<%if(user.isCanShow()){%>
									<%if(pictures.isStatus()){%>
									<td class="text-center">	
										<a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures.getId()%>&status=hide&type=gallery"><abbr title="Hide"><button onclick='overlay()' class="btn btn-warning">Hide&nbsp;</button></abbr></a>
									</td> 
									<%} else{%>
									<td class="text-center">	
										<a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures.getId()%>&status=show&type=gallery"><abbr title="Show"><button onclick='overlay()' class="btn btn-primary">Show</button></abbr></a>
									</td>
									<%} %>
							<%} %>
									<td class="text-center"><a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures.getId()%>&status=updateall&type=gallery"><input type="submit" class="btn btn-primary" value="Open"></a></td>
									<td class="text-center"><button class="btn btn-primary btnCopy" data-toggle="popover" data-content="Copied!" data-clipboard-text="<%=pictures.getPageLink()%>">Copy</button></td>
									<%if (user.getUserType().equalsIgnoreCase("Administrator")) {%>
									<td class="text-center"><a href="/BFAR-R7/UpdatePictureStatus?contentid=<%=pictures.getId()%>&status=delete&type=gallery">
									<input type="submit" class="btn btn-danger" onclick="return confirm('do you want to delete?');" value="X"></a></td>
									<%} %>
								</tr>
								<%}}catch(Exception e){
									   e.getMessage();
								   }%>
							</tbody>
						</table>
						</div>
						</div>						
					</div>
				</div>
			
	</div>
		<div class="col-md-12">&nbsp;</div>
	</div>
<button type="button" class="btn hide" id="btnupload" data-toggle="modal"  data-target="#myModal">Uploading</button>
<div class="modal modal-transparent fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="">
					<div class="modal-body" align="center">
						<img id="loader" src="${pageContext.request.contextPath}/cms/media/img/loading gif/loading.gif" style="display: none;" />
						<label><b>Please Wait!</b></label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>