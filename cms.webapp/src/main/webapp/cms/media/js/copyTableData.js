var clipboard = new Clipboard('.clipboard', {
	// The selection of the correct content is done here.
	text : function(trigger) {
		return trigger.innerHTML;
	}
//clipboard.js will take the entire inner content of the clicked cell.
});

// Re-implement the cosmetic highlighting of the cell content. 
//This is actually done AFTER the content is copied to clipboard, 
//but it gives a visual indication that something happened.
clipboard.on('success', function(e) {
	var range = document.createRange();

	range.selectNodeContents(e.trigger);
	window.getSelection().addRange(range);
});
