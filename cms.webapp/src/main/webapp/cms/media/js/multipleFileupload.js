window.onload = function(){
        
    //Check File API support
	$("#albumimgfileupload").click(function(){
		$("#albumimgfileupload").val("");	
		$('.albumpicthumbnail, #previmg').parent().remove();
        $('result').hide();
        $("#btnclear").hide();
        
		});
	$("#btnclear").click(function(e){
		e.preventDefault();
		$('.albumpicthumbnail, #previmg').parent().remove();
        $('#result').hide();
        $('#albumimgfileupload').val("");
        $(this).hide();
	});
    if(window.File && window.FileList && window.FileReader)
    {
        var filesInput = document.getElementById("albumimgfileupload");
        filesInput.addEventListener("change", function(event){   
        	$("#btnclear").show();
        	$('#result').show();
            var files = event.target.files; //FileList object
            var output = document.getElementById("result");
            var fileCount = 0;
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];
                
                //Only pics
                if(!file.type.match('image'))
                  continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener("load",function(event){
                    
                    var picFile = event.target;
                    
                    var div = document.createElement("div");
                    
                    div.innerHTML = "<div id='previmg' class='col-sm-6 col-md-3'>" +
                    		"<a href='#'><div class='thumbnail'>" +
                    		"<img class='albumpicthumbnail' style='height: 200px; width: 100%; display: block;' src='" + picFile.result + "'" +
                            "title='" + picFile.name + "'/>" +
                            		"</div></a> " +
                            		"<textarea type='text' class='form-control' name='albumTitleNo"+fileCount+"' placeholder='Title'></textarea> <br>" +
                            		"<input type='date' class='form-control' name='albumEventDate"+fileCount+"' placeholder='Date'/>" +
                            		"</div>";
                    
                    output.insertBefore(div,null);  
                    ++fileCount;
                
                });
                
                 //Read the image
                picReader.readAsDataURL(file);
            }                               
           
        });
    }
    else
    {
        console.log("Your browser does not support File API");
    }
}
    