tinymce.init({
			selector : '.mceEditor',
			plugins : [ "searchreplace visualblocks code fullscreen image paste",
					"advlist autolink lists link charmap print preview anchor",
					"insertdatetime  table contextmenu" ],
			toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
			table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol",
			autosave_ask_before_unload : false,
			/*max_height : 500,
			min_height : 560,
			height : 10,*/
			paste_data_images: true,
			paste_insert_word_content_callback : "convertWord",
			paste_auto_cleanup_on_paste : true,
		});

function convertWord(type, content) {
	switch (type) {
	// Gets executed before the built in logic performs it's cleanups
	case "before":
		// content = content.toLowerCase(); // Some dummy logic
		// alert(content);
		break;
	// Gets executed after the built in logic performs it's cleanups
	case "after":
		// alert(content);
		content = content.replace(/<!(?:--[\s\S]*?--\s*)?>\s*/g, '');
		// content = content.toLowerCase(); // Some dummy logic
		// alert(content);
		break;
	}
	return content;
}
