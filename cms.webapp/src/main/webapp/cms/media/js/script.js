$(document).ready(function() {
	
	/*$('[data-toggle="popover"]').popover();*/
	// for navigation
	$('#cssmenu1 > ul > li > a').click(function() {
		$('#cssmenu1 li').removeClass('active');
		$(this).closest('li').addClass('active');
		var checkElement = $(this).next();
		if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			$(this).closest('li').removeClass('active');
			checkElement.slideUp('normal');
		}
		if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$('#cssmenu ul ul:visible').slideUp('normal');
			checkElement.slideDown('normal');
		}
		if ($(this).closest('li').find('ul').children().length == 0) {
			return true;
		} else {
			return false;
		}
	});
	// for navigation

	// for tabs
	$('#myTab').tabs();

	// for tabs

	// for modal pop up
	/*
	 * <button id="" class="btnmodalopen">Add HTML Content</button> <div
	 * class="modalpopup" title="Add Content"> <button id=""
	 * class="btnmodalclose">Close</button> </div>
	 */
	var clipboard = new Clipboard('.btnCopy');

	clipboard.on('success', function(e) {
		console.log(e);
	});

	clipboard.on('error', function(e) {
		console.log(e);
	});

	$(".modalpopup").addClass("hide");
	$(".btnmodalopen").button().on("click", function() {
		$(".modalpopup").dialog();
		$(".modalpopup").removeClass("hide");
	});

	$(".btnmodalclose").button().on("click", function() {
		$(".modalpopup").dialog("close");
	});
	// for modal pop up

	
});

function PreviewImage(id, viewer) {
	pdffile = document.getElementById(id).files[0];
	pdffile_url = URL.createObjectURL(pdffile);
	$('#' + viewer).attr('src', pdffile_url);
	$('#' + viewer).attr('width', '100%');
	$('#' + viewer).attr('height', '800px');
	$('#dvpdf').addClass('hide');
	$('#dvpdfupdate').removeClass('hide');
	$('#pdffileupdatestatus').val('true');

}
function insertPdf(id) {
	if ($('#' + id).get(0).files.length === 0) {
		alert("No files selected.");
		return false;
	}
}

function goBack() {
	window.history.back();
}

function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden"
			: "visible";
}
function uploading() { 
	$('#btnupload').click();
    document.getElementById('loader').style.display='block';
}