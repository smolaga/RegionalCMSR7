$(document).ready(function(){
$('.dataTables').DataTable({
	 "ordering": false
});


$('.dataTablesWithExportButtons').DataTable( {
	dom: 'Bfrtip',
	"ordering": false,
	buttons: [
	    'copyHtml5',
	    'excelHtml5',
	    'csvHtml5',
	    'pdfHtml5',
	    'print'
	    ]
} );
} );
