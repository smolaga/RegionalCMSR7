package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class Feedback extends BaseDomain implements Domain{
    private String recipientAddress;
    private String subject;
    private  String content;
    private boolean markasRead;
    
    
    public Feedback(){
    
    }


	public String getRecipientAddress() {
		return recipientAddress;
	}


	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public boolean isMarkasRead() {
		return markasRead;
	}


	public void setMarkasRead(boolean markasRead) {
		this.markasRead = markasRead;
	}


}
