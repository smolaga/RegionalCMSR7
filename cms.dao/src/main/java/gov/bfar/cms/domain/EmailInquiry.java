package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class EmailInquiry extends BaseDomain implements Domain{
    
    private String emailaddress;
    private String password;
    
    public EmailInquiry() {
        
    }

    

    public String getEmailaddress() {
        return emailaddress;
    }



    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public EmailInquiry(String emailaddress, String password) {
        super();
        this.emailaddress = emailaddress;
        this.password = password;
    }



    @Override
    public String toString() {
        return "EmailInquiry [emailaddress=" + emailaddress + ", password=" + password + "]";
    }



    public EmailInquiry(long tid, String id, String dateCreated, String createdBy, String modifiedBy, String modifiedDate, boolean status) {
        super(tid, id, dateCreated, createdBy, modifiedBy, modifiedDate, status);
        // TODO Auto-generated constructor stub
    }
    

    

}
