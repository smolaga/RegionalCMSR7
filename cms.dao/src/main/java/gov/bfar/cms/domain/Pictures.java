package gov.bfar.cms.domain;



import gov.bfar.cms.domain.intefaces.Domain;

public class Pictures extends BaseDomain implements Domain{

	private String title;
	private String titleId;
	private String image;
	private String imageFilePath;
	private String imageDescription;
	private String dateEvent;
	private String type;
	private String contentType;
	private String pageLink;
	private int sequence;
 

	public Pictures() {

	}


	public Pictures(String title, String titleId, String image, String imageFilePath, String imageDescription,
			String dateEvent, String type, String contentType, String pageLink, int sequence) {
		super();
		this.title = title;
		this.titleId = titleId;
		this.image = image;
		this.imageFilePath = imageFilePath;
		this.imageDescription = imageDescription;
		this.dateEvent = dateEvent;
		this.type = type;
		this.contentType = contentType;
		this.pageLink = pageLink;
		this.sequence = sequence;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getTitleId() {
		return titleId;
	}


	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getImageFilePath() {
		return imageFilePath;
	}


	public void setImageFilePath(String imageFilePath) {
		this.imageFilePath = imageFilePath;
	}


	public String getImageDescription() {
		return imageDescription;
	}


	public void setImageDescription(String imageDescription) {
		this.imageDescription = imageDescription;
	}


	public String getDateEvent() {
		return dateEvent;
	}


	public void setDateEvent(String dateEvent) {
		this.dateEvent = dateEvent;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getContentType() {
		return contentType;
	}


	public void setContentType(String contentType) {
		this.contentType = contentType;
	}


	public String getPageLink() {
		return pageLink;
	}


	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}


	public int getSequence() {
		return sequence;
	}


	public void setSequence(int sequence) {
		this.sequence = sequence;
	}


	@Override
	public String toString() {
		return "Pictures [title=" + title + ", titleId=" + titleId + ", image=" + image + ", imageFilePath="
				+ imageFilePath + ", imageDescription=" + imageDescription + ", dateEvent=" + dateEvent + ", type="
				+ type + ", contentType=" + contentType + ", pageLink=" + pageLink + ", sequence=" + sequence + "]";
	}

	
}
