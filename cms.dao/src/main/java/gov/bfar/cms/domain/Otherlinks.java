package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class Otherlinks extends BaseDomain implements Domain {
    private String title;
    private String content;
    private String pageLink;
    
    
    public Otherlinks() {
        
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPageLink() {
        return pageLink;
    }

    public void setPageLink(String pageLink) {
        this.pageLink = pageLink;
    }


}
