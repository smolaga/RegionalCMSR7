package gov.bfar.cms.domain;

import java.util.UUID;

public class BaseDomain {

	private long tid;
	private String id;
	private String dateCreated;
	private String createdBy;
	private String modifiedBy;
	private String modifiedDate;
	private boolean status;

	public BaseDomain() {

	}

	

	@Override
	public String toString() {
		return "BaseDomain [tid=" + tid + ", id=" + id + ", dateCreated=" + dateCreated + ", createdBy=" + createdBy
				+ ", modifiedBy=" + modifiedBy + ", modifiedDate=" + modifiedDate + ", status=" + status + "]";
	}



	public long getTid() {
		return tid;
	}



	public void setTid(long tid) {
		this.tid = tid;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getDateCreated() {
		return dateCreated;
	}



	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}



	public String getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public String getModifiedBy() {
		return modifiedBy;
	}



	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}



	public String getModifiedDate() {
		return modifiedDate;
	}



	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}



	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}



	public BaseDomain(long tid, String id, String dateCreated, String createdBy, String modifiedBy, String modifiedDate,
			boolean status) {
		super();
		this.tid = tid;
		this.id = id;
		this.dateCreated = dateCreated;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
		this.status = status;
	}



	public void activate() {
		this.status = true;
	}

	public void deactive() {
		this.status = false;
	}

	public static String newUUID() {
		return UUID.randomUUID().toString();
	}
}
