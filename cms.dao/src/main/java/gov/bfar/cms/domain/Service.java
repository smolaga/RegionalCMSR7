package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class Service extends BaseDomain implements Domain{

	private String title;
	private String content;
	private String pdfFilePath;
	private String contentType;
	private String subContentLink;
	private String pageLink;
	private int sequence;
	
	

	public Service() {

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}	
	

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	
	
	public String getSubContentLink() {
        return subContentLink;
    }

    public void setSubContentLink(String subContentLink) {
        this.subContentLink = subContentLink;
    }

    public String getPdfFilePath() {
		return pdfFilePath;
	}

	public void setPdfFilePath(String pdfFilePath) {
		this.pdfFilePath = pdfFilePath;
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	
	@Override
    public String toString() {
        return "Service [title=" + title + ", content=" + content + ", pageLink=" + pageLink + ", sequence=" + sequence
                + ", contentType=" + contentType + ", pdfFilePath=" + pdfFilePath + "]";
	}

}
