package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class Header extends BaseDomain implements Domain{

	private String image;
	private String type;
	private int sequence;

	public Header() {

	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	
	
}
