package gov.bfar.cms.domain;

public class Search {

    private String id;
    private String title;
    private String pageLink;
    
    public Search() {
        // TODO Auto-generated constructor stub
    }

    public Search(String id, String title, String pageLink) {
        super();
        this.id = id;
        this.title = title;
        this.pageLink = pageLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPageLink() {
        return pageLink;
    }

    public void setPageLink(String pageLink) {
        this.pageLink = pageLink;
    }

    @Override
    public String toString() {
        return "Search [id=" + id + ", title=" + title + ", pageLink=" + pageLink + "]";
    }
}
