package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class FilePath extends BaseDomain implements Domain{

	private String fileType;
	private String filePath;
	private String contentType;
	private String region;
	
	public FilePath() {
		// TODO Auto-generated constructor stub
	}

	public FilePath(String fileType, String filePath, String contentType, String region) {
		super();
		this.fileType = fileType;
		this.filePath = filePath;
		this.contentType = contentType;
		this.region = region;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public String toString() {
		return "FilePath [fileType=" + fileType + ", filePath=" + filePath + ", contentType=" + contentType
				+ ", region=" + region + "]";
	}
	
	
}
