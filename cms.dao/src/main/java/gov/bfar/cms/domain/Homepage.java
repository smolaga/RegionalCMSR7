package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class Homepage extends BaseDomain implements Domain {
    private String head;
    private String shortContent;
    private String content;
    private String subContentLink;
    private String seeAllLink;
    private String pageLink;
    private String linkStatus;
    private String columnType;
    private String headid;
    private int sequence;
    private String pdfFilePath;
    private String contentType;
    private boolean viewonpage;
    private boolean viewonlist;

    
    public Homepage() {
        // TODO Auto-generated constructor stub
    }


	public String getHead() {
		return head;
	}


	public void setHead(String head) {
		this.head = head;
	}


	public String getShortContent() {
		return shortContent;
	}


	public void setShortContent(String shortContent) {
		this.shortContent = shortContent;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getSubContentLink() {
		return subContentLink;
	}


	public void setSubContentLink(String subContentLink) {
		this.subContentLink = subContentLink;
	}


	public String getSeeAllLink() {
		return seeAllLink;
	}


	public void setSeeAllLink(String seeAllLink) {
		this.seeAllLink = seeAllLink;
	}


	public String getPageLink() {
		return pageLink;
	}


	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}


	public String getLinkStatus() {
		return linkStatus;
	}


	public void setLinkStatus(String linkStatus) {
		this.linkStatus = linkStatus;
	}


	public String getColumnType() {
		return columnType;
	}


	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}


	public String getHeadid() {
		return headid;
	}


	public void setHeadid(String headid) {
		this.headid = headid;
	}


	public int getSequence() {
		return sequence;
	}


	public void setSequence(int sequence) {
		this.sequence = sequence;
	}


	public String getPdfFilePath() {
		return pdfFilePath;
	}


	public void setPdfFilePath(String pdfFilePath) {
		this.pdfFilePath = pdfFilePath;
	}


	public String getContentType() {
		return contentType;
	}


	public void setContentType(String contentType) {
		this.contentType = contentType;
	}


	public boolean isViewonpage() {
		return viewonpage;
	}


	public void setViewonpage(boolean viewonpage) {
		this.viewonpage = viewonpage;
	}
	
	public void vopactivate() {
		this.viewonpage = true;
	}

	public void vopdeactive() {
		this.viewonpage = false;
	}


	public boolean isViewonlist() {
		return viewonlist;
	}


	public void setViewonlist(boolean viewonlist) {
		this.viewonlist = viewonlist;
	}
	
	public void volactivate() {
		this.viewonlist = true;
	}

	public void voldeactive() {
		this.viewonlist = false;
	}


	public Homepage(String head, String shortContent, String content, String subContentLink, String seeAllLink,
			String pageLink, String linkStatus, String columnType, String headid, int sequence, String pdfFilePath,
			String contentType, boolean viewonpage, boolean viewonlist) {
		super();
		this.head = head;
		this.shortContent = shortContent;
		this.content = content;
		this.subContentLink = subContentLink;
		this.seeAllLink = seeAllLink;
		this.pageLink = pageLink;
		this.linkStatus = linkStatus;
		this.columnType = columnType;
		this.headid = headid;
		this.sequence = sequence;
		this.pdfFilePath = pdfFilePath;
		this.contentType = contentType;
		this.viewonpage = viewonpage;
		this.viewonlist = viewonlist;
	}


	@Override
	public String toString() {
		return "Homepage [head=" + head + ", shortContent=" + shortContent + ", content=" + content
				+ ", subContentLink=" + subContentLink + ", seeAllLink=" + seeAllLink + ", pageLink=" + pageLink
				+ ", linkStatus=" + linkStatus + ", columnType=" + columnType + ", headid=" + headid + ", sequence="
				+ sequence + ", pdfFilePath=" + pdfFilePath + ", contentType=" + contentType + ", viewonpage="
				+ viewonpage + ", viewonlist=" + viewonlist + "]";
	}


    /*
     * public static void main(String[] args) { ======= public void
     * setHeadid(String headid) { this.headid = headid; } public static void
     * main(String[] args) { >>>>>>> .r191 Homepage homepage = new Homepage();
     * HomepageDao homepageDao = new HomepageDao(); Optional<String> date =
     * DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss"); String uuid =
     * BaseDomain.newUUID(); homepage.setId(uuid); homepage.setHead("weee");
     * homepage.setShortContent("weee"); homepage.setContent("weee");
     * homepage.setSeeAllLink("weee"); homepage.setPageLink("weee");
     * homepage.setLinkStatus("1"); homepage.setColumnType("left");
     * homepage.setSequence(1); homepage.setStatus(true);
     * homepage.setDateCreated(date.get()); homepage.setCreatedBy(uuid);
     * homepage.setModifiedDate(date.get()); homepage.setModifiedBy(uuid);
     * homepageDao.save(homepage); homepageDao.save(homepage);
     * homepageDao.findAll(); List<Homepage> homepages = homepageDao.findAll();
     * for (Homepage homepage2: homepages) {
     * System.out.println(homepage2.getId());
     * System.out.println(homepage2.getHead());
     * System.out.println(homepage2.getShortContent());
     * System.out.println(homepage2.getContent());
     * System.out.println(homepage2.getSeeAllLink());
     * System.out.println(homepage2.getPageLink());
     * System.out.println(homepage2.getLinkStatus());
     * System.out.println(homepage2.getColumnType());
     * System.out.println(homepage2.getSequence());
     * System.out.println(homepage2.isStatus());
     * System.out.println(homepage2.getDateCreated());
     * System.out.println(homepage2.getCreatedBy());
     * System.out.println(homepage2.getModifiedDate());
     * System.out.println(homepage2.getModifiedBy()); System.out.println(
     * "++++++++++++++++++++++++++++++++++++++++++++++++++++++"); } }
     */
    /*
     * public static Optional<Homepage> toBean(Map properties) {
     * Optional<Homepage> homepageValue = Optional.empty(); try { Homepage
     * homepage = new Homepage(); BeanUtils.populate(homepage, properties);
     * Optional<Date> formattedDate = DateUtil.convertDateString("11/03/2015",
     * "MM/dd/yyyy"); Optional<String> formattedDates =
     * DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss"); Date transactionDate =
     * DateUtil.getDate(); System.out.println(formattedDates.get());
     * homepage.setCreatedBy(""); homepage.setCreationDate(transactionDate);
     * homepage.setModifiedBy(""); homepage.setDateModified(transactionDate);
     * homepageValue = Optional.of(homepage); } catch (Exception e) { } return
     * homepageValue; }
     */

}
