package gov.bfar.cms.domain.intefaces;

public interface Domain {
	
	String getId();

	void setId(String id);

	String getDateCreated();

	void setDateCreated(String dateCreated);

	String getCreatedBy();

	void setCreatedBy(String createdBy);

	String getModifiedBy();

	void setModifiedBy(String modifiedBy);

	String getModifiedDate();

	void setModifiedDate(String ModifiedDate);

	boolean isStatus();

	void setStatus(boolean status);
	
}
