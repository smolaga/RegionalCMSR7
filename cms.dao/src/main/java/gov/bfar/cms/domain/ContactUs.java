package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class ContactUs extends BaseDomain implements Domain {
    private String title;
    private String content;
    private String contentType;
    private String pageLink;
    
    
    public ContactUs() {
        
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getPageLink() {
        return pageLink;
    }

    public void setPageLink(String pageLink) {
        this.pageLink = pageLink;
    }


}
