package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class Hits extends BaseDomain implements Domain {
    private String JSessionId;
    private String sessionDate;
    
    
    
    public Hits() {
        
    }



    public String getJSessionId() {
        return JSessionId;
    }



    public void setJSessionId(String jSessionId) {
        JSessionId = jSessionId;
    }



    public String getSessionDate() {
        return sessionDate;
    }



    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

}
