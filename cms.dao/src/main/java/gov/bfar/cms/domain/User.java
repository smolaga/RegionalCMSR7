package gov.bfar.cms.domain;

import gov.bfar.cms.domain.intefaces.Domain;

public class User extends BaseDomain implements Domain {

	private String firstName;
	private String middleName;
	private String lastName;
	private String designation;
	private String userName;
	private String userPassword;
	private String userType;
	private String secretQuestion;
	private String answer;
	private boolean canUpdate;
	private boolean canShow;
	private String userAccessContent;

	public User() {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getSecretQuestion() {
		return secretQuestion;
	}

	public void setSecretQuestion(String secretQuestion) {
		this.secretQuestion = secretQuestion;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public boolean isCanUpdate() {
		return canUpdate;
	}

	public void setCanUpdate(boolean canUpdate) {
		this.canUpdate = canUpdate;
	}

	public boolean isCanShow() {
		return canShow;
	}

	public void setCanShow(boolean canShow) {
		this.canShow = canShow;
	}

	public String getUserAccessContent() {
		return userAccessContent;
	}

	public void setUserAccessContent(String userAccessContent) {
		this.userAccessContent = userAccessContent;
	}

	public User(String firstName, String middleName, String lastName, String designation, String userName,
			String userPassword, String userType, String secretQuestion, String answer, boolean canUpdate,
			boolean canShow, String userAccessContent) {
		super();
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.designation = designation;
		this.userName = userName;
		this.userPassword = userPassword;
		this.userType = userType;
		this.secretQuestion = secretQuestion;
		this.answer = answer;
		this.canUpdate = canUpdate;
		this.canShow = canShow;
		this.userAccessContent = userAccessContent;
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName
				+ ", designation=" + designation + ", userName=" + userName + ", userPassword=" + userPassword
				+ ", userType=" + userType + ", secretQuestion=" + secretQuestion + ", answer=" + answer
				+ ", canUpdate=" + canUpdate + ", canShow=" + canShow + ", userAccessContent=" + userAccessContent
				+ "]";
	}

	

	
}
