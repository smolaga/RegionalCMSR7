package gov.bfar.cms.dao;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Pictures;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class PicturesDao extends BaseDao<Pictures>{

	public PicturesDao() {
		 super(Pictures.class);
	}
	
	public int setOneActiveStatus(String type, String contentType, String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("type", type);
            map.put("contentType", contentType);
            map.put("id", id);
            String query = Pictures.class.getName().toString() + ".setOneActiveStatus";
            status = (Integer)session.update(query, map);
            session.commit();
        } finally {
            session.close();
        }
        return status;
    }
	
	public List<Pictures> picturesSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".picturesSearchList",map);
            return pictures;
        } finally {
            session.close();
        }
    }

	public List<Pictures> sliderListAll() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".sliderListAll");
            return pictures;
        } finally {
            session.close();
        }
    }
	
	public Pictures spAutoOrdering(int lastSequence, String id, String type, String contentType, int oldseq){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Pictures pictures = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("lastSequence", lastSequence);
            map.put("id", id);
            map.put("type", type);
            map.put("contentType", contentType);
            map.put("oldseq", oldseq);
			pictures = (Pictures) session.selectOne(Pictures.class.getName().toString() + ".spAutoOrdering", map);
            return pictures;
		}finally {
			session.close();
		}
	}
	
	public Pictures spAutoOrderingGallery(int lastSequence, String id, String type, String contentType, String titleId, int oldseq){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Pictures pictures = null;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("lastSequence", lastSequence);
            map.put("id", id);
            map.put("type", type);
            map.put("contentType", contentType);
            map.put("titleId", titleId);
            map.put("oldseq", oldseq);
            pictures = (Pictures) session.selectOne(Pictures.class.getName().toString() + ".spAutoOrderingGallery", map);
            return pictures;
        }finally {
            session.close();
        }
    }
	
	public Pictures OneBannerDisplay(){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Pictures pictures = null;
		try {
			
			pictures = (Pictures) session.selectOne(Pictures.class.getName().toString() + ".OneBannerDisplay");
            return pictures;
		}finally {
			session.close();
		}
	}
	
	public Pictures getSlider(){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Pictures pictures = null;
		try {
			
			pictures = (Pictures) session.selectOne(Pictures.class.getName().toString() + ".getSlider");
            return pictures;
		}finally {
			session.close();
		}
	}
	
	public Pictures getFirstSlider(){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Pictures pictures = null;
		try {
			
			pictures = (Pictures) session.selectOne(Pictures.class.getName().toString() + ".getFirstSlider");
            return pictures;
		}finally {
			session.close();
		}
	}
	
	public List<Pictures> getNotFirstSlider() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".getNotFirstSlider");
            return pictures;
        } finally {
            session.close();
        }
    }
	
	 public List<Pictures> sliderContentList() {
     	
 		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
         SqlSession session = sqlSessionFactory.openSession();
         
         try{
         	List<Pictures> pictures= session.selectList(Pictures.class.getName().toString() + ".sliderContentList");
         	return pictures;
         	
         }finally{
         	session.close();
         }
         
  }
	

	public List<Pictures> bannerListAll() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".bannerListAll");
            return pictures;
        } finally {
            session.close();
        }
	}
	
	public Pictures getBanner(){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Pictures pictures = null;
		try {
			
			pictures = (Pictures) session.selectOne(Pictures.class.getName().toString() + ".getBanner");
            return pictures;
		}finally {
			session.close();
		}
	}
	
	public int getSuggestedSequenceGallery(String type, String contentType, String titleId){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
        try {
            try {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("type", type);
                map.put("contentType", contentType);
                map.put("titleId", titleId);
                count =  session.selectOne(Pictures.class.getName().toString() + ".getSuggestedSequenceGallery",map);
            } catch (Exception e) {
                count = 1;
            }
            return count;
        }finally {
            session.close();
        }
    }
	
	public int getSuggestedSequence(String type, String contentType){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
		try {
            try {
            	Map<String, Object> map = new HashMap<String, Object>();
                map.put("type", type);
                map.put("contentType", contentType);
            	count =  session.selectOne(Pictures.class.getName().toString() + ".getSuggestedSequence",map);
			} catch (Exception e) {
				count = 1;
			}
            return count;
		}finally {
			session.close();
		}
	}
	
	public int firstSequenceDeterminerGallery(String type, String contentType, String titleId){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("type", type);
            map.put("contentType", contentType);
            map.put("titleId", titleId);
			count =  session.selectOne(Pictures.class.getName().toString() + ".firstSequenceDeterminer",map);
            return count;
		}finally {
			session.close();
		}
	}
	
	public int firstSequenceDeterminer(String type, String contentType){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("type", type);
            map.put("contentType", contentType);
			count =  session.selectOne(Pictures.class.getName().toString() + ".firstSequenceDeterminer",map);
            return count;
		}finally {
			session.close();
		}
	}
	
	/*public int getCountSlider(String type){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("type", type);
			count =  session.selectOne(Pictures.class.getName().toString() + ".getCountSlider",map);
            return count;
		}finally {
			session.close();
		}
	}*/
	
	public int getCountSlider(){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
		try {
			count =  session.selectOne(Pictures.class.getName().toString() + ".getCountSlider");
            return count;
		}finally {
			session.close();
		}
	}
   
	
	public static void main(String[] args) {
		PicturesDao picturesDao = new PicturesDao();
		int x = 0;
		 x = picturesDao.getCountSlider()-1;
		 System.out.println(x);
		for (int i = 1; i <= x; i++) {
			System.out.println("COUNT : " + i);
		}
	}
	
	public List<Pictures> galleryTitleList() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".galleryTitleList");
            return pictures;
            
        } finally {
            session.close();
        }
    }
	
/*	public List<Pictures> galleryImageListAll(String titleId) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	 Map<String, Object> map = new HashMap<String, Object>();
             map.put("titleId", titleId);
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".galleryImageListAll", map);
            return pictures;
            
        } finally {
            session.close();
        }
    }*/
	
	public List<Pictures> galleryImageListAll(String titleId) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	Map<String, Object> map = new HashMap<String, Object>();
            map.put("titleId", titleId);
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".galleryImageListAll", map);
            return pictures;
        } finally {
            session.close();
        }
    }
	
	public List<Pictures> galleryPerMonthYear(int month, int year) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	Map<String, Object> map = new HashMap<String, Object>();
            map.put("year", year);
            map.put("month", month);
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".galleryPerMonthYear", map);
            return pictures;
        } finally {
            session.close();
        }
    }
	
	public List<Pictures> galleryContentList() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {                    
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".galleryContentList");
            return pictures;
        } finally {
            session.close();
        }
    }
	
	public List<Pictures> galleryImageList(String titleId) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {                    
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("titleId", titleId);
            List<Pictures> pictures = session.selectList(Pictures.class.getName().toString() + ".galleryImageList", map);
            return pictures;
        } finally {
            session.close();
        }
    }
	
	
	
	
}
