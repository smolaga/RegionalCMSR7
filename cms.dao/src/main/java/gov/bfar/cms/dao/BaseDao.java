package gov.bfar.cms.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import gov.bfar.cms.domain.intefaces.Domain;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class BaseDao<T extends Domain> {
	
	private Class<T> clazz;
	
	public BaseDao(Class<T> clazz) {
        this.clazz = clazz;
    }
	
	public List<T> findAll() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            List<T> list = session.selectList(clazz.getName().toString() + ".findAll");
            return list;
        } finally {
            session.close();
        }
    }

    public int save(T entity) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            String query = clazz.getName() + ".save";
            status = (Integer)session.insert(query, entity);
            session.commit();
        } finally {
            session.close();
        }

        return status;
    }

    public int delete(String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            String query = clazz.getName() + ".delete";
            status = (Integer)session.delete(query, id);
            session.commit();
        } finally {
            session.close();
        }

        return status;
    }

    public int update(T entity) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            String query = clazz.getName() + ".update";
            status = (Integer)session.update(query, entity);
            session.commit();
        } finally {
            session.close();
        }

        return status;
    }

    public T findById(String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        T entity = null;
        try {
            String query = clazz.getName() + ".findById";
            entity = session.selectOne(query, id);

        } finally {
            session.close();
        }

        return entity;
    }
    


    public Class<T> getClazz() {
        return clazz;
    }
}
