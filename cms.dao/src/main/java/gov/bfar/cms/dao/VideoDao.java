package gov.bfar.cms.dao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Video;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class VideoDao extends BaseDao<Video>{

	public VideoDao() {
		 super(Video.class);
	}

	public List<Video> videoSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<Video> videos = session.selectList(Video.class.getName().toString() + ".videoSearchList",map);
            return videos;
        } finally {
            session.close();
        }
    }
	
	public int getSuggestedSequence(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
        try {
            try {
                count =  session.selectOne(Video.class.getName().toString() + ".getSuggestedSequence");
            } catch (Exception e) {
                count = 1;
            }
            return count;
        }finally {
            session.close();
        }
    }
	
	public List<Video> videoFindAllContent() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {

            List<Video> video = session.selectList(Video.class.getName().toString() + ".videoFindAllContent");
            return video;
        } finally {
            session.close();
        }
    }
	
	public Video spAutoOrdering(int lastSequence, String id, int oldseq){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Video videos = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("lastSequence", lastSequence);
            map.put("id", id);
            map.put("oldseq", oldseq);
            videos = (Video) session.selectOne(Video.class.getName().toString() + ".spAutoOrdering", map);
            return videos;
		}finally {
			session.close();
		}
	}
	
	public List<Video> videoContentList() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {

            List<Video> video = session.selectList(Video.class.getName().toString() + ".videoContentList");
            return video;
        } finally {
            session.close();
        }
    }
}
