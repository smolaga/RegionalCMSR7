package gov.bfar.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Laws;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class LawsDao extends BaseDao<Laws>{

    public LawsDao() {
        super(Laws.class);
   }
    
    public int setOneActiveStatus(String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", id);
            String query = Laws.class.getName().toString() + ".setOneActiveStatus";
            status = (Integer)session.update(query, map);
            session.commit();
        } finally {
            session.close();
        }
        return status;
    }
    
    public List<Laws> lawsSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<Laws> laws = session.selectList(Laws.class.getName().toString() + ".lawsSearchList",map);
            return laws;
        } finally {
            session.close();
        }
    }
    
    public List<Laws> lawsFindAllContent(String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Laws> laws = session.selectList(Laws.class.getName().toString() + ".lawsFindAllContent", map);
            return laws;
        } finally {
            session.close();
        }
    }
    
    public List<Laws> lawsContentList(String conTypeHTML, String conTypePDF) {
        
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Laws> laws= session.selectList(Laws.class.getName().toString() + ".lawsContentList", map);
            return laws;
            
        }finally{
            session.close();
        }
 }
    
    
    public Laws lawsGetContent(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Laws laws = null;
        try {           
            laws = (Laws) session.selectOne(Laws.class.getName().toString() + ".lawsGetContent");
            return laws;
        }finally {
            session.close();
        }
    }
}


