package gov.bfar.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Otherlinks;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class OtherlinksDao  extends BaseDao<Otherlinks> {
    
    
    public OtherlinksDao() {
        super(Otherlinks.class);
   }
    
    public int setOneActiveStatus(String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", id);
            String query = Otherlinks.class.getName().toString() + ".setOneActiveStatus";
            status = (Integer)session.update(query, map);
            session.commit();
        } finally {
            session.close();
        }
        return status;
    }
    
    public List<Otherlinks> otherlinksFindAllContent() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {

            List<Otherlinks> otherlinks = session.selectList(Otherlinks.class.getName().toString() + ".otherlinksFindAllContent");
            return otherlinks;
        } finally {
            session.close();
        }
    }
    
    public Otherlinks otherlinksContentList(){
   
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Otherlinks otherlinks = null;
        try {           
            otherlinks = (Otherlinks) session.selectOne(Otherlinks.class.getName().toString() + ".otherlinksContentList");
            return otherlinks;
        }finally {
            session.close();
        }
    }
    

  

}
