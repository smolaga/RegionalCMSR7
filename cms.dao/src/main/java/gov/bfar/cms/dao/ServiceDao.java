package gov.bfar.cms.dao;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Service;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class ServiceDao extends BaseDao<Service>{

	public ServiceDao() {
		 super(Service.class);
	}
	
	public int setOneActiveStatus(String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", id);
            String query = Service.class.getName().toString() + ".setOneActiveStatus";
            status = (Integer)session.update(query, map);
            session.commit();
        } finally {
            session.close();
        }
        return status;
    }
	
	public List<Service> serviceFindAllContent(String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Service> service = session.selectList(Service.class.getName().toString() + ".serviceFindAllContent", map);
            return service;
        } finally {
            session.close();
        }
    }
	
	public List<Service> serviceSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<Service> services = session.selectList(Service.class.getName().toString() + ".serviceSearchList",map);
            return services;
        } finally {
            session.close();
        }
    }
	
	public List<Service> serviceContentList(String conTypeHTML, String conTypePDF) {
    	
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
        	List<Service> service= session.selectList(Service.class.getName().toString() + ".serviceContentList", map);
        	return service;
        	
        }finally{
        	session.close();
        }
 }
	
	
	public Service serviceGetContent(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Service service = null;
        try {           
            service = (Service) session.selectOne(Service.class.getName().toString() + ".serviceGetContent");
            return service;
        }finally {
            session.close();
        }
    }
}
