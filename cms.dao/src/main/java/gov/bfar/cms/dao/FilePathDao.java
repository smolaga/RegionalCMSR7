package gov.bfar.cms.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.FilePath;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class FilePathDao extends BaseDao<FilePath>{
	
	public FilePathDao() {
		 super(FilePath.class);
	}
	
	public FilePath getFilePath(String fileType, String contentType) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        FilePath filePaths = null;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("fileType", fileType);
            map.put("contentType", contentType);
            filePaths = (FilePath)session.selectOne(FilePath.class.getName().toString() + ".getFilePath", map);
            return filePaths;
        } finally {
            session.close();
        }
    }
	
}
