package gov.bfar.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Homepage;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class HomepageDao extends BaseDao<Homepage> {

	public HomepageDao() {
		super(Homepage.class);
	}

	public List<Homepage> findAllContent(String columntype, String conTypeHTML, String conTypePDF, String headID) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columntype);
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            map.put("headid", headID);
            List<Homepage> homepages = session.selectList(Homepage.class.getName().toString() + ".findAllContent", map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
	public int getSuggestedSequence(String contentType, String columnType){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("contentType", contentType);
            map.put("columnType", columnType);
            try {
            	count =  session.selectOne(Homepage.class.getName().toString() + ".getSuggestedSequence",map);
			} catch (Exception e) {
				count = 1;
			}
			
			
            return count;
		}finally {
			session.close();
		}
	}
	public Homepage spAutoOrdering(int lastSequence, String id, String columnType, String contentType, int oldseq){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Homepage homepages = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("lastSequence", lastSequence);
            map.put("id", id);
            map.put("columnType", columnType);
            map.put("contentType", contentType);
            map.put("oldseq", oldseq);
            homepages = (Homepage) session.selectOne(Homepage.class.getName().toString() + ".spAutoOrdering", map);
            return homepages;
		}finally {
			session.close();
		}
	}
	public List<Homepage> findAllCenterContent(String columntype, String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columntype);
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Homepage> homepages = session.selectList(Homepage.class.getName().toString() + ".findAllCenterContent", map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
	public List<Homepage> webTitleList(String columntype) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columntype);
            List<Homepage> homepages = session.selectList(Homepage.class.getName().toString() + ".webTitleList", map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
	public List<Homepage> webMainContentList(String columntype, String headid) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columntype);
            map.put("headid", headid);
            List<Homepage> homepages = session.selectList(Homepage.class.getName().toString() + ".webMainContentList", map);
            
            return homepages;
        } finally {
            session.close();
        }
    }
	
	public List<Homepage> webSidebarListList(String columntype, String headid) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columntype);
            map.put("headid", headid);
            List<Homepage> homepages = session.selectList(Homepage.class.getName().toString() + ".webSidebarListList", map);
            
            return homepages;
        } finally {
            session.close();
        }
    }
	
	
	public List<Homepage> titleList(String columntype, String contenttype) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columntype);
            map.put("contentType", contenttype);
            List<Homepage> homepages = session.selectList(Homepage.class.getName().toString() + ".titleList", map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
	public List<Homepage> subContentList(String columnType,String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	Map<String, Object> map = new HashMap<String, Object>();
        	map.put("columnType", columnType);
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Homepage> homepages = session.selectList(Homepage.class.getName().toString() + ".subContentList", map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
	public Homepage findNewPDFFiles(String columnType, String ctpdf, String ctsubpdf, String pdfFilePath) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columnType);
            map.put("ctpdf", ctpdf);
            map.put("ctsubpdf", ctsubpdf);
            map.put("pdfFilePath", pdfFilePath);
            Homepage homepages = (Homepage) session.selectOne(Homepage.class.getName().toString() + ".findNewPDFFiles", map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
	public Homepage findOldPDFFiles(String columnType, String ctpdf, String ctsubpdf, String pdfFilePath) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columnType);
            map.put("ctpdf", ctpdf);
            map.put("ctsubpdf", ctsubpdf);
            map.put("pdfFilePath", pdfFilePath);
            Homepage homepages = (Homepage) session.selectOne(Homepage.class.getName().toString() + ".findOldPDFFiles", map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
	
	public List<Homepage> homepageSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<Homepage> homepages = session.selectList(Homepage.class.getName().toString() + ".homepageSearchList",map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
	public Homepage getFirstContent(String columnType, String headid) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();

        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("columnType", columnType);
            map.put("headid", headid);
           Homepage homepages = (Homepage) session.selectOne(Homepage.class.getName().toString() + ".getFirstContent", map);
            return homepages;
        } finally {
            session.close();
        }
    }
	
}
