package gov.bfar.cms.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Hits;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class HitsDao  extends BaseDao<Hits> {
    
    
    public HitsDao() {
        super(Hits.class);
   }
    
    public int getHitCount() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {

            int hits = session.selectOne(Hits.class.getName().toString() + ".getHitCount");
            return hits;
        } finally {
            session.close();
        }
    }

}
