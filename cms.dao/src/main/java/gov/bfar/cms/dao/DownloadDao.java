package gov.bfar.cms.dao;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Download;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class DownloadDao extends BaseDao<Download>{

    public DownloadDao() {
         super(Download.class);
    }

    public List<Download> downloadSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<Download> downloads = session.selectList(Download.class.getName().toString() + ".downloadSearchList",map);
            return downloads;
        } finally {
            session.close();
        }
    }
    

    public int getSuggestedSequence(){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
		try {
            try {
            	count =  session.selectOne(Download.class.getName().toString() + ".getSuggestedSequence");
			} catch (Exception e) {
				count = 1;
			}
            return count;
		}finally {
			session.close();
		}
	}
    
    public List<Download> downloadFindAllContent(String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Download> download = session.selectList(Download.class.getName().toString() + ".downloadFindAllContent",map);
            return download;
        } finally {
            session.close();
        }
    }
    
    public Download spAutoOrdering(int lastSequence, String id, int oldseq){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Download downloads = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("lastSequence", lastSequence);
            map.put("id", id);
            map.put("oldseq", oldseq);
            downloads = (Download) session.selectOne(Download.class.getName().toString() + ".spAutoOrdering", map);
            return downloads;
		}finally {
			session.close();
		}
	}
    
    public List<Download> downloadContentList(String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Download> download = session.selectList(Download.class.getName().toString() + ".downloadContentList",map);
            return download;
        } finally {
            session.close();
        }
    }
    public List<Download> downloadSideBarList(String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Download> download = session.selectList(Download.class.getName().toString() + ".downloadSideBarList",map);
            return download;
        } finally {
            session.close();
        }
    }
    
    public Download downloadSelectList(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Download download = null;
        try {           
            download = (Download) session.selectOne(Download.class.getName().toString() + ".downloadSelectList");
            return download;
        }finally {
            session.close();
        }
    }
    public Download downloadSelectListTrue(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Download download = null;
        try {           
            download = (Download) session.selectOne(Download.class.getName().toString() + ".downloadSelectListTrue");
            return download;
        }finally {
            session.close();
        }
    }
    
}
