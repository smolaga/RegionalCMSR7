package gov.bfar.cms.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.EmailInquiry;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class EmailInquiryDao extends BaseDao<EmailInquiry>{
    
    public EmailInquiryDao() {
        super(EmailInquiry.class);
   }

    
    public EmailInquiry OneEmailSend(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        EmailInquiry emailInquiry = null;
        try {           
            emailInquiry = (EmailInquiry) session.selectOne(EmailInquiry.class.getName().toString() + ".OneEmailSend");
            return emailInquiry;
        }finally {
            session.close();
        }
    } 

}
