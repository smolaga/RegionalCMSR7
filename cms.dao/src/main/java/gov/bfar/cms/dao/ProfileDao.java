package gov.bfar.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Profile;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class ProfileDao extends BaseDao<Profile>{

	public ProfileDao() {
		 super(Profile.class);		  
	}
	
	public int getSuggestedSequence(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
        try {
            try {
                count =  session.selectOne(Profile.class.getName().toString() + ".getSuggestedSequence");
            } catch (Exception e) {
                count = 1;
            }
            return count;
        }finally {
            session.close();
        }
    }
	
	public List<Profile> profileSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<Profile> profiles = session.selectList(Profile.class.getName().toString() + ".profileSearchList",map);
            return profiles;
        } finally {
            session.close();
        }
    }
	
	public List<Profile> profileFindAllContent(String conTypeHTML, String conTypePDF){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
         try {

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<Profile> profile = session.selectList(Profile.class.getName().toString() + ".profileFindAllContent", map);
            return profile;
        } finally {
            session.close();
        }
       }
        
	public Profile spAutoOrdering(int lastSequence, String id, int oldseq){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Profile profiles = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("lastSequence", lastSequence);
            map.put("id", id);
            map.put("oldseq", oldseq);
            profiles = (Profile) session.selectOne(Profile.class.getName().toString() + ".spAutoOrdering", map);
            return profiles;
		}finally {
			session.close();
		}
	}
	
        public List<Profile> profileContentList(String conTypeHTML, String conTypePDF) {
        	
    		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
            SqlSession session = sqlSessionFactory.openSession();
            
            try{
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("conTypeHTML", conTypeHTML);
                map.put("conTypePDF", conTypePDF);
            	List<Profile> profile= session.selectList(Profile.class.getName().toString() + ".profileContentList",map);
            	return profile;
            	
            }finally{
            	session.close();
            }
     }
        
        public List<Profile> profileSideBarList(String conTypeHTML, String conTypePDF) {
            
            SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
            SqlSession session = sqlSessionFactory.openSession();
            
            try{
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("conTypeHTML", conTypeHTML);
                map.put("conTypePDF", conTypePDF);
                List<Profile> profile= session.selectList(Profile.class.getName().toString() + ".profileSideBarList",map);
                return profile;
                
            }finally{
                session.close();
            }
     }
        public Profile profileSelectList(){
            SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
            SqlSession session = sqlSessionFactory.openSession();
            Profile profile = null;
            try {           
                profile = (Profile) session.selectOne(Profile.class.getName().toString() + ".profileSelectList");
                return profile;
            }finally {
                session.close();
            }
        }

}
