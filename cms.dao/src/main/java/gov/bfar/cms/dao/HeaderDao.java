package gov.bfar.cms.dao;


import gov.bfar.cms.domain.Header;

public class HeaderDao extends BaseDao<Header>{

	public HeaderDao() {
		 super(Header.class);
	}

}
