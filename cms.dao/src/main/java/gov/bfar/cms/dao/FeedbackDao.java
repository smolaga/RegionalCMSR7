package gov.bfar.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Feedback;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class FeedbackDao extends BaseDao<Feedback> {
    
    public FeedbackDao(){
        super(Feedback.class);
    }
    
    public List<Feedback> feedbackList() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {

            List<Feedback> feedback = session.selectList(Feedback.class.getName().toString() + ".feedbackList");
            return feedback;
        } finally {
            session.close();
        }
    }
    
    public List<Feedback> getNewestFeedback() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            
            List<Feedback> feedback = session.selectList(Feedback.class.getName().toString() + ".getNewestFeedback");
            return feedback;
        } finally {
            session.close();
        }
    }
    
    public List<Feedback> recipientFeedbackList(String recipientaddress, String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("recipientAddress", recipientaddress);
            map.put("id", id);
            List<Feedback> feedback = session.selectList(Feedback.class.getName().toString() + ".recipientFeedbackList",map);
            return feedback;
        } finally {
            session.close();
        }
    }

}
