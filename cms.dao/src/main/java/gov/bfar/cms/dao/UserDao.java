package gov.bfar.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import gov.bfar.cms.domain.User;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class UserDao extends BaseDao<User> {

	public UserDao() {
		super(User.class);
	}
	
	public User login(String userName, String userPassword){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        User user = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("userName", userName);
            map.put("userPassword", userPassword);
            user = (User) session.selectOne(User.class.getName().toString() + ".login", map);
            return user;
		}finally {
			session.close();
		}
	}
	
	public int validateUser(String userName){
		
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        User user = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("userName", userName);
            int userCount =  session.selectOne(User.class.getName().toString() + ".validateUser", map);
            return userCount;
		}finally {
			session.close();
		}
	}
	
	
	
	 public List<User> userFindAllContent() {
	        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	        SqlSession session = sqlSessionFactory.openSession();
	        
	        try {

	            List<User> user = session.selectList(User.class.getName().toString() + ".userFindAllContent");
	            return user;
	        } finally {
	            session.close();
	        }
	    }
	    
	    public User userContentList(){
	        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	        SqlSession session = sqlSessionFactory.openSession();
	        User user = null;
	        try {           
	            user = (User) session.selectOne(User.class.getName().toString() + ".userList");
	            return user;
	        }finally {
	            session.close();
	        }
	    }
}
