package gov.bfar.cms.dao;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.Footer;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class FooterDao extends BaseDao<Footer>{

	public FooterDao() {
		 super(Footer.class);
	}
	
	public int setOneActiveStatus(String contentType, String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("contentType", contentType);
            map.put("id", id);
            String query = Footer.class.getName().toString() + ".setOneActiveStatus";
            status = (Integer)session.update(query, map);
            session.commit();
        } finally {
            session.close();
        }
        return status;
    }
	
	 public List<Footer> footerFindAllContent() {
	        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	        SqlSession session = sqlSessionFactory.openSession();
	        try {
	            List<Footer> footer = session.selectList(Footer.class.getName().toString() + ".footerFindAllContent");
	            return footer;
	        } finally {
	            session.close();
	        }
	    }
	    
	    public Footer footerContentList(){
	        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	        SqlSession session = sqlSessionFactory.openSession();
	        Footer footer = null;
	        try {           
	            footer = (Footer) session.selectOne(Footer.class.getName().toString() + ".footerContentList");
	            return footer;
	        }finally {
	            session.close();
	        }
	    }
	    
	    public Footer OneFooterDisplay(){
	        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	        SqlSession session = sqlSessionFactory.openSession();
	        Footer footer = null;
	        try {           
	        	footer = (Footer) session.selectOne(Footer.class.getName().toString() + ".OneFooterDisplay");
	            return footer;
	        }finally {
	            session.close();
	        }
	    } 

}
