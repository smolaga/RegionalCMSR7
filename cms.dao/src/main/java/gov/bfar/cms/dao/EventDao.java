package gov.bfar.cms.dao;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.*;
import gov.bfar.cms.util.MyBatisConnectionFactory;
public class EventDao {

	public EventDao() {
		super();
		// TODO Auto-generated constructor stub
	}
	public  List<Pictures> getListEvent(){
		 SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	        SqlSession session = sqlSessionFactory.openSession();
		List<Pictures> pictureList = session.selectList(Pictures.class.getName().toString() + ".sliderListAll");
		return pictureList;
	}
}

