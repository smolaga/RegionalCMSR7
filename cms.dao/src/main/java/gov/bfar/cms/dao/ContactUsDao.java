package gov.bfar.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.ContactUs;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class ContactUsDao  extends BaseDao<ContactUs> {
    
    
    public ContactUsDao() {
        super(ContactUs.class);
   }
    
    public int setOneActiveStatus(String id) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        Integer status = null;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", id);
            String query = ContactUs.class.getName().toString() + ".setOneActiveStatus";
            status = (Integer)session.update(query, map);
            session.commit();
        } finally {
            session.close();
        }
        return status;
    }
    
    public List<ContactUs> contactUsFindAllContent() {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {

            List<ContactUs> contactUs = session.selectList(ContactUs.class.getName().toString() + ".contactUsFindAllContent");
            return contactUs;
        } finally {
            session.close();
        }
    }
    
    public ContactUs contactUsContentList(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        ContactUs contactUs = null;
        try {           
            contactUs = (ContactUs) session.selectOne(ContactUs.class.getName().toString() + ".contactUsContentList");
            return contactUs;
        }finally {
            session.close();
        }
    } 

    public List<ContactUs> contactUsSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<ContactUs> contactUs = session.selectList(ContactUs.class.getName().toString() + ".contactUsSearchList",map);
            return contactUs;
        } finally {
            session.close();
        }
    }
  

}
