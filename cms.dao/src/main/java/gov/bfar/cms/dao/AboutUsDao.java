package gov.bfar.cms.dao;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import gov.bfar.cms.domain.AboutUs;
import gov.bfar.cms.util.MyBatisConnectionFactory;

public class AboutUsDao extends BaseDao<AboutUs>{

	public AboutUsDao() {
		 super(AboutUs.class);
	}

	public AboutUs spAutoOrdering(int lastSequence, String id, int oldseq){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        AboutUs aboutUs = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
            map.put("lastSequence", lastSequence);
            map.put("id", id);
            map.put("oldseq", oldseq);
            aboutUs = (AboutUs) session.selectOne(AboutUs.class.getName().toString() + ".spAutoOrdering", map);
            return aboutUs;
		}finally {
			session.close();
		}
	}
	
	public List<AboutUs> aboutUsFindAllContent(String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
        	Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<AboutUs> aboutUs = session.selectList(AboutUs.class.getName().toString() + ".aboutUsFindAllContent",map);
            return aboutUs;
        } finally {
            session.close();
        }
    }
	
	public int getSuggestedSequence(){
		SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        int count = 0;
		try {
            try {
            	count =  session.selectOne(AboutUs.class.getName().toString() + ".getSuggestedSequence");
			} catch (Exception e) {
				count = 1;
			}
            return count;
		}finally {
			session.close();
		}
	}
	
	public List<AboutUs> aboutUsContentList(String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
        	Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<AboutUs> aboutUs = session.selectList(AboutUs.class.getName().toString() + ".aboutUsContentList",map);
            return aboutUs;
        } finally {
            session.close();
        }
    }
	
	
	
	public List<AboutUs> aboutUsSideBarList(String conTypeHTML, String conTypePDF) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
        	Map<String, Object> map = new HashMap<String, Object>();
            map.put("conTypeHTML", conTypeHTML);
            map.put("conTypePDF", conTypePDF);
            List<AboutUs> aboutUs = session.selectList(AboutUs.class.getName().toString() + ".aboutUsSideBarList",map);
            return aboutUs;
        } finally {
            session.close();
        }
    }
	
	public AboutUs aboutUsSelectList(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        AboutUs aboutUs = null;
        try {           
            aboutUs = (AboutUs) session.selectOne(AboutUs.class.getName().toString() + ".aboutUsSelectList");
            return aboutUs;
        }finally {
            session.close();
        }
    }
	public AboutUs aboutUsSelectListTrue(){
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        AboutUs aboutUs = null;
        try {           
            aboutUs = (AboutUs) session.selectOne(AboutUs.class.getName().toString() + ".aboutUsSelectListTrue");
            return aboutUs;
        }finally {
            session.close();
        }
    }
	
	public List<AboutUs> aboutUsSearchList(String title) {
        SqlSessionFactory sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession();
        
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", title);
            List<AboutUs> aboutUs = session.selectList(AboutUs.class.getName().toString() + ".aboutUsSearchList",map);
            return aboutUs;
        } finally {
            session.close();
        }
    }
	
}
