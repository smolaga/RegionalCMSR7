package gov.bfar.cms.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DateUtil {
	private static final Logger LOGGER = Logger.getLogger(Date.class.toString());

	public static Date getDate() {
		return (new Date());
	}

	public static Optional<String> getFormattedDate(String format) {
		Optional<String> formattedDate = Optional.empty();
		try {
			final DateFormat dateFormatter = new SimpleDateFormat(format);
			formattedDate = Optional.of(dateFormatter.format(new Date()));
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
		return formattedDate;
	}

	public static Optional<Date> convertDateString(String dateToBeParse, String format) {
		Optional<Date> formattedDate = Optional.empty();
		final DateFormat dateFormatter = new SimpleDateFormat(format);
		try {
			formattedDate = Optional.of(dateFormatter.parse(dateToBeParse));
		} catch (ParseException e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}

		return formattedDate;
	}
	
	public static void main(String[] args) {
		Optional<Date> formattedDate = convertDateString("11/03/2015", "MM/dd/yyyy");
		Optional<String> formattedDates = getFormattedDate("MM/dd/yyyy hh:mm:ss");
		if(formattedDate.isPresent()) {
			System.out.println(formattedDate.get());
			System.out.println(formattedDates.get());
		}
		
		
	}
}
