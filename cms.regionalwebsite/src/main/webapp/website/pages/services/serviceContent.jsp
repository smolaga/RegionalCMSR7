<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.ServiceDao"%>
<%@page import="gov.bfar.cms.domain.Service"%>

<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">				
					<div class="contentContainer">
						 <%
						 	try{
						 	String id = (String)request.getAttribute("servicecontentid");
						    ServiceDao servicedao = new ServiceDao();
						    Service service = servicedao.findById(id);
						    if (service.getContentType().equalsIgnoreCase("subhtml")) {
						%>
						<%=service.getContent()%>
						<%
						    } else if (service.getContentType().equalsIgnoreCase("subpdf")) {
						%>
						<iframe id="viewer" style="border: 1px solid #666CCC"
							title="PDF in an i-Frame" src="<%=service.getPdfFilePath()%>"
							frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
						<%
						    }
						 	}catch(Exception e){
						 	    e.getMessage();
						 	   	}
						%>	
					</div>
				</div>
			</div>		
		</div>
</div>