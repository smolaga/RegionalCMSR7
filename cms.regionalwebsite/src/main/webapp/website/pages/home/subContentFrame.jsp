<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	/* String columnType = request.getAttribute("columnType").toString(); */
	String id = request.getAttribute("id").toString();
	
%>
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-9" style="border-right: 1px solid #bcb9bd">	
			<% HomepageDao homepageDao = new HomepageDao();
				Homepage homepage = homepageDao.findById(id);%>	
				<div class="contentContainer">
				   <%if(homepage.getContentType().equalsIgnoreCase("html") || homepage.getContentType().equalsIgnoreCase("subhtml")){%>
			   		<%-- <%=homepage.getHead() %> --%>
			   		<br>
				   	<%=homepage.getContent() %>
				   <%}else if(homepage.getContentType().equalsIgnoreCase("pdf") || homepage.getContentType().equalsIgnoreCase("subpdf")){%>
				   	<%=homepage.getHead() %>
				   	<br>
					<iframe id="viewerUpdate" title="PDF in an i-Frame" src="<%=homepage.getPdfFilePath() %>" frameborder="0" scrolling="no" height="50px" width="100%" ></iframe>
					<%} %>
				</div>
			</div>
			<div class="col-md-3">
					<div style="border: 1px solid #b3b8ba">
						<div class="scroll">
						<%	try{
							HomepageDao homepageDaol = new HomepageDao();
							List<Homepage> homepagesl = homepageDaol.webTitleList(homepage.getColumnType()); 
							%>
							<%for (Homepage homepagel : homepagesl){%>
							<input type="hidden" name="columntype" value="<%=homepagel.getColumnType()%>">
							<input type="hidden" name="headid" value="<%=homepagel.getId()%>">
							<h6 class="intro-text text-center">
							<%=homepagel.getHead() %>
							</h6><br>
							<% List<Homepage> leftcontent = homepageDaol.webSidebarListList(homepage.getColumnType(), homepagel.getId());
								for(Homepage homepagelc : leftcontent){%>					
							<a href="<%=homepagelc.getPageLink()%>"><%=homepagelc.getShortContent() %></a>
							<%}}
							}catch(Exception e){
							e.getMessage();
						}%>
						</div>
					</div>
				</div>
			
		</div>
	</div>
</div>

