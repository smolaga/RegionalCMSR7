<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	
	String id = request.getAttribute("id").toString();
	
%>
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-9" style="border-right: 1px solid #bcb9bd">	
			<% HomepageDao homepageDao = new HomepageDao();
				Homepage homepage = homepageDao.findById(id);%>	
				<div class="contentContainer">
				   <%if(homepage.getContentType().equalsIgnoreCase("html") || homepage.getContentType().equalsIgnoreCase("subhtml")){%>
			   		
			   		<br>
				   	<%=homepage.getContent() %>
				   <%}else if(homepage.getContentType().equalsIgnoreCase("pdf") || homepage.getContentType().equalsIgnoreCase("subpdf")){%>
				   	<%=homepage.getHead() %>
				   	<br>
					<iframe id="viewerUpdate" title="PDF in an i-Frame" src="<%=homepage.getPdfFilePath() %>" frameborder="0" scrolling="no" height="50px" width="100%" ></iframe>
					<%} %>
				</div>
			</div>
			
			<div class="col-md-3">
					<div class="contentside">
						<h5 class="intro-text text-center">
						<%
							HomepageDao titleDao = new HomepageDao(); 
							Homepage title = titleDao.findById(homepage.getHeadid());
						%>
						<%=title.getHead() %>
						</h5>
						<div class="scroll">
							<table class="hoverTable contentTable">
							<% 	try{
							    HomepageDao dao = new HomepageDao();
								List<Homepage> homepages = dao.webSidebarListList(homepage.getColumnType().toString(), homepage.getHeadid());
									for(Homepage hp : homepages) { %>							
									<tr>
										<td><a class="side" href="<%=hp.getPageLink()%>"><%=hp.getHead() %></a></td>
									</tr>
									<%}								
								}catch(Exception e){
								    e.getMessage();
								}%>
							</table>
							<br>
						</div>
					</div>
				</div> 
		</div>
	</div>
</div>

