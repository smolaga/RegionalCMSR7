<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%String tid = request.getAttribute("gallerycontentid").toString(); 
PicturesDao picturesDao = new PicturesDao();
List<Pictures>pictures = picturesDao.galleryImageList(tid);%>


<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="contentContainer">
					<%-- <%
						for (Pictures pic : pictures) {
					%>
					<div class="col-md-2">
					<table>
						<tr>
							<td style="cursor: pointer;"><img alt="GALLERY IMAGE"
							class="img-responsive dsdgal" width="100%" height="200px"
							src="<%=pic.getImageFilePath()%>">
							<p hidden style="font-size: 13px; word-wrap: break-word;"><%=pic.getTitle()%></p>
							<div hidden class="dvImgDesc"><%=pic.getImageDescription()%></div>
							<br></td>
						</tr>
					</table>
					</div>
					<%}	%> --%>
					<ul style="list-style: none">
						<%
							for (Pictures pic : pictures) {
						%>
						<li class="col-md-2" style="cursor: pointer;"><img alt="GALLERY IMAGE"
							class="img-responsive dsdgal" width="100%" height="200px"
							src="<%=pic.getImageFilePath()%>">
							<p hidden style="font-size: 13px; word-wrap: break-word;"><%=pic.getTitle()%></p>
							<div hidden class="dvImgDesc"><%=pic.getImageDescription()%></div><br>
							</li>
							
						<%
							}
						%>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="text-align: right;">
				<button type="button" class="btn btn-primary btn-sm"
					data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>