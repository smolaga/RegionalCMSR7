<%@page import="gov.bfar.cms.util.DateUtil"%>
<%@page import="java.util.Optional"%>
<%@page import="gov.bfar.cms.domain.BaseDomain"%>
<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<style>
table, th, td {
	border: 1px solid black;
}
</style>

<%
	String id = (String) request.getAttribute("gallerycontentid");
	PicturesDao picturesDao = new PicturesDao();
	List<Pictures> pictures = picturesDao.galleryContentList();
%>
<body>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="contentContainer">
						<div class="row" style="margin: 15px">
							<%
								for (Pictures pic : pictures) {
							%>
							<div class="col-md-3">
								<table>
									<tr>
										<td><a href="<%=pic.getPageLink()%>">
										<img alt="" src="<%=pic.getImageFilePath()%>" width="100%" height="200px"></a> <br>
										<p	style="height: 100px; text-align: center; font-size: 11px; font-weight: bold"><%=pic.getTitle()%></p>
										</td>
									</tr>
								</table>
								<br>
							</div>
							<%
								}
							%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>