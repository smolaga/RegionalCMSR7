<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.domain.Video"%>
<%@page import="gov.bfar.cms.dao.VideoDao"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
    String id = (String)request.getAttribute("videocontentid");
    VideoDao videoDao = new VideoDao();
    List<Video> video = videoDao.videoContentList();
   
%>

<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="border-right: 1px solid #bcb9bd">
				<div class="contentContainer">
					
					<div class="row" style="margin:15px">	
						<% for(Video videos : video){ %>	
						  <div class="col-md-4" style="font-size:17px;font-weight: bold;text-align:center">
						    <video width="300px" height="200px" controls>
							  <source src="<%=videos.getVideoFilePath() %>" type="video/mp4"></source>
							</video><br>
							<p style="height:100px; text-align: justify; font-size: 15px; font-weight: bold;word-wrap:break-word"><%=videos.getTitle() %></p>
						</div>
						<%} %>
						</div>
					</div>
					</div>
					
				</div>	
			</div>
		</div>	
</body>
</html>