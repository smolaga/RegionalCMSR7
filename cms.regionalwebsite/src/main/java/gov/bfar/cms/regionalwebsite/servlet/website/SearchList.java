package gov.bfar.cms.regionalwebsite.servlet.website;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import gov.bfar.cms.dao.AboutUsDao;
import gov.bfar.cms.dao.ContactUsDao;
import gov.bfar.cms.dao.DownloadDao;
import gov.bfar.cms.dao.HomepageDao;
import gov.bfar.cms.dao.LawsDao;
import gov.bfar.cms.dao.PicturesDao;
import gov.bfar.cms.dao.ProfileDao;
import gov.bfar.cms.dao.ServiceDao;
import gov.bfar.cms.dao.VideoDao;
import gov.bfar.cms.domain.AboutUs;
import gov.bfar.cms.domain.ContactUs;
import gov.bfar.cms.domain.Download;
import gov.bfar.cms.domain.Homepage;
import gov.bfar.cms.domain.Laws;
import gov.bfar.cms.domain.Pictures;
import gov.bfar.cms.domain.Profile;
import gov.bfar.cms.domain.Search;
import gov.bfar.cms.domain.Service;
import gov.bfar.cms.domain.Video;

/**
 * Servlet implementation class Search
 */
@WebServlet("/SearchList")
public class SearchList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchList() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		search(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    search(request, response);
	}
	
	protected void search(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    request.removeAttribute("searchlist");
	    String search = request.getParameter("search");
	    List<Search> searchlist = new ArrayList<>();
	    AboutUsDao aboutUsDao = new AboutUsDao();
	    List<AboutUs> aboutUs = aboutUsDao.aboutUsSearchList(search);
        for (AboutUs aboutUs2 : aboutUs) {
            searchlist.add(new Search(aboutUs2.getId(), aboutUs2.getTitle(), aboutUs2.getPageLink()));
        }
	    ContactUsDao contactUsDao = new ContactUsDao();
	    List<ContactUs> contactUs = contactUsDao.contactUsSearchList(search);
	    for (ContactUs contactUs2 : contactUs) {
            searchlist.add(new Search(contactUs2.getId(), contactUs2.getTitle(), contactUs2.getPageLink()));
        }
	    DownloadDao downloadDao = new DownloadDao();
	    List<Download> downloads = downloadDao.downloadSearchList(search);
	    for (Download download : downloads) {
	        searchlist.add(new Search(download.getId(), download.getTitle(), download.getPageLink()));
        }
        HomepageDao homepageDao = new HomepageDao();
        List<Homepage> homepages = homepageDao.homepageSearchList(search);
        for (Homepage homepage : homepages) {
            searchlist.add(new Search(homepage.getId(), homepage.getHead(), homepage.getPageLink()));
        }
        LawsDao lawsDao = new LawsDao();
        List<Laws> laws = lawsDao.lawsSearchList(search);
        for (Laws laws2 : laws) {
            searchlist.add(new Search(laws2.getId(), laws2.getTitle(), laws2.getPageLink()));
        }
        PicturesDao picturesDao = new PicturesDao();
        List<Pictures> pictures = picturesDao.picturesSearchList(search);
        for (Pictures pictures2 : pictures) {
            searchlist.add(new Search(pictures2.getId(), pictures2.getTitle(), pictures2.getPageLink()));
        }
        ProfileDao profileDao = new ProfileDao();
        List<Profile> profiles = profileDao.profileSearchList(search);
        for (Profile profile : profiles) {
            searchlist.add(new Search(profile.getId(), profile.getTitle(), profile.getPageLink()));
        }
        ServiceDao serviceDao = new ServiceDao();
        List<Service> services = serviceDao.serviceSearchList(search);
        for (Service service : services) {
            searchlist.add(new Search(service.getId(), service.getTitle(), service.getPageLink()));
        }
        VideoDao videoDao = new VideoDao();
        List<Video> videos = videoDao.videoSearchList(search);
        for (Video video : videos) {
            searchlist.add(new Search(video.getId(), video.getTitle(), video.getPageLink()));
        }
        
    
        request.setAttribute("searchkey", search);
        request.setAttribute("searchcount", searchlist.size());
        request.setAttribute("searchlist", searchlist);
	    request.setAttribute("navigation", "../website/pages/search/index.jsp");
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
        dispatcher.forward(request, response);
    }

}
