package gov.bfar.cms.regionalwebsite.servlet.website;

import java.util.List;

import gov.bfar.cms.dao.HomepageDao;
import gov.bfar.cms.domain.Homepage;

public class Application {

    public static void main(String[] args) {
        try {
            HomepageDao homepageDaol = new HomepageDao();
            Homepage homepage = homepageDaol.getFirstContent("left", "707b7c7b-005c-47e7-a58b-b5bf60a057f3");
            System.out.println(homepage.isViewonpage());
        } catch (Exception e) {
            System.out.println(e);
        }
        
    }
}
