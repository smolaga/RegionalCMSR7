package gov.bfar.cms.regionalwebsite.servlet.website;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import gov.bfar.cms.dao.HitsDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Hits;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class WebsiteHomepage
 */
@WebServlet("/WebsiteHomepage")
public class WebsiteHomepage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WebsiteHomepage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        inserthits(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    inserthits(request, response);
	}

	protected void inserthits(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    HttpSession session = request.getSession();
	    String uuid = BaseDomain.newUUID();
        Optional<String> date = DateUtil.getFormattedDate("MM/dd/yyyy hh:mm:ss aa");
        Hits hits = new Hits();
        HitsDao hitsDao = new HitsDao();
        hits.setId(uuid);
        hits.setJSessionId(session.getId().toString());
        hits.setSessionDate(date.get());
        hitsDao.save(hits);      
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
        dispatcher.forward(request, response);        
    }
}
