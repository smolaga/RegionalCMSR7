package gov.bfar.cms.regionalwebsite.servlet.website;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WebsiteNavigation
 */
@WebServlet("/WebsiteNavigation")
public class WebsiteNavigation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WebsiteNavigation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		navigation(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		navigation(request, response);
	}
	
	protected void navigation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("pageAction");		
		if (action.equalsIgnoreCase("home")) {
			request.setAttribute("navigation", "../website/pages/home/index.jsp");			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("aboutus")) {			
			String id = request.getParameter("id");
			request.setAttribute("aboutuscontentid", id);
			request.setAttribute("navigation", "../website/pages/aboutus/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("service")) {
			request.setAttribute("navigation", "../website/pages/services/index.jsp");			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("gallery")) {
            request.setAttribute("navigation", "../website/pages/gallery/photogallery.jsp");           
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
            dispatcher.forward(request, response);
        }		
		else if (action.equalsIgnoreCase("program")) {
            request.setAttribute("navigation", "../website/pages/program/index.jsp");           
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("laws")) {
            request.setAttribute("navigation", "../website/pages/laws/index.jsp");        
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("fisheriesprofile")) {
			request.setAttribute("navigation", "../website/pages/fisheriesprofile/index.jsp");		
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("download")) {
            request.setAttribute("navigation", "../website/pages/download/index.jsp");            
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("photogallery")) {
			request.setAttribute("navigation", "../website/pages/gallery/photogallery.jsp");			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("videogallery")) {
			request.setAttribute("navigation", "../website/pages/gallery/videogallery.jsp");		
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("contacts")) {
			request.setAttribute("navigation", "../website/pages/contacts/index.jsp");			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("contentFrame")) {
			String id = request.getParameter("id");			
			request.setAttribute("id", id);
			request.setAttribute("navigation", "../website/pages/home/contentFrame.jsp");			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("subContentFrame")) {
            String id = request.getParameter("id");
            request.setAttribute("id", id);
            request.setAttribute("navigation", "../website/pages/home/subContentFrame.jsp");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("aboutContentFrame")) {
			String ctype = request.getParameter("contenttype");
			String id = request.getParameter("id");
			request.setAttribute("aboutuscontentid", id);
			request.setAttribute("navigation", "../website/pages/aboutus/index.jsp");
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("profileContentFrame")) {
			String id = request.getParameter("id");
			request.setAttribute("profilecontentid", id);			
			request.setAttribute("navigation", "../website/pages/fisheriesprofile/index.jsp");			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("serviceContentFrame")) {
			String id = request.getParameter("id");
			request.setAttribute("servicecontentid", id);			
			request.setAttribute("navigation", "../website/pages/services/serviceContent.jsp");		
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("galleryContentFrame")) {
            String id = request.getParameter("id");
            request.setAttribute("gallerycontentid", id);           
            request.setAttribute("navigation", "../website/pages/gallery/galleryImage.jsp");            
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("lawsContentFrame")) {
            String id = request.getParameter("id");
            request.setAttribute("lawscontentid", id);           
            request.setAttribute("navigation", "../website/pages/laws/lawsContent.jsp");           
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("centerContentFrame")) {
			String id = request.getParameter("id");
			request.setAttribute("homepagecontentid", id);			
			request.setAttribute("navigation", "../website/pages/home/CentercontentFrame.jsp");			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		else if (action.equalsIgnoreCase("downloadContentFrame")) {
            String id = request.getParameter("id");
            request.setAttribute("downloadcontentid", id);          
            request.setAttribute("navigation", "../website/pages/download/index.jsp");            
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
            dispatcher.forward(request, response);
        }
		else if (action.equalsIgnoreCase("galleryPerYearMonth")) {
			int year = Integer.parseInt(request.getParameter("year"));
			int month = Integer.parseInt(request.getParameter("month"));
			request.setAttribute("gyear", year);
			request.setAttribute("gmonth", month);
			request.setAttribute("navigation", "../website/pages/gallery/photogallery.jsp");			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/website/websiteIndex.jsp");
			dispatcher.forward(request, response);
		}
		
	}

}
