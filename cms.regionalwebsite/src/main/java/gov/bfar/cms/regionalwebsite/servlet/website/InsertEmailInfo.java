package gov.bfar.cms.regionalwebsite.servlet.website;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Optional;

import gov.bfar.cms.dao.FeedbackDao;
import gov.bfar.cms.domain.BaseDomain;
import gov.bfar.cms.domain.Feedback;
import gov.bfar.cms.regionalwebsite.util.RecaptchaUtil;
import gov.bfar.cms.util.DateUtil;

/**
 * Servlet implementation class InsertEmailInfo
 */
@WebServlet("/InsertEmailInfo")
public class InsertEmailInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertEmailInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 // TODO Auto-generated method stub
        emailInfo(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    emailInfo(request, response);
	}
	protected void emailInfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Optional<String> date = DateUtil.getFormattedDate("MM/dd/yy hh:mm:ss aa");
		String uuid = BaseDomain.newUUID();
		HttpSession session = request.getSession();
		String userid = (String) session.getAttribute("userid");
		Feedback feedback = new Feedback();
		FeedbackDao feedbackDao = new FeedbackDao();
		feedback.setId(uuid);
		feedback.setRecipientAddress(request.getParameter("email"));
		feedback.setSubject(request.getParameter("subject"));
		feedback.setContent(request.getParameter("message"));
		feedback.setMarkasRead(false);
		feedback.setDateCreated(date.get());
		feedback.setCreatedBy(userid);
		
		// NEW RECAPTCHA CONFIG START
		String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
		System.out.println("gRecaptchaResponse=" + gRecaptchaResponse);
		boolean validRecaptcha = false;
		String errorRecaptchaResponse = null;
		// Verify CAPTCHA.
		validRecaptcha = RecaptchaUtil.verify(gRecaptchaResponse);
		if (!validRecaptcha) {
			errorRecaptchaResponse = "Captcha invalid!";
		} else {
			try {
				feedbackDao.save(feedback);
				session.setAttribute("message", "MESSAGE IS ALREADY SENT!!!");
			} catch (Exception e) {
				session.setAttribute("message", "MESSAGE IS NOT SENT!!!");
			}
		}
		// NEW RECAPTCHA CONFIG START
		response.sendRedirect("/WebsiteNavigation?pageAction=contacts");
	    
    }

}
