<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.AboutUsDao"%>
<%@page import="gov.bfar.cms.domain.AboutUs"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>	
<div class="container">
	<div class="row" >			
		<div class="col-md-9">				
			<div width="560" height="100%" class="contentContainer">
			<% AboutUsDao aboutUsDao = new AboutUsDao();
				List<AboutUs> aboutUs = aboutUsDao.aboutUsSideBarList("html","pdf");
				String content = "";
				String contenttype = "";
				String pdffilepath = "";
				
				if(request.getAttribute("aboutuscontentid") != null){
					String id = (String) request.getAttribute("aboutuscontentid");					
					AboutUs getaboutus = aboutUsDao.findById(id);
					content = getaboutus.getContent();
					contenttype = getaboutus.getContentType();
					pdffilepath = getaboutus.getPdfFilePath();
				}else{
				    try{
				        AboutUs getaboutus = aboutUsDao.aboutUsSelectList();
						content = getaboutus.getContent();
						contenttype = getaboutus.getContentType();
						pdffilepath = getaboutus.getPdfFilePath();    
				    }catch(Exception e){
				        e.getMessage();
				    }
					
				}
				%>					
											
		<%if(contenttype.equalsIgnoreCase("html") || contenttype.equalsIgnoreCase("subhtml")){%>
					<%=content%>						
			<%}else if(contenttype.equalsIgnoreCase("pdf") || contenttype.equalsIgnoreCase("subpdf")){%>						
					<iframe id="viewer" style="border:1px solid #666CCC" title="PDF in an i-Frame" src="<%=pdffilepath %>" frameborder="1" scrolling="auto" height="1100" width="850" ></iframe>
			<%}else{ %>						
			<%} %>							
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="contentside">
				<div class="intro-text text-center">About List</div>
				<div class="scroll">
					<table class="hoverTable contentTable">
					<% 	try{
					    AboutUsDao aboutUsDaosidebar = new AboutUsDao();
						List<AboutUs> aboutUssidebar = aboutUsDaosidebar.aboutUsSideBarList("html","pdf");						
							for(AboutUs aboutUs2 : aboutUssidebar) { %>							
							<tr>
								<td><a class="side" href="<%=aboutUs2.getPageLink()%>"><%=aboutUs2.getTitle() %></a></td>
								
							</tr>
							<%}
						}catch(Exception e){
						    e.getMessage();					
						    }%> 						
					</table>
					<br>
				</div>
			</div>
		</div> 
		
	</div>
</div>
