<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%String tid = request.getAttribute("gallerycontentid").toString(); 
PicturesDao picturesDao = new PicturesDao();
List<Pictures>pictures = picturesDao.galleryImageList(tid);%>

 <div class="container">  
        <ul class="row" style="list-style: none">
        <% for(Pictures pic : pictures){ %>	
            <li class="col-xs-2 col-sm-2">
            	<img alt="GALLERY IMAGE" class="img-responsive" width="100%" height="100%" src="<%=pic.getImageFilePath()%>">
                <br>
                <div style="text-align:center; font-size:15px"><%=pic.getTitle() %></div><br>
            </li>
		<%}%>
        </ul>             
    </div>
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content"> 
          <div class="modal-header" style="text-align: right;">
	        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">&times;</button>
	      </div>
          <div class="modal-body">     
          </div>
        </div>
      </div>
    </div>