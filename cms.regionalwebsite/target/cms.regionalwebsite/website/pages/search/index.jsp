<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container">

	<div class="col-lg-12"><h1><label>About ${searchcount} results ("${searchkey}")</label></h1></div>

	<div class="col-md-12" align="left">
		<c:forEach items="${searchlist}" var="searchlist">
			<a href="${searchlist.getPageLink()}"><p><label>${searchlist.getTitle()}</label></p></a><br>
		</c:forEach>
	</div>
	
</div>