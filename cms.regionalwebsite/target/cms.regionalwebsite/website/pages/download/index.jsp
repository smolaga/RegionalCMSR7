<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.DownloadDao"%>
<%@page import="gov.bfar.cms.domain.Download"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>	
	
<div class="section">
		<div class="container">
			<div class="row" >			
				<div class="col-md-9">				
					<div width="560" height="100%" class="contentContainer">
					<% DownloadDao downloadDao = new DownloadDao();
						List<Download> download = downloadDao.downloadSideBarList("html","pdf");
						String content = "";
						String contenttype = "";
						String pdffilepath = "";
						
						if(request.getAttribute("downloadcontentid") != null){
							String id = (String) request.getAttribute("downloadcontentid");							
							Download getdownload = downloadDao.findById(id);
							content = getdownload.getContent();
							contenttype = getdownload.getContentType();
							pdffilepath = getdownload.getPdfFilePath();
						}else{
						    try{
						        Download getdownload = downloadDao.downloadSelectList();
								content = getdownload.getContent();
								contenttype = getdownload.getContentType();
								pdffilepath = getdownload.getPdfFilePath();    
						    }catch(Exception e){
						        e.getMessage();
						    }
							
						}
						%>					
													
				<%if(contenttype.equalsIgnoreCase("html") || contenttype.equalsIgnoreCase("subhtml")){%>
							<%=content%>						
					<%}else if(contenttype.equalsIgnoreCase("pdf") || contenttype.equalsIgnoreCase("subpdf")){%>						
							<iframe id="viewer" style="border:1px solid #666CCC" title="PDF in an i-Frame" src="<%=pdffilepath %>" frameborder="1" scrolling="auto" height="1100" width="850" ></iframe>
					<%}else{ %>						
					<%} %>							
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="contentside">
						<div class="intro-text text-center">Downloads List</div>
						<div class="scroll">
							<table class="hoverTable contentTable">
							<% 	try{
							    DownloadDao downloadDaosidebar = new DownloadDao();
								List<Download> downloadsidebar = downloadDaosidebar.downloadSideBarList("html","pdf");								
								
									for(Download downloads : downloadsidebar) { %>							
									<tr>
										<td><a class="side" href="<%=downloads.getPageLink()%>"><%=downloads.getTitle() %></a></td>										
									</tr>
									<%}
								}catch(Exception e){
								    e.getMessage();								  
								}%> 
								
							</table>
							<br>
						</div>
					</div>
				</div> 
				
			</div>
		</div>
	</div>