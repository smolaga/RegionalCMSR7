<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.dao.ProfileDao"%>
<%@page import="gov.bfar.cms.domain.Profile"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>	
<div class="container">
	<div class="row" >			
		<div class="col-md-9">				
			<div width="560" height="100%" class="contentContainer">
			<% ProfileDao profileDao = new ProfileDao();
				List<Profile> profile = profileDao.profileSideBarList("html","pdf");
				String content = "";
				String contenttype = "";
				String pdffilepath = "";
				
				if(request.getAttribute("profilecontentid") != null){
					String id = (String) request.getAttribute("profilecontentid");					
					Profile getprofile = profileDao.findById(id);
					content = getprofile.getContent();
					contenttype = getprofile.getContentType();
					pdffilepath = getprofile.getPdfFilePath();
				}else{
				    try{
				        Profile getprofile = profileDao.profileSelectList();
						content = getprofile.getContent();
						contenttype = getprofile.getContentType();
						pdffilepath = getprofile.getPdfFilePath();   
				    }catch(Exception e){
				        e.getMessage();
				    }
					
				}
				%>					
											
		<%if(contenttype.equalsIgnoreCase("html") || contenttype.equalsIgnoreCase("subhtml")){%>
					<%=content%>						
			<%}else if(contenttype.equalsIgnoreCase("pdf") || contenttype.equalsIgnoreCase("subpdf")){%>						
					<iframe id="viewer" style="border:1px solid #666CCC" title="PDF in an i-Frame" src="<%=pdffilepath %>" frameborder="1" scrolling="auto" height="1100" width="850" ></iframe>
			<%}else{ %>						
			<%} %>							
			</div>
			
			<!--frame end-->
		</div>
		
		<div class="col-md-3">
			<div class="contentside">
				<div class="intro-text text-center">Fisheries Profile List</div>
				<div class="scroll">
					<table class="hoverTable contentTable">
					<% 	try{
					    ProfileDao profileUsDaosidebar = new ProfileDao();
						List<Profile> profilesidebar = profileUsDaosidebar.profileSideBarList("html","pdf");						
						
							for(Profile profiles : profilesidebar) { %>							
							<tr>
								<td><a class="side" href="<%=profiles.getPageLink()%>"><%=profiles.getTitle() %></a></td>
								
							</tr>
							<%}
						}catch(Exception e){
						    e.getMessage();						  
						}%> 
						
					</table>
					<br>
				</div>
			</div>
		</div>	
	</div>
</div>
