<%@page import="gov.bfar.cms.dao.HomepageDao"%>
<%@page import="gov.bfar.cms.domain.Homepage"%>
<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>
<%@page import="gov.bfar.cms.domain.Otherlinks"%>
<%@page import="gov.bfar.cms.dao.OtherlinksDao"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<style>
ul.a {
	list-style-type: circle;
}
</style>
<div class="container">
	<div class="row">
		
<%try {
	PicturesDao picturesDao1 = new PicturesDao();
	int x = 0;
	x = picturesDao1.getCountSlider() - 1;
%>
<%	PicturesDao picturesDao = new PicturesDao();
	Pictures pictures1 = picturesDao.getFirstSlider();%>
<%if(pictures1.isStatus()){%>
		<div class="slider-color">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div id="carousel-example-generic" class="carousel slide">
							<!-- Indicators -->
							<ol class="carousel-indicators hidden-xs">
								<li data-target="#carousel-example-generic" data-slide-to="0"
									class="active"></li>
								<%for (int i = 1; i <= x; i++) {%>
								<li data-target="#carousel-example-generic"
									data-slide-to="<%=i%>"></li>
								<%}%>
							</ol>

							<!-- Wrapper for slides -->
							
							<div class="carousel-inner" style="height: 300px; width: 100%">
								<div class="item active" style="width: 100%; height: 300px;">
									<%if (pictures1.getPageLink().equalsIgnoreCase("")) {%>
									<img class="img-responsive" style="width: 100%; height: 100%;"
										alt="" width="100%" height="100%"
										src="<%=pictures1.getImageFilePath()%>">
									<%} else {%>
									<a href="<%=pictures1.getPageLink()%>"> <img
										class="img-responsive" style="width: 100%; height: 100%;"
										alt="" width="100%" height="100%"
										src="<%=pictures1.getImageFilePath()%>">
									</a>
									<%}%>
								</div>
								<%List<Pictures> pictures = picturesDao1.getNotFirstSlider();%>
								<%for (Pictures picture : pictures) {%>
								<div class="item" style="height: 100%; width: 100%">
									<%if (picture.getPageLink() == null) {%>
									<img class="img-responsive" style="width: 100%; height: 100%;"
										alt="Slider Image" width="100%" height="100%"
										src="<%=picture.getImageFilePath()%>">
									<%} else {%>
									<a href="<%=picture.getPageLink()%>"> <img
										class="img-responsive" style="width: 100%; height: 100%;"
										alt="Slider Image" width="100%" height="100%"
										src="<%=picture.getImageFilePath()%>">
									</a>
									<%}%>
								</div>
								<%}%>
								</div>
							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic"
								data-slide="prev"> <span class="icon-prev"></span>
							</a> <a class="right carousel-control"
								href="#carousel-example-generic" data-slide="next"> <span
								class="icon-next"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<%}} catch (Exception e) {
			e.getMessage();
	}%>				
		
		<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div style="border: 1px solid #b3b8ba; background-color: white">
							<%try{
							    HomepageDao homepageDaol = new HomepageDao();
								List<Homepage> homepagesl = homepageDaol.webTitleList("left");%>
							<%for (Homepage homepagel : homepagesl) {
							Homepage flc = homepageDaol.getFirstContent("left", homepagel.getId());
							if(flc.isViewonpage() == true){%>
							<div class="intro-title text-center">
								<%=homepagel.getHead()%>
							</div>

							<%List<Homepage> leftcontent = homepageDaol.webMainContentList("left", homepagel.getId());
								for (Homepage homepagelc : leftcontent) {%>
								<a href="<%=homepagelc.getPageLink()%>"><%=homepagelc.getShortContent()%></a>
								<%}%>
							<a href="<%=flc.getPageLink()%>">
						<!-- <button type="button" class="btn btn-primary btn-xs" style="margin-left: 72%;">Read More</button></a> -->
							<%}%>
							<%}}catch(Exception e){
							    e.getMessage();
							    System.out.println(e);
							}%>


						</div>
					</div>
					<div class="col-md-6">
						<div style="background: #d0e4ef;">
							<%try{
								HomepageDao homepageDaoc = new HomepageDao();
								List<Homepage> homepagesc = homepageDaoc.webTitleList("center");%>
								<%for (Homepage homepagec : homepagesc) {
								Homepage fc = homepageDaoc.getFirstContent("center", homepagec.getId());
								if(fc.isViewonpage() == true){%>
							<div class="intro-title text-center">
								<%=homepagec.getHead()%>
							</div>
							<br>
								<%List<Homepage> centercontent = homepageDaoc.webMainContentList("center", homepagec.getId());
								if(fc.getShortContent() != null){
								for (Homepage homepagecc : centercontent) {%>
									<a href="<%=homepagecc.getPageLink()%>"><%=homepagecc.getShortContent()%></a>
									<%}%>
							<a href="<%=fc.getPageLink()%>">
							<!-- <button type="button" class="btn btn-primary btn-xs" style="margin-left: 82%;">Read More</button></a>-->
							<%}}%>
							<%}}catch(Exception e){
								    e.getMessage();
								}%>
						</div>
					</div>
					<div class="col-md-3">
						<div style="border: 1px solid #b3b8ba; background-color: white">
							<%try{ 
								HomepageDao homepageDaor = new HomepageDao();
								List<Homepage> homepagesr = homepageDaor.webTitleList("right");%>
								<%for (Homepage homepager : homepagesr) {
								Homepage fr = homepageDaor.getFirstContent("right", homepager.getId());
								if(fr.isViewonpage()){%>

							<div class="intro-title text-center">
								<%=homepager.getHead()%>
							</div>							
								<%List<Homepage> rightcontent = homepageDaor.webMainContentList("right", homepager.getId());
								for (Homepage homepagerc : rightcontent) {%>
							<a href="<%=homepagerc.getPageLink()%>"><%=homepagerc.getShortContent()%></a>
							<%}%>
							<!-- <a href="<%=fr.getPageLink()%>"><button type="button" class="btn btn-primary btn-xs" style="margin-left: 72%;">Read More</button></a> -->
							<%}%>
							<%}}catch(Exception e){
								    e.getMessage();
								}%>
						</div>
					</div>
					<div class="col-md-12" style="margin-top:2%; margin-bottom:0%">
						<%
							try {
								OtherlinksDao otherlinksDao = new OtherlinksDao();
								Otherlinks otherlinks = otherlinksDao.otherlinksContentList();
						%>
						<%
							if (otherlinks.getContent() != null) {
						%>
						<div class="intro-title text-center"><%=otherlinks.getTitle()%></div>
						<%=otherlinks.getContent()%>
						<%
							}
						%>
						<%
							} catch (Exception e) {
								e.getMessage();
							}
						%>
					</div>
				</div>
			</div>
		</div>

		<script>
			// Activates the Carousel
			$('.carousel').carousel({
				interval : 5000
			})
		</script>
	</div>
</div>