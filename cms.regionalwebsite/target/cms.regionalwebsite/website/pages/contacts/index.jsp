<%@page import="java.util.List"%>
<%@page import="gov.bfar.cms.regionalwebsite.util.RecaptchaUtil"%>
<%@page import="gov.bfar.cms.dao.ContactUsDao"%>
<%@page import="gov.bfar.cms.domain.ContactUs"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha"%>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<%
							try {
								ContactUsDao contactUsDao = new ContactUsDao();
								ContactUs contactUs = contactUsDao.contactUsContentList();
						%>
						<%=contactUs.getContent()%>
						<%
							} catch (Exception e) {
								e.getMessage();
							}
						%>
					</div>
				</div>
			</div>

			<form method="POST" action="/InsertEmailInfo">
				<div class="row">
					<div class="col-md-6">
						<div class="intro-text text-center">Email Inquiry</div>
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12">
									<h4>Email:</h4>
								</div>
								<div class="col-md-12">
									<input name="email" class="form-control"
										placeholder="Please enter your Email *"
										pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
										oninvalid="setCustomValidity('You have not given a correct e-mail address')"
										onchange="try{setCustomValidity('')}catch(e){}">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12">
									<h4>Subject:</h4>
								</div>
								<div class="col-md-12">
									<input name="subject" class="form-control"
										placeholder="Please enter the Subject" required>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-12">
								<h4>Message *</h4>
							</div>
							<div class="col-md-12">
								<textarea id="tacontent" class="form-control"
									style="height: 500px;" name="message"></textarea>
							</div>
						</div>
						<%-- <div class="col-md-12">
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-5">
							<div align="center" id="captchasection"><img id="captcha" src="<c:url value="simpleCaptcha.jpg"  />" width="100%"  style="background-color: red; border:solid 5px black; font-family:cursive; font-weight: bolder;"></div>
							<button onclick="return location.reload();"><i class="fa fa-refresh" aria-hidden="true"></i> Change Image</button>
						</div>
						<div class="col-md-7"><label>Security Check:</label><input class="form-control" placeholder="Please type the text in image" type="text" name="answer" /></div>
					</div> --%>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-12" align="center">
							<%-- <%
								String sitekey = RecaptchaUtil.getConfig("sitekey");
								String secretkey = RecaptchaUtil.getConfig("secretkey");
								ReCaptcha c = ReCaptchaFactory.newReCaptcha(sitekey, secretkey, false);
								out.print(c.createRecaptchaHtml(null, null));
							%> --%>
							
							<!-- RECAPTCHA TEMPLATE START -->
							<%String sitekey = RecaptchaUtil.getConfig("sitekey");%>
							 <div class="g-recaptcha" data-sitekey="<%=sitekey%>"></div>
							 <!-- RECAPTCHA TEMPLATE END -->
							 
						</div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-12">
							<input type="submit" class="btn btn-success btn-send"
								value="Send message"><br>
							<br> <label style="color: red">${message}</label>
							<%
								session.removeAttribute("message");
							%>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
