<%@page import="gov.bfar.cms.dao.PicturesDao"%>
<%@page import="gov.bfar.cms.domain.Pictures"%>
<%@page import="gov.bfar.cms.dao.FooterDao"%>
<%@page import="gov.bfar.cms.domain.Footer"%>
<%@page import="gov.bfar.cms.dao.HitsDao"%>
<%@page import="gov.bfar.cms.domain.Hits"%>

<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <%String nav  = "";
if(request.getAttribute("navigation") == null){
	nav = "/website/pages/home/index.jsp";
}
else{
	nav = request.getAttribute("navigation").toString(); 
}
%>
<title>Regional Websites</title>
    <link href="${pageContext.request.contextPath}/website/media/css/bootstrap.css" rel="stylesheet">    
	<link href="${pageContext.request.contextPath}/website/media/css/web.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/website/media/css/navigation.css" rel="stylesheet">
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src="${pageContext.request.contextPath}/website/media/js/jquery-2.1.4.min.js" type="text/javascript"></script>	
	<script src="${pageContext.request.contextPath}/website/media/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/website/media/js/analytics.js" type="text/javascript"></script>	
	<script src="${pageContext.request.contextPath}/website/media/js/script.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/website/media/js/photo-gallery.js" type="text/javascript"></script>
</head>
<body>
<div class="container" style="border: 1px solid #b3b8ba;">
<div class="row">      
    <div class="BFARHeader">
 		<img class="img-responsive img-border img-fixed">								
								<% try{
								PicturesDao picturesDao = new PicturesDao();
								Pictures pictures = picturesDao.getBanner();%>	
								<img alt="" width="100%" height="250px" src="<%=pictures.getImageFilePath()%>"> 
								<%}catch(Exception e){
						 	    e.getMessage();} %>							
	</div>		
			
	<div class="content_section">	
		<nav class="navbar navbar-inverse marginBottom-0 text-center" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- <a class="navbar-brand" href="index.html" >Felis</a> -->
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/WebsiteNavigation?pageAction=home">Home</a></li>
                <li><a href="/WebsiteNavigation?pageAction=aboutus">About Us</a></li>
                  <li><a href="/WebsiteNavigation?pageAction=service">Services</a></li>
                   <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Gallery <b class="caret"></b></a>
                    <ul class="dropdown-menu"> 
                      <li><a href='/WebsiteNavigation?pageAction=photogallery'>Photos</a></li>
						<li><a href='/WebsiteNavigation?pageAction=videogallery'>Videos</a></li>                                          
                    </ul>
                </li>
              		<li><a href="/WebsiteNavigation?pageAction=laws">Laws and Regulation</a></li>
				 <li><a href="/WebsiteNavigation?pageAction=fisheriesprofile">Fisheries Profile</a></li>
				  <li><a href="/WebsiteNavigation?pageAction=download">Downloads</a></li>
				 <li><a href="/WebsiteNavigation?pageAction=contacts">Contact Us</a></li>         
            </ul>
        </div>      
    </nav>	
        <div class="container searchDate">
    	<div class="row">
			<div class="col-md-6 text-left">
				<div class="date">
					<div id="dateandtime"></div>
					<script>
						var myVar = setInterval(function() {
							myTimer()
						}, 1000);
						function myTimer() {
							var d = new Date();
							var date = new Date();
							var getdate = date.toDateString();
							document.getElementById("dateandtime").innerHTML = getdate
									+ " " + d.toLocaleTimeString();
						}
					</script>
				</div>
			</div>
			 <div class="col-lg-3">
			 </div>
			 <div class="col-lg-3">
			 	<form method="POST" action="/SearchList">
				    <div class="input-group">
				      <input type="text" class="form-control" name="search" placeholder="Search for...">
				      <span class="input-group-btn">
				      	<input class="btn btn-primary" type="submit" value="Go!">
				      </span>
				    </div>
			    </form>
			  </div>
    	</div>
    </div>
    			
    <div class="container" >
		<div class="row">
			<jsp:include page="<%=nav%>"></jsp:include>
		</div>
	</div>
<br><br>
    <script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).parent().siblings().removeClass('open');
                $(this).parent().toggleClass('open');
            });
        });
    })(jQuery);
    </script>
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '${pageContext.request.contextPath}/website/media/js/analytics.js', 'ga');
    ga('create', 'UA-40413119-1', 'bootply.com');
    ga('send', 'pageview');
    </script>
    


<div class="col-md-12 text-center">
<div class="row">
 <% try{ FooterDao footerDao = new FooterDao();
Footer footer = footerDao.footerContentList(); %>
<%if(footer.isStatus()){%>

<div class="footer" style="border-top:1px solid #b3b8ba">
    <div class="container" >
 <div class="divider">       
            	
					<%=footer.getContent() %>
					
					<% try{ HitsDao hitsDao = new HitsDao();%>
					Total number of visits: 
					<%=hitsDao.getHitCount()%>
					<%}catch(Exception e){
						e.getMessage();} %>
						
		    		<%} %>
		    		
		    	</div>
					<%}catch(Exception e){
						e.getMessage();
					 }%>		        
		</div>				     
	</div>
</div>
	
</div>
 </div>
 </div>
</div>
</body>
</html>