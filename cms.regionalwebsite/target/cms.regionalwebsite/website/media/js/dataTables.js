	$(document).ready(function(){
$('.dataTables').DataTable({
	 "dom": '<"top"i>rt<"bottom"lp><"clear">'
});


$('.dataTablesWithExportButtons').DataTable( {
	dom: 'Bfrtip',
	"ordering": false,
	buttons: [
	    'copyHtml5',
	    'excelHtml5',
	    'csvHtml5',
	    'pdfHtml5',
	    'print'
	    ]
} );
} );
