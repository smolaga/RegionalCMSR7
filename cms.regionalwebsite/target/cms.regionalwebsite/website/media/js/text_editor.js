tinymce.init({
			selector : '.mceEditor',
			plugins : [ "searchreplace visualblocks code fullscreen",
					"advlist autolink lists link charmap print preview anchor",
					"insertdatetime  table contextmenu" ],
			toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
			autosave_ask_before_unload : false,
			/*max_height : 500,
			min_height : 560,
			height : 10,*/
			paste_insert_word_content_callback : "convertWord",
			paste_auto_cleanup_on_paste : true,
		});

//bind data to textAreaView
//bindDataToTextAreaView("AboutUsDaoImp");
//end

/*	tinymce.init({
	    selector: "textarea",
	    theme: "modern",
	    plugins: [
	        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	        "searchreplace wordcount visualblocks visualchars code fullscreen",
	        "insertdatetime media nonbreaking save table contextmenu directionality",
	        "emoticons template paste textcolor colorpicker textpattern imagetools"
	    ],
	    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	    toolbar2: "print preview media | forecolor backcolor emoticons",
	    image_advtab: true,
	    templates: [
	        {title: 'Test template 1', content: 'Test 1'},
	        {title: 'Test template 2', content: 'Test 2'}
	    ]
	});*/

function convertWord(type, content) {
	switch (type) {
	// Gets executed before the built in logic performs it's cleanups
	case "before":
		// content = content.toLowerCase(); // Some dummy logic
		// alert(content);
		break;
	// Gets executed after the built in logic performs it's cleanups
	case "after":
		// alert(content);
		content = content.replace(/<!(?:--[\s\S]*?--\s*)?>\s*/g, '');
		// content = content.toLowerCase(); // Some dummy logic
		// alert(content);
		break;
	}
	return content;
}
